package co.dpworld.pmsapp.service;

import co.dpworld.pmsapp.model.TablaColaDeTrabajo;

/**
 * Interfaz que define los metodos para las colas de trabajo
 * @author Sergio.Arias
 *
 */
public interface IColaDeTrabajoService extends ICRUD<TablaColaDeTrabajo> {

	public TablaColaDeTrabajo listarCola() throws Exception;
}
