/**
 * 
 */
package co.dpworld.pmsapp.service;

import co.dpworld.pmsapp.model.InformacionUsuarioRp;
import co.dpworld.pmsapp.model.Usuario;

/**
 * Interfaz que define los metodos para el servicio de usuarios
 * 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
public interface IUsuarioService extends ICRUD<Usuario> {

	/**
	 * Metodo que retorna la informacion de un usuario
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Objeto con la informacion del usuario consultado
	 * @throws Exception 
	 */
	InformacionUsuarioRp obtenerInfoUsuario();

}
