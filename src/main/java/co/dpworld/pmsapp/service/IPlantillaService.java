package co.dpworld.pmsapp.service;

import java.io.IOException;

import co.dpworld.pmsapp.model.Archivo;

public interface IPlantillaService {

	Archivo descargarArchivo(String codigoTipoDocumento) throws IOException, Exception;

}
