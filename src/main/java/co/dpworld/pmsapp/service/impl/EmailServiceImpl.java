/**
 * 
 */
package co.dpworld.pmsapp.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.service.IEmailService;

/**
 * Servicio que gestiona operaciones relacionadas con correo electronico
 * 
 * @author Sergio Arias
 * @date 11/04/2020
 *
 */
@Service
public class EmailServiceImpl implements IEmailService {

	@Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	private Environment env;
	
	@Override
	public void enviarEmail(String[] listEmailTo,String subject,String messageHtml) throws MessagingException {
		

        MimeMessage msg = javaMailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
        helper.setFrom(env.getProperty("spring.mail.username"));

        helper.setTo(listEmailTo);

        helper.setSubject(subject);

        // true = text/html
        helper.setText(messageHtml, true);

        javaMailSender.send(msg);
		
	}

}
