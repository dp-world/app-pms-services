package co.dpworld.pmsapp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.ColaDeTrabajo;
import co.dpworld.pmsapp.model.TablaColaDeTrabajo;
import co.dpworld.pmsapp.service.IColaDeTrabajoService;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que contiene la implementacion de colas de trabajo 
 * @author Sergio Arias
 * @date 20/11/2019
 *
 */
@Service
public class ColaDeTrabajoServiceImpl implements IColaDeTrabajoService{

	@Autowired
	private RidcAdapter ridcAdapter;
	
	@Autowired 
	private UtilRidcWcc utilRidcWcc;
	
	
	public TablaColaDeTrabajo listarCola() throws Exception {
		
		
		
		
		TablaColaDeTrabajo tablaColaDeTrabajo = new TablaColaDeTrabajo();
		List<ColaDeTrabajo> listaColaDeTrabajo = new ArrayList<>();
		
		//1. Obtener el username del JWT
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		
		//2. integracion con oracle wcc para obtener inbox de un usuario 
		List<DataObject> listaInbox = ridcAdapter.obtenerInboxUsuario(currentPrincipalName, "WorkflowInQueue");
		
		
		//Obtener hash codigos y descripciones vista de departamentos
		Map<String,String> hashDepartamentos = utilRidcWcc.obtenerClaveValorDepartamentoPorCod();
		
		//3. Se recorren cada uno de los flujos para obtener info de cada uno(metadata)
		for(DataObject flujo :  listaInbox)
		{
			if(flujo.get("dWfStepID") != null && flujo.get("dID") != null ){
			
				//4. integracion con oracle wcc para obtener informacion (metadata) de cada flujo
				List<DataObject> listaMetadataFlujo = ridcAdapter.obtenerInformacionDeUnFlujo(currentPrincipalName,
														  "DOC_INFO",
														  flujo.get("dWfStepID"),
														  flujo.get("dID"))
														  ;
				if(!listaMetadataFlujo.isEmpty()) {
					DataObject metadataFlujo = listaMetadataFlujo.get(0);
					DataObject metadataFlujoLocalData = listaMetadataFlujo.get(1);
					
					
					// Tipo Solicitud = Creacion
					
					UtilRidcWcc util = new UtilRidcWcc();
					String proceso = util.obtenerCodigoTipoSolicitudPorCodigo(metadataFlujo.get("xProceso"));
					
					if(proceso.equalsIgnoreCase("Creacion"))
					{
						//5. Se obtienen los datos para mostrar en la cola de trabajo
						ColaDeTrabajo colaDeTrabajo = new ColaDeTrabajo();
						colaDeTrabajo.setDepartamento(utilRidcWcc.obtenerNombreListaPorCodHash(metadataFlujo.get("xDepartamento"),hashDepartamentos));
						colaDeTrabajo.setFecha(metadataFlujo.get("dLastModifiedDate"));
						colaDeTrabajo.setId(metadataFlujo.get("dID"));
						colaDeTrabajo.setNombrePaso(flujo.get("dWfStepDescription") + " - ("+metadataFlujoLocalData.get("RemainingStepUsers")+")");
						colaDeTrabajo.setRequiereSoporte(metadataFlujo.get("xRequiereSoporte").equalsIgnoreCase("0") ? false : true);
						colaDeTrabajo.setNombreDocumento(flujo.get("dOriginalName"));
						colaDeTrabajo.setTipoSolicitud(proceso);
						colaDeTrabajo.setIdPaso(flujo.get("dWfStepID"));
						colaDeTrabajo.setdDocName(flujo.get("dDocName"));
						colaDeTrabajo.setdRevLabel(flujo.get("dRevLabel"));
						colaDeTrabajo.setdSecurityGroup(flujo.get("dSecurityGroup"));
						colaDeTrabajo.setComentarios(metadataFlujo.get("xComments"));
						
						String listaUsuariosPorComa = metadataFlujoLocalData.get("RemainingStepUsers");
						if(listaUsuariosPorComa.contains(","))
							colaDeTrabajo.setListaUsuarios(Arrays.asList(listaUsuariosPorComa.split(",")));
			
						//6. Se agregan los datos a la lista general de cola de trabajo
						listaColaDeTrabajo.add(colaDeTrabajo);
					}else if(proceso.equalsIgnoreCase("Actualizacion")){
						
						//5. Se obtienen los datos para mostrar en la cola de trabajo
						ColaDeTrabajo colaDeTrabajo = new ColaDeTrabajo();
						colaDeTrabajo.setDepartamento(utilRidcWcc.obtenerNombreListaPorCodHash(metadataFlujo.get("xDepartamento"),hashDepartamentos));
						colaDeTrabajo.setFecha(metadataFlujo.get("dLastModifiedDate"));
						colaDeTrabajo.setId(metadataFlujo.get("dID"));
						colaDeTrabajo.setNombrePaso(flujo.get("dWfStepDescription")+ " - ("+metadataFlujoLocalData.get("RemainingStepUsers")+")");
						colaDeTrabajo.setRequiereSoporte(metadataFlujo.get("xRequiereSoporte").equalsIgnoreCase("0") ? false : true);
						colaDeTrabajo.setNombreDocumento(metadataFlujo.get("xNombreDocumento"));
						colaDeTrabajo.setTipoSolicitud(proceso);
						colaDeTrabajo.setIdPaso(flujo.get("dWfStepID"));
						colaDeTrabajo.setdDocName(flujo.get("dDocName"));
						colaDeTrabajo.setdRevLabel(flujo.get("dRevLabel"));
						colaDeTrabajo.setdSecurityGroup(flujo.get("dSecurityGroup"));
						colaDeTrabajo.setComentarios(metadataFlujo.get("xComments"));
						
						String listaUsuariosPorComa = metadataFlujoLocalData.get("RemainingStepUsers");
						if(listaUsuariosPorComa.contains(","))
							colaDeTrabajo.setListaUsuarios(Arrays.asList(listaUsuariosPorComa.split(",")));
			
						//6. Se agregan los datos a la lista general de cola de trabajo
						listaColaDeTrabajo.add(colaDeTrabajo);
					}else if(proceso.equalsIgnoreCase("Obsoletizacion")){
						
						//5. Se obtienen los datos para mostrar en la cola de trabajo
						ColaDeTrabajo colaDeTrabajo = new ColaDeTrabajo();
						colaDeTrabajo.setDepartamento(utilRidcWcc.obtenerNombreListaPorCodHash(metadataFlujo.get("xDepartamento"),hashDepartamentos));
						colaDeTrabajo.setFecha(metadataFlujo.get("dLastModifiedDate"));
						colaDeTrabajo.setId(metadataFlujo.get("dID"));
						colaDeTrabajo.setNombrePaso(flujo.get("dWfStepDescription")+ " - ("+metadataFlujoLocalData.get("RemainingStepUsers")+")");
						colaDeTrabajo.setRequiereSoporte(metadataFlujo.get("xRequiereSoporte").equalsIgnoreCase("0") ? false : true);
						colaDeTrabajo.setNombreDocumento(metadataFlujo.get("xNombreDocumento"));
						colaDeTrabajo.setTipoSolicitud(proceso);
						colaDeTrabajo.setIdPaso(flujo.get("dWfStepID"));
						colaDeTrabajo.setdDocName(flujo.get("dDocName"));
						colaDeTrabajo.setdRevLabel(flujo.get("dRevLabel"));
						colaDeTrabajo.setdSecurityGroup(flujo.get("dSecurityGroup"));
						colaDeTrabajo.setComentarios(metadataFlujo.get("xComments"));
						
						String listaUsuariosPorComa = metadataFlujoLocalData.get("RemainingStepUsers");
						if(listaUsuariosPorComa.contains(","))
							colaDeTrabajo.setListaUsuarios(Arrays.asList(listaUsuariosPorComa.split(",")));
			
						//6. Se agregan los datos a la lista general de cola de trabajo
						listaColaDeTrabajo.add(colaDeTrabajo);
						
					}else if(proceso.equalsIgnoreCase("Reactivacion")){
						
						//5. Se obtienen los datos para mostrar en la cola de trabajo
						ColaDeTrabajo colaDeTrabajo = new ColaDeTrabajo();
						colaDeTrabajo.setDepartamento(utilRidcWcc.obtenerNombreListaPorCodHash(metadataFlujo.get("xDepartamento"),hashDepartamentos));
						colaDeTrabajo.setFecha(metadataFlujo.get("dLastModifiedDate"));
						colaDeTrabajo.setId(metadataFlujo.get("dID"));
						colaDeTrabajo.setNombrePaso(flujo.get("dWfStepDescription")+ " - ("+metadataFlujoLocalData.get("RemainingStepUsers")+")");
						colaDeTrabajo.setRequiereSoporte(metadataFlujo.get("xRequiereSoporte").equalsIgnoreCase("0") ? false : true);
						colaDeTrabajo.setNombreDocumento(metadataFlujo.get("xNombreDocumento"));
						colaDeTrabajo.setTipoSolicitud(proceso);
						colaDeTrabajo.setIdPaso(flujo.get("dWfStepID"));
						colaDeTrabajo.setdDocName(flujo.get("dDocName"));
						colaDeTrabajo.setdRevLabel(flujo.get("dRevLabel"));
						colaDeTrabajo.setdSecurityGroup(flujo.get("dSecurityGroup"));
						colaDeTrabajo.setComentarios(metadataFlujo.get("xComments"));
						
						String listaUsuariosPorComa = metadataFlujoLocalData.get("RemainingStepUsers");
						if(listaUsuariosPorComa.contains(","))
							colaDeTrabajo.setListaUsuarios(Arrays.asList(listaUsuariosPorComa.split(",")));
			
						//6. Se agregan los datos a la lista general de cola de trabajo
						listaColaDeTrabajo.add(colaDeTrabajo);
					}else if(proceso.equalsIgnoreCase("Nomenclatura")){
						
						//5. Se obtienen los datos para mostrar en la cola de trabajo
						ColaDeTrabajo colaDeTrabajo = new ColaDeTrabajo();
						colaDeTrabajo.setDepartamento(utilRidcWcc.obtenerNombreListaPorCodHash(metadataFlujo.get("xDepartamento"),hashDepartamentos));
						colaDeTrabajo.setFecha(metadataFlujo.get("dLastModifiedDate"));
						colaDeTrabajo.setId(metadataFlujo.get("dID"));
						colaDeTrabajo.setNombrePaso(flujo.get("dWfStepDescription")+ " - ("+metadataFlujoLocalData.get("RemainingStepUsers")+")");
						colaDeTrabajo.setRequiereSoporte(false);
						colaDeTrabajo.setNombreDocumento(metadataFlujo.get("xNombreDocumento"));
						colaDeTrabajo.setTipoSolicitud(proceso);
						colaDeTrabajo.setIdPaso(flujo.get("dWfStepID"));
						colaDeTrabajo.setdDocName(flujo.get("dDocName"));
						colaDeTrabajo.setdRevLabel(flujo.get("dRevLabel"));
						colaDeTrabajo.setdSecurityGroup(flujo.get("dSecurityGroup"));
						colaDeTrabajo.setComentarios(metadataFlujo.get("xComments"));
						
						String listaUsuariosPorComa = metadataFlujoLocalData.get("RemainingStepUsers");
						if(listaUsuariosPorComa.contains(","))
							colaDeTrabajo.setListaUsuarios(Arrays.asList(listaUsuariosPorComa.split(",")));
			
						//6. Se agregan los datos a la lista general de cola de trabajo
						listaColaDeTrabajo.add(colaDeTrabajo);
					}
					

				
				}
			}
			
			
		}
		tablaColaDeTrabajo.setListaColaDeTrabajo(listaColaDeTrabajo);
		tablaColaDeTrabajo.setCantidad(listaColaDeTrabajo.size());
		return tablaColaDeTrabajo;
	}



	@Override
	public TablaColaDeTrabajo guardar(TablaColaDeTrabajo t) {
		return null;
	}

	@Override
	public TablaColaDeTrabajo modificar(TablaColaDeTrabajo t) {
		return null;
	}

	@Override
	public TablaColaDeTrabajo leer(Integer id) {
		return null;
	}

	@Override
	public void eliminar(Integer id) {
		
	}

	@Override
	public List<TablaColaDeTrabajo> listar() {
		return null;
	}




}
