package co.dpworld.pmsapp.service.impl;

import java.io.File;
import java.nio.file.Files;

import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.model.Archivo;
import co.dpworld.pmsapp.service.IPlantillaService;

@Service
public class PlantillaServiceImpl implements IPlantillaService{


		
	@Override
	public Archivo descargarArchivo(String codigoTipoDocumento) throws Exception {
		Archivo archivo = new Archivo();
		File file;
		byte[] fileContent;
		
		switch (codigoTipoDocumento) {
		
		case "01":
			
			file = new File("C://tmp//plantillas//XX-01-XXX - Template Manual.doc");
			fileContent = Files.readAllBytes(file.toPath());
			
			archivo.setArchivo(fileContent);
			archivo.setMimetype(Files.probeContentType(file.toPath()));
			archivo.setTituloArchivo(file.getName());
			break;

		case "02":
			
			
			file = new File("C://tmp//plantillas//XX-02-XXX - Template Procedimiento.doc");
			fileContent = Files.readAllBytes(file.toPath());
			
			archivo.setArchivo(fileContent);
			archivo.setMimetype(Files.probeContentType(file.toPath()));
			archivo.setTituloArchivo(file.getName());
			break;
			
		
		case "05":
			
			
			file = new File("C://tmp//plantillas//XX-05-XXX - Template Instrucción de Trabajo.doc");
			fileContent = Files.readAllBytes(file.toPath());
			
			archivo.setArchivo(fileContent);
			archivo.setMimetype(Files.probeContentType(file.toPath()));
			archivo.setTituloArchivo(file.getName());
			break;
			
		case "06":
			
			
			file = new File("C://tmp//plantillas//XX-06-XXX - Template de Guía-Política.doc");
			fileContent = Files.readAllBytes(file.toPath());
			
			archivo.setArchivo(fileContent);
			archivo.setMimetype(Files.probeContentType(file.toPath()));
			archivo.setTituloArchivo(file.getName());
			break;

		case "08":
			
			
			file = new File("C://tmp//plantillas//XX-08-XXX - Template de Ayuda Visual.docx");
			fileContent = Files.readAllBytes(file.toPath());
			
			archivo.setArchivo(fileContent);
			archivo.setMimetype(Files.probeContentType(file.toPath()));
			archivo.setTituloArchivo(file.getName());
			break;
			
			
		default:
			break;
		}
	

		if(archivo.getArchivo() == null)
		{
			throw new Exception("No se encuientra la plantilla");
		}
		
		return archivo;
	}

}
