/**
 * 
 */
package co.dpworld.pmsapp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.InformacionUsuarioRp;
import co.dpworld.pmsapp.model.Usuario;
import co.dpworld.pmsapp.service.IUsuarioService;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import oracle.stellent.ridc.model.DataObject;

/**
 * Servicio que gestiona las operaciones relacionadas con usuarios de la API
 * 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
@Service("userDetailsService")
public class UserServiceImpl implements UserDetailsService, IUsuarioService {

	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	@Autowired
	private RidcAdapter ridcAdapter;
	
	@Autowired
	private UtilRidcWcc utilRidcWcc;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		// TODO Se debe agregar la implementacion para obtener el usuario de directorio
		// activo DP World

		/*
		 * if (user == null) { throw new
		 * UsernameNotFoundException(String.format("Usuario no existe", username)); }
		 */

		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("ADMIN"));

		UserDetails userDetails = new User("test", bcrypt.encode("qwerty12345"), roles);
		System.out.println("USUARIO PRUEBA -> " + userDetails.toString());
		return userDetails;
	}

	@Override
	public Usuario guardar(Usuario usuario) {
		return null;
	}

	@Override
	public Usuario modificar(Usuario t) {
		return null;
	}

	@Override
	public Usuario leer(Integer id) {
		return null;
	}

	@Override
	public List<Usuario> listar() {
		return null;
	}

	@Override
	public void eliminar(Integer id) {

	}

	@SuppressWarnings("deprecation")
	@Override		
	public InformacionUsuarioRp obtenerInfoUsuario()  {
		//1. Obtener el username del JWT
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		String roles = "";
		
		List<DataObject> listaInfoUser =  ridcAdapter.obtenerInfoUsuario(currentPrincipalName,"USER_INFO");
		InformacionUsuarioRp infoRp = new InformacionUsuarioRp();
		for(DataObject infoUsuario :  listaInfoUser) {
			infoRp.setCorreoElectronico(infoUsuario.get("dEmail"));
			try {
			infoRp.setDepartamento(infoUsuario.get("dUserType")); // Codigo del departamento
			}catch(Exception e)
			{
				
			}
			infoRp.setNombreCompleto(infoUsuario.get("dFullName"));
			roles = infoUsuario.get("uRolesAppPms"); // Metadata Personalizada User | Lista separada por ,
			
		}try {
			infoRp.setNombreDepartamento(utilRidcWcc.obtenerNombreDepartamentoPorCod(infoRp.getDepartamento()));
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(!roles.isEmpty()) {
			if(roles.contains(",")) {
				List<String> trimmedoles = Arrays.asList(roles.split(",")).stream().map(String::trim).collect(Collectors.toList());
				infoRp.setListaRoles(trimmedoles);
			}else {
				List<String> listaRoles = new ArrayList<>();
				listaRoles.add(roles.trim());
				infoRp.setListaRoles(listaRoles);
			}
		}
		return infoRp;

	}

}
