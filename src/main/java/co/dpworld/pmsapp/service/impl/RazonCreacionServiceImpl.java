/**
 * 
 */
package co.dpworld.pmsapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.RazonCreacion;
import co.dpworld.pmsapp.service.IRazonCreacionService;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que contiene la implementacion del servicio razon y/o creacion de documentos
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@Service
public class RazonCreacionServiceImpl implements IRazonCreacionService{

	@Autowired
	private RidcAdapter ridcAdapter;
	
	@Override
	public RazonCreacion guardar(RazonCreacion t) {
		return null;
	}

	@Override
	public RazonCreacion modificar(RazonCreacion t) {
		return null;
	}

	@Override
	public RazonCreacion leer(Integer id) {
		return null;
	}

	@Override
	public List<RazonCreacion> listar() {
		List<RazonCreacion> listaRazonCreacion = new ArrayList<>();
		
		/* integracion con oracle wcc para obtener listas de una vista */		
		List<DataObject> listaFuenteCreacionWCC = ridcAdapter.obtenerValoresVistas("vistaFuenteCreacion","FuenteCreacion");
		for(DataObject fila :  listaFuenteCreacionWCC)
		{
			listaRazonCreacion.add(new RazonCreacion(Integer.parseInt(fila.get("IDFuente")),fila.get("DescripcionFuente")));
		}
		return listaRazonCreacion;
	}

	@Override
	public void eliminar(Integer id) {

	}


}
