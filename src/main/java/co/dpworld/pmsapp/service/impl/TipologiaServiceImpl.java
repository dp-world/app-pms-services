/**
 * 
 */
package co.dpworld.pmsapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.Tipologia;
import co.dpworld.pmsapp.service.ITipologiaService;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que contiene la implementacion del servicio de tipologias documentales
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@Service
public class TipologiaServiceImpl implements ITipologiaService {

	@Autowired
	private RidcAdapter ridcAdapter;

	@Override
	public Tipologia guardar(Tipologia t) {
		return null;
	}

	@Override
	public Tipologia modificar(Tipologia t) {
		return null;
	}

	@Override
	public Tipologia leer(Integer id) {
		return null;
	}

	@Override
	public List<Tipologia> listar() {
		List<Tipologia> listaTipologias = new ArrayList<>();

		/* integracion con oracle wcc para obtener listas de una vista */
		List<DataObject> listaTipologiasWCC = ridcAdapter.obtenerValoresVistas("vistaTipoDocumentos",
				"tipoDocumento");
		for (DataObject fila : listaTipologiasWCC) {
			listaTipologias.add(new Tipologia(fila.get("docCode"), fila.get("docDescription")));
		}
		return listaTipologias;
	}

	@Override
	public void eliminar(Integer id) {

	}

}
