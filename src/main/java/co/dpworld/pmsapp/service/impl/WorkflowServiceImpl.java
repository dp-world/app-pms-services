/**
 * 
 */
package co.dpworld.pmsapp.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.ActualizarPasoRq;
import co.dpworld.pmsapp.model.AnexoRq;
import co.dpworld.pmsapp.model.Archivo;
import co.dpworld.pmsapp.model.DocumentoRp;
import co.dpworld.pmsapp.model.DocumentoRq;
import co.dpworld.pmsapp.model.InformacionUsuarioRp;
import co.dpworld.pmsapp.model.MetadataClaveValor;
import co.dpworld.pmsapp.model.PasoRq;
import co.dpworld.pmsapp.service.ICreacionDocumento;
import co.dpworld.pmsapp.service.IUsuarioService;
import co.dpworld.pmsapp.util.wf.UtilEmail;
import co.dpworld.pmsapp.util.wf.UtilWorkflowActualizacion;
import co.dpworld.pmsapp.util.wf.UtilWorkflowCambioNomenclatura;
import co.dpworld.pmsapp.util.wf.UtilWorkflowCreacion;
import co.dpworld.pmsapp.util.wf.UtilWorkflowGeneral;
import co.dpworld.pmsapp.util.wf.UtilWorkflowObsoletizacion;
import co.dpworld.pmsapp.util.wf.UtilWorkflowReactivacion;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que contiene la implementacion del servicio de flujo de trabajo
 * creacion de documentos
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@Service
public class WorkflowServiceImpl implements ICreacionDocumento {

	Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);


	
	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private Environment env;

	@Autowired
	private UtilWorkflowGeneral utilWorkflowGeneral;

	@Autowired
	private UtilWorkflowCreacion utilWorklowDocumentos;

	@Autowired
	private UtilWorkflowActualizacion utilWorklowActualizacion;

	@Autowired
	private UtilWorkflowObsoletizacion utilWokflowObsoletizacion;

	@Autowired
	private UtilWorkflowReactivacion utilWorkflowReactivacion;

	@Autowired
	private UtilWorkflowCambioNomenclatura utilWorkflowCambioNomenclatura;
	
	@Autowired
	private UtilEmail utilEmail;
	
	@Autowired
	private IUsuarioService usuarioService;
	
	
	private static final String SUBJECT_CREACION = "Solicitud De Creación De Documento";
	private static final String SUBJECT_ACTUALIZACION = "Solicitud De Modificación de Documento";
	private static final String SUBJECT_OBSOLETIZACION = "Solicitud de Obsoletizacion de Documento";
	private static final String SUBJECT_REACTIVACION = "Solicitud de Reactivación de Documento";
	private static final String SUBJECT_NOMENCLATURA = "Solicitud de Cambio de Nomenclatura de Documento";
	
	

	@Override
	public boolean crearDocumento(DocumentoRq creacionDocumentoRq) throws Exception {

		// Obtener correo electornico del solicitante
		InformacionUsuarioRp informacionUsuario = usuarioService.obtenerInfoUsuario();
		String correoSolitante = informacionUsuario.getCorreoElectronico();
		
		// Tipo Solicitud = Creacion
		if (creacionDocumentoRq.getTipoSolicitud().equalsIgnoreCase("Creacion")) {
			if(utilWorklowDocumentos.validarSolicitudCreacion(creacionDocumentoRq)) 
			{
				
				String emailMetadata = "<p style=\"padding-left:15px\">\r\n" + 
						"        <span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Nombre Documento: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getTituloDocumento()+"</span>\r\n <br>" + 
						"<span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Tipo de Solicitud: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getTipoSolicitud()+"</span>\r\n <br>" + 
						"<span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Fecha de la Solicitud: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+new Date()+"</span>\r\n" +
						"</p>";
				
				String[] listEmailTo = new String[] {correoSolitante};
				utilEmail.enviarCorreoSolicitante(listEmailTo,
						                        SUBJECT_CREACION + " '"+creacionDocumentoRq.getTituloDocumento()+"'",
												"<p> Saludos, </p><p>Su solicitud está siendo evaluada para procesar su documento a la mayor brevedad posible.</p> "+emailMetadata);
				return true;
			}else {
				return false;
			}

			// Tipo Solicitud = Actualizacion
		} else if (creacionDocumentoRq.getTipoSolicitud().equalsIgnoreCase("Actualizacion")) {
			if(utilWorklowActualizacion.validarSolicitudActualizacion(creacionDocumentoRq))
			{
				String emailMetadata = "<p style=\"padding-left:15px\">\r\n" + 
						"        <span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Nombre Documento: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getNombreDocumento()+"</span>\r\n" + 
						"<span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Tipo de Solicitud: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getTipoSolicitud()+"</span>\r\n" + 
						"</p>";
				
				String[] listEmailTo = new String[] {correoSolitante};
				utilEmail.enviarCorreoSolicitante(listEmailTo,
						                         SUBJECT_ACTUALIZACION + " '"+creacionDocumentoRq.getNombreDocumento()+"'",
						                         "<p> Saludos, </p><p>Su solicitud está siendo evaluada para enviarle el documento solicitado a la mayor brevedad posible.</p> "+emailMetadata);
				return true;
			}else {
				return false ;
			}

			// Tipo Solicitud = Obsoletizacion
		} else if (creacionDocumentoRq.getTipoSolicitud().equalsIgnoreCase("Obsoletizacion")) {
			if(utilWokflowObsoletizacion.validarSolicitudObsoletizacion(creacionDocumentoRq))
			{
				String emailMetadata = "<p style=\"padding-left:15px\">\r\n" + 
						"        <span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Nombre Documento: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getNombreDocumento()+"</span>\r\n" + 
						"<span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Tipo de Solicitud: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getTipoSolicitud()+"</span>\r\n" + 
						"</p>";
				
				String[] listEmailTo = new String[] {correoSolitante};
				utilEmail.enviarCorreoSolicitante(listEmailTo,
						                          SUBJECT_OBSOLETIZACION  + " '"+creacionDocumentoRq.getNombreDocumento()+"'"
						                         ,"<p> Saludos, </p><p>Su solicitud está siendo evaluada para Obsoletizar el documento a la mayor brevedad posible.</p> "+emailMetadata);
				return true;
			}else {
				return false ;
			}

			// Tipo Solicitud - Reactivacion
		} else if (creacionDocumentoRq.getTipoSolicitud().equalsIgnoreCase("Reactivacion")) {
			if(utilWorkflowReactivacion.tipoSolicitudReactivacion(creacionDocumentoRq))
			{
				String emailMetadata = "<p style=\"padding-left:15px\">\r\n" + 
						"        <span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Nombre Documento: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getNombreDocumento()+"</span>\r\n" + 
						"<span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Tipo de Solicitud: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getTipoSolicitud()+"</span>\r\n" + 
						"</p>";
				
				String[] listEmailTo = new String[] {correoSolitante};
				utilEmail.enviarCorreoSolicitante(listEmailTo,
						                          SUBJECT_REACTIVACION  + " '"+creacionDocumentoRq.getNombreDocumento()+"'",
						                          "<p> Saludos, </p><p>Su solicitud está siendo evaluada para reactivar el documento a la mayor brevedad posible.</p> "+emailMetadata);
				return true;
			}else {
				return false ;
			}

			// Tipo Solicitud - Cambio de nomenclatura
		} else if (creacionDocumentoRq.getTipoSolicitud().equalsIgnoreCase("Nomenclatura")) {
			if(utilWorkflowCambioNomenclatura.tipoSolicitudNomenclatura(creacionDocumentoRq))
			{
				String emailMetadata = "<p style=\"padding-left:15px\">\r\n" + 
						"        <span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Nombre Documento: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getNombreDocumento()+"</span>\r\n" + 
						"<span style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:8pt;font-weight:bold\">\r\n" + 
						"                <b>Tipo de Solicitud: </b></span>\r\n" + 
						"        <span style=\"font-family:Arial,Helvetica,sans-serif;font-size:9pt;font-weight:normal\">\r\n" + 
						"                "+creacionDocumentoRq.getTipoSolicitud()+"</span>\r\n" + 
						"</p>";
				
				String[] listEmailTo = new String[] {correoSolitante};
				utilEmail.enviarCorreoSolicitante(listEmailTo,
						                          SUBJECT_NOMENCLATURA  + " '"+creacionDocumentoRq.getNombreDocumento()+"'"
						                          ,"<p> Saludos, </p><p>Su solicitud está siendo evaluada para gestionar el documento a la mayor brevedad posible.</p> "+emailMetadata);
				return true;
			}else {
				return false;
			}
		} else {
			logger.error("EL TIPO DE SOLICITUD '{}' NO ES VALIDO PARA INICAR UN FLUJO DE TRABAJO",creacionDocumentoRq.getTipoSolicitud());
			return false;
		}
	}

	@Override
	public DocumentoRp listarMetadataPasoWorkflow(String dWfStepID, String dID) throws Exception {

		// 1. Obtener el username del JWT
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();

		// 2. integracion con oracle wcc para obtener informacion (metadata) de cada
		// flujo
		DataObject docInfo = ridcAdapter.obtenerInformacionDeUnFlujo(currentPrincipalName, "DOC_INFO", dWfStepID, dID)
				.get(0);

		// 3. Mapear la respuesta del resulset DOC INFO al objeto de respuesta del
		// servicio
		DocumentoRp creacionDocumentoRp = utilWorklowDocumentos.mapearDatosCreacionDocumento(docInfo);

		return creacionDocumentoRp;
	}

	@Override
	public boolean enviarPaso(PasoRq pasoDocumentoRq, String dID) throws Exception {

		boolean resultadoOperacion = false;
		if (pasoDocumentoRq.getTipoDeAccion() != null) {

			// 1. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();

			// 2. Validar que accion se debe realizar (GUARDAR, APROBAR, RECHAZAR)
			switch (pasoDocumentoRq.getTipoDeAccion()) {
			case GUARDAR:

				break;

			case APROBAR:
				resultadoOperacion = ridcAdapter.aprobarFlujo(dID, currentPrincipalName);
				break;

			case RECHAZAR:
				String razonRechazo = pasoDocumentoRq.getRazonRechazo();
				if (razonRechazo == null || razonRechazo.isEmpty()) {
					throw new Exception("Se debe enviar la razon del rechazo para rechazar el flujo.");
				}
				resultadoOperacion = ridcAdapter.rechazar(dID, currentPrincipalName, razonRechazo);
				break;
			default:
				break;
			}

		} else {
			throw new Exception("Se debe enviar el tipo de accion a realizar: (GUARDAR, APROBAR, RECHAZAR)");
		}

		return resultadoOperacion;
	}

	@Override
	public Archivo descargarArchivo(String id) {
		return ridcAdapter.obtenerArchivo(id);
	}

	@Override
	public boolean actualizarPaso(ActualizarPasoRq actualizarPasoRq, String id, String dDocName, String dRevLabel,
			String dSecurityGroup, String idPaso) throws Exception {

		// 1. Mapear metadata de request a metadata Oracle WCC
		List<MetadataClaveValor> listaMetadataClaveValor = utilWorkflowGeneral

				.mapeoMetadataActualizacion(actualizarPasoRq);
		// Se valida si es necesario la generacion de la nomenclatura del documento
		utilWorkflowCambioNomenclatura.validacionGeneracionNomenclatura(actualizarPasoRq, idPaso,
				listaMetadataClaveValor);

		// Se revisan los ids de pasos para asignacion automatica de metadata
		utilWorkflowGeneral.revisionIdPasoAsignacionMetadataAutomatica(idPaso, listaMetadataClaveValor,actualizarPasoRq);

		// Se valida si es necesario reemplazar el documento existente en el workflow
		utilWorkflowGeneral.validacionRemplazoDocumento(actualizarPasoRq, id, dDocName);

		return ridcAdapter.actualizarMetadata(listaMetadataClaveValor, id, dDocName, dRevLabel, dSecurityGroup);
	}

	@Override
	public boolean anexarDocumento(AnexoRq anexoRq) throws IOException {

		String rutaTemporal = env.getProperty("wcc.ruta.temporal");
		try {
			// 1. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();

			// 2. Convertir base64 a File

			File archivo = new File(rutaTemporal + "\\" + anexoRq.getTituloDocumento());
			FileUtils.writeByteArrayToFile(archivo, anexoRq.getDocumentoAnexo());

			// 2. Registrar el documento anexo en Oracle WCC
			String dIDDestino = ridcAdapter.subirDocumento2(new ArrayList<>(), anexoRq.getTituloDocumento(),
					anexoRq.getTituloDocumento(), "Documento_DP-World", "Processes", "ProfileSupport", archivo,
					currentPrincipalName);

			// 3. Anexar documento registrado al documento principal
			return ridcAdapter.anexarDocumento(anexoRq.getIdDocumentoPrincipal(), dIDDestino);

		} finally {
			// Borrar documentos
			utilWorkflowGeneral.borrarDocumentosTemporales(rutaTemporal);
		}
	}

	@Override
	public Archivo descargarAnexo(String dDocName) {
		return ridcAdapter.obtenerAnexo(dDocName);
	}

}
