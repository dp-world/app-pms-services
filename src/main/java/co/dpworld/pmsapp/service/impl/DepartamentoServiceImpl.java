/**
 * 
 */
package co.dpworld.pmsapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.Departamento;
import co.dpworld.pmsapp.service.IDepartamentoService;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que contiene la implementacion del servicio de departamentos y/o areas
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@Service
public class DepartamentoServiceImpl implements IDepartamentoService{

	@Autowired
	private RidcAdapter ridcAdapter;
	
	@Override
	public Departamento guardar(Departamento t) {
		return null;
	}

	@Override
	public Departamento modificar(Departamento t) {
		return null;
	}

	@Override
	public Departamento leer(Integer id) {
		return null;
	}

	@Override
	public List<Departamento> listar() {

		List<Departamento> listaDepartamentos = new ArrayList<>();
		
		/* integracion con oracle wcc para obtener listas de una vista */		
		List<DataObject> listaDoDepartamentos = ridcAdapter.obtenerValoresVistas("vistaDepartamento","Departamento");
		for(DataObject fila :  listaDoDepartamentos)
		{
			listaDepartamentos.add(new Departamento(fila.get("Codigo"),fila.get("Descripcion")));
		}
		return listaDepartamentos;
	}

	@Override
	public void eliminar(Integer id) {
		
	}

}
