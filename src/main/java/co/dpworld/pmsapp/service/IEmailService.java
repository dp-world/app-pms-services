package co.dpworld.pmsapp.service;

import javax.mail.MessagingException;

/**
 * Interfaz que define operaciones para email
 * @author Sergio Arias
 * @date 11/04/2020
 *
 */
public interface IEmailService {

	void enviarEmail(String[] listEmailTo,String subject,String messageHtml) throws MessagingException;

}
