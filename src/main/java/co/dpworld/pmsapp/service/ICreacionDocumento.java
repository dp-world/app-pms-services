/**
 * 
 */
package co.dpworld.pmsapp.service;

import java.io.IOException;

import javax.mail.MessagingException;

import co.dpworld.pmsapp.model.ActualizarPasoRq;
import co.dpworld.pmsapp.model.AnexoRq;
import co.dpworld.pmsapp.model.Archivo;
import co.dpworld.pmsapp.model.DocumentoRp;
import co.dpworld.pmsapp.model.DocumentoRq;
import co.dpworld.pmsapp.model.PasoRq;

/**
 * Interfaz que define los metodos para el servicio creacion de documentos
 * 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
public interface ICreacionDocumento {

	/**
	 * Metodo que realiza el check-in del documento en Oracle WCC para iniciar el flujo de creacion de documentos
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param creacionDocumentoRq
	 * @return
	 */
	boolean crearDocumento(DocumentoRq creacionDocumentoRq) throws IOException,MessagingException,Exception;
	
	/**
	 * Metodo que retorna la metadato de un paso de un workflow existente en oracle wcc
	 * @author Sergio Arias
	 * @date 21/11/2019
	 * @param dWfStepID Id del paso donde se encuentra el workflow
	 * @param dID Id del flujo actual en oracle wcc
	 * @return Lista con metadata resulset DOC_INFO
	 */
	DocumentoRp listarMetadataPasoWorkflow(String dWfStepID, String dID) throws Exception;
	
	
	/**
	 * Metodo que guarda la informacion de un flujo en Oracle WCC y gestiona la accion (Rechazo y/o aprobacion)
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param creacionDocumentoRp 
	 * @return
	 * @throws Exception
	 */
	boolean enviarPaso(PasoRq pasoDocumentoRq, String dID) throws Exception;

	/**
	 * Metodo para descargar un archivo por id
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param id
	 * @return
	 */
	Archivo descargarArchivo(String id);

	/**
	 * Metodo que actualiza la informacion/metadata de un documento asociado a un flujo
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param actualizarPasoRq
	 * @param id
	 * @param dDocName
	 * @param dRevLabel
	 * @param dSecurityGroup
	 * @return
	 * @throws Exception 
	 */
	boolean actualizarPaso(ActualizarPasoRq actualizarPasoRq, String id, String dDocName, String dRevLabel,
			String dSecurityGroup,String idPaso) throws IOException, Exception;

	/**
	 * Metodo que asocia un anexo a un documento existente en Oracle WCC
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param anexoRq
	 * @return
	 * @throws IOException 
	 */
	boolean anexarDocumento(AnexoRq anexoRq) throws IOException;

	/**
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param dDocName
	 * @return
	 */
	Archivo descargarAnexo(String dDocName);
}
