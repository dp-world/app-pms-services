package co.dpworld.pmsapp.service;

import co.dpworld.pmsapp.model.RazonCreacion;

/**
 * Interfaz que define las operaciones de la razon de creacion
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
public interface IRazonCreacionService extends ICRUD<RazonCreacion> {

}
