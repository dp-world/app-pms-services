
package co.dpworld.pmsapp.service;


import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Servicio que gestiona la autenticacion de los recursos 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
@Service
public class RestAuthService {

	/**
	 * Metodo que gestiona el acceso de los metodos de la API por ROL
	 * @author Sergio Arias
	 * @date 7/11/2019
	 * @param path Contiene el metodo del recurso de el cual se verifica el acceso
	 * @return Verdadero si tiene acceso y Falso si no tiene acceso al recurso
	 */
	public boolean hasAccess(String path) {
		boolean rpta = false;

		String metodoRol = "";

		// /listar
		switch (path) {
		case "listar":
			metodoRol = "ADMIN";
			break;

		case "listarId":
			metodoRol = "ADMIN,USER,DBA";
			break;
		}

		String metodoRoles[] = metodoRol.split(",");
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			System.out.println(authentication.getName());
			
			for (GrantedAuthority auth : authentication.getAuthorities()) {
				String rolUser = auth.getAuthority();
				System.out.println(rolUser);
				
				for (String rolMet : metodoRoles) { 
					if (rolUser.equalsIgnoreCase(rolMet)) {
						rpta = true;
					}
				}
			}
		}
		return rpta;
	}
}

