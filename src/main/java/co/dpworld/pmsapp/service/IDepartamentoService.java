package co.dpworld.pmsapp.service;

import co.dpworld.pmsapp.model.Departamento;


/**
 * Interfaz que define las operaciones de departamentos
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
public interface IDepartamentoService extends ICRUD<Departamento> {

}
