package co.dpworld.pmsapp.service;

import co.dpworld.pmsapp.model.Tipologia;

/**
 * Interfaz que define las operaciones de tipologias
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
public interface ITipologiaService extends ICRUD<Tipologia> {

}
