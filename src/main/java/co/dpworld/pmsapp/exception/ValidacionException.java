/**
 * 
 */
package co.dpworld.pmsapp.exception;

/**
 * @author Sergio Arias
 * @date 20/04/2020
 *
 */
public class ValidacionException extends Exception {

	public ValidacionException(String message) {
		super(message);
	}

}
