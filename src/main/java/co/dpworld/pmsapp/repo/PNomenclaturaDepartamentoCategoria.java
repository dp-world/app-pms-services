/**
 * 
 */
package co.dpworld.pmsapp.repo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;

/**
 * Tabla de base de datos que contiene las secuencias de nomenclatura para las
 * categorias o tipo de documentos
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@ApiModel
@Entity
@IdClass(CompositeKeyNomenclatura.class)
@Table(name = "parametrica_nomenclatura_dep_cat")
public class PNomenclaturaDepartamentoCategoria {

	/** codigo de la categoria de documento */
	@Id
	@NotNull(message = "El campo codigoCategoria no puede ser nulo y/o vacio")
	@Column(name = "codigo_categoria", nullable = false, length = 100)
	private String codigoCategoria;

	/** codigo del departamento */
	@Id
	@NotNull(message = "El campo codigoDepartamento no puede ser nulo y/o vacio")
	@Column(name = "codigo_departamento", nullable = false, length = 100)
	private String codigoDepartamento;

	/** secuencia de nomenclatura para el departamento y categoria asociado */
	@Column(name = "secuencia_nomenclatura", nullable = false, length = 100)
	private int secuenciaNomenclaturaDepCat;

	/**
	 * Metodo get para codigoDepartamento
	 * 
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @return Retorna codigoDepartamento
	 */
	public String getCodigoDepartamento() {
		return codigoDepartamento;
	}

	/**
	 * Metodo set para codigoDepartamento
	 * 
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @param setea codigoDepartamento
	 */
	public void setCodigoDepartamento(String codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}

	/**
	 * Metodo get para codigoCategoria
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna codigoCategoria
	 */
	public String getCodigoCategoria() {
		return codigoCategoria;
	}

	/**
	 * Metodo set para codigoCategoria
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param setea codigoCategoria
	 */
	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	/**
	 * Metodo get para secuenciaNomenclaturaDepCat
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna secuenciaNomenclaturaDepCat
	 */
	public int getSecuenciaNomenclaturaDepCat() {
		return secuenciaNomenclaturaDepCat;
	}

	/**
	 * Metodo set para secuenciaNomenclaturaDepCat
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param setea secuenciaNomenclaturaDepCat
	 */
	public void setSecuenciaNomenclaturaDepCat(int secuenciaNomenclatura) {
		this.secuenciaNomenclaturaDepCat = secuenciaNomenclatura;
	}

}
