/**
 * 
 */
package co.dpworld.pmsapp.repo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;

/**
 * Tabla de base de datos que contiene las secuencias de nomenclatura para la
 * asociacion de departamento y categoria
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@ApiModel
@Entity
@Table(name = "parametrica_nomenclatura_cat")
public class PNomenclaturaCategoria {

	/** codigo del departamento */
	@Id
	@NotNull(message = "El campo codigoCategoria no puede ser nulo y/o vacio")
	@Column(name = "codigo_categoria", nullable = false, length = 100)
	private String codigoCategoria;

	/** secuencia de nomenclatura para el departamento y categoria asociado */
	@Column(name = "secuencia_nomenclatura", nullable = false, length = 100)
	private int secuenciaNomenclaturaCat;

	/**
	 * Metodo get para codigoCategoria
	 * 
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @return Retorna codigoCategoria
	 */
	public String getCodigoCategoria() {
		return codigoCategoria;
	}

	/**
	 * Metodo set para codigoCategoria
	 * 
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @param setea codigoCategoria
	 */
	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	/**
	 * Metodo get para codigoCategoria
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna codigoCategoria
	 */
	public String getDepartamento() {
		return codigoCategoria;
	}

	/**
	 * Metodo set para codigoCategoria
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param setea codigoCategoria
	 */
	public void setDepartamento(String departamento) {
		this.codigoCategoria = departamento;
	}

	/**
	 * Metodo get para secuenciaNomenclaturaCat
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna secuenciaNomenclaturaCat
	 */
	public int getSecuenciaNomenclaturaCat() {
		return secuenciaNomenclaturaCat;
	}

	/**
	 * Metodo set para secuenciaNomenclaturaCat
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param setea secuenciaNomenclaturaCat
	 */
	public void setSecuenciaNomenclaturaCat(int secuenciaNomenclatura) {
		this.secuenciaNomenclaturaCat = secuenciaNomenclatura;
	}

}
