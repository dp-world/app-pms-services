/**
 * 
 */
package co.dpworld.pmsapp.repo;

import java.io.Serializable;

/**
 * Clase que contiene las llaves para parametrica nomenclatura
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
public class CompositeKeyNomenclatura implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoDepartamento;
    private String codigoCategoria;
	/**
	 * Metodo get para codigoDepartamento
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna codigoDepartamento
	 */
	public String getCodigoDepartamento() {
		return codigoDepartamento;
	}
	/**
	 * Metodo set para codigoDepartamento
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param setea codigoDepartamento
	 */
	public void setCodigoDepartamento(String codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}
	/**
	 * Metodo get para codigoCategoria
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna codigoCategoria
	 */
	public String getCodigoCategoria() {
		return codigoCategoria;
	}
	/**
	 * Metodo set para codigoCategoria
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param setea codigoCategoria
	 */
	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}
    
    
}