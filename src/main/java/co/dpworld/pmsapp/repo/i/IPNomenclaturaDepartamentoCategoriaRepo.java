/**
 * 
 */
package co.dpworld.pmsapp.repo.i;

import org.springframework.data.jpa.repository.JpaRepository;

import co.dpworld.pmsapp.repo.CompositeKeyNomenclatura;
import co.dpworld.pmsapp.repo.PNomenclaturaDepartamentoCategoria;

/**
 * Interfaz que contiene las operaciones CRUD para entidad parametrica_nomenclatura
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
public interface IPNomenclaturaDepartamentoCategoriaRepo extends JpaRepository<PNomenclaturaDepartamentoCategoria, CompositeKeyNomenclatura> {
	

}