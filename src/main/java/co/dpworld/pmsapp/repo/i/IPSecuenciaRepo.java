/**
 * 
 */
package co.dpworld.pmsapp.repo.i;

import org.springframework.data.jpa.repository.JpaRepository;

import co.dpworld.pmsapp.repo.PSecuencia;

/**
 * Interfaz que contiene las operaciones CRUD para entidad parametrica de
 * secuencias
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
public interface IPSecuenciaRepo extends JpaRepository<PSecuencia, String> {

}