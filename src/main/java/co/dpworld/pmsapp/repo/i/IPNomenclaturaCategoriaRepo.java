/**
 * 
 */
package co.dpworld.pmsapp.repo.i;

import org.springframework.data.jpa.repository.JpaRepository;

import co.dpworld.pmsapp.repo.PNomenclaturaCategoria;

/**
 * Interfaz que contiene las operaciones CRUD para entidad parametrica nomenclatura categoria
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
public interface IPNomenclaturaCategoriaRepo extends JpaRepository<PNomenclaturaCategoria, String> {
	

}