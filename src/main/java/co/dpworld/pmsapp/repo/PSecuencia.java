/**
 * 
 */
package co.dpworld.pmsapp.repo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;

/**
 * Tabla de base de datos que contiene las secuencias de documentos
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@ApiModel
@Entity
@Table(name = "parametrica_secuencias")
public class PSecuencia {

	/** codigo de la secuencia */
	@Id
	@NotNull(message = "El campo codigo de la secuencia no puede ser nulo y/o vacio")
	@Column(name = "codigo_secuencia", nullable = false, length = 100)
	private String codigoSecuencia;

	/** secuencia de documento */
	@Column(name = "secuencia_doc", nullable = false, length = 100)
	private int secuencia_doc;

	/**
	 * Metodo get para codigoSecuencia
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @return Retorna codigoSecuencia
	 */
	public String getCodigoSecuencia() {
		return codigoSecuencia;
	}

	/**
	 * Metodo set para codigoSecuencia
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param setea codigoSecuencia
	 */
	public void setCodigoSecuencia(String codigoSecuencia) {
		this.codigoSecuencia = codigoSecuencia;
	}

	/**
	 * Metodo get para secuencia_doc
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @return Retorna secuencia_doc
	 */
	public int getSecuencia_doc() {
		return secuencia_doc;
	}

	/**
	 * Metodo set para secuencia_doc
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param setea secuencia_doc
	 */
	public void setSecuencia_doc(int secuencia_doc) {
		this.secuencia_doc = secuencia_doc;
	}

}
