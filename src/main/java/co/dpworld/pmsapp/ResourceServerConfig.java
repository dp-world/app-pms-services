/**
 * 
 */
package co.dpworld.pmsapp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;



/**
 * Clase que gestiona los permisos a los recursos del proyecto
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Autowired
    private ResourceServerTokenServices tokenServices;
	
    @Value("${security.jwt.resource-ids}")
    private String resourceIds;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceIds).tokenServices(tokenServices);
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
                http
                .exceptionHandling().authenticationEntryPoint(new AuthException())
                .and()
                .requestMatchers()
                .and()
                .authorizeRequests()
                .antMatchers("/v2/api-docs/**" ).permitAll() // Permitir publicamente acceso a la documentacion de la API
                .antMatchers("/usuarios/**").authenticated() // Permitir acceso si esta autenticado
                .antMatchers("/departamentos/**").authenticated() // Permitir acceso si esta autenticado
                .antMatchers("/tipologias/**").authenticated() // Permitir acceso si esta autenticado
                .antMatchers("/razones/**").authenticated() // Permitir acceso si esta autenticado
                //.antMatchers("/flujos/descargar").permitAll()
                .antMatchers("/flujos/**").permitAll() //Permitir acceso si esta autenticado
                .antMatchers("/plantillas/**").permitAll() 
                .antMatchers("/cola/**").authenticated()
                .antMatchers("/documentos/**").authenticated()
                .antMatchers("/usuarios/**").authenticated();
                
    }    

}
