/**
 * 
 */
package co.dpworld.pmsapp.apiwcc;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import co.dpworld.pmsapp.apiwcc.connection.RIDCIdcConnection;
import co.dpworld.pmsapp.exception.ValidacionException;
import co.dpworld.pmsapp.model.Archivo;
import co.dpworld.pmsapp.model.MetadataClaveValor;
import oracle.stellent.ridc.IdcClient;
import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.IdcClientManager;
import oracle.stellent.ridc.IdcContext;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.DataResultSet;
import oracle.stellent.ridc.model.TransferFile;
import oracle.stellent.ridc.protocol.ServiceResponse;

/**
 * Clase que contiene metodos de utilidad para integracion con el ECM Oracle
 * WebCenter Content - API RIDC JAVA
 * 
 * @author Sergio Arias
 * @date 10/11/2019
 *
 */
@Component
public class RidcAdapter {

	Logger logger = LoggerFactory.getLogger(RidcAdapter.class);
	
	public static String sessionID = "";

	@Autowired
	private Environment env;

	/** Mensaje que indica que un documento ha sido subido exitosamente */
	private static String documentoOk = "Se ha comprobado correctamente en el elemento de contenido";
	private static String documentoOkIng = "Successfully checked in content item";

	/**
	 * Metodo encargado de validar las credenciales de un usuario para determinar si
	 * es posible su inicio de sesion (En Oracle WebCenter Content)
	 * 
	 * @author Sergio Arias
	 * @date 10/11/2019
	 * @param usuario    Usuario
	 * @param contrasena Contraseña del usuario
	 * @return Verdado si los datos son correctos y Falso si son incorrectos
	 * @throws AuthenticationException
	 */
	public boolean validarCredenciales(String usuario, String contrasena) throws AuthenticationException {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();

			dataBinder.putLocal("IdcService", "CHECK_USER_CREDENTIALS");
			dataBinder.putLocal("userName", usuario);
			dataBinder.putLocal("userExtendedInfo", "0"); // Obtener informacion extendida
			dataBinder.putLocal("getUserInfo", "0"); // Obtener informacion del usuario
			dataBinder.putLocal("authenticateUser", "0"); // Autenticar el usuario
			dataBinder.putLocal("userPassword", contrasena);
			dataBinder.putLocal("hasSecurityInfo", "0");

			logger.info("PETICION -> ");
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("RESPUESTA -> ");
			logger.debug(con.binder2String(responseBinder));
			logger.debug("RESUL SETS -> ");
			logger.debug(con.result2String(responseBinder));

			// Validar si es error de credenciales
			// throw new BadCredentialsException("Error de credenciales usuario"); //
			// Capturar error de credenciales

			// Si isPromptLogin es = 1 la autenticacion es satisfactoria

			//

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage());
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage());
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return true;
	}

	/**
	 * Metodo que llama el servicio GET_SCHEMA_VIEW_VALUES para obtener valores
	 * configurados en vistas (En Oracle WebCenter Content)
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @param nombreEsquema Nombre del esquema (vista) del cual se desea consultar
	 *                      informacion
	 * @param resultSet     Nombre del resulset para obtener los datos (Nombre de la
	 *                      tabla)
	 * @return Lista de valores configurados en la vista
	 */
	public List<DataObject> obtenerValoresVistas(String nombreEsquema, String resultSet) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "GET_SCHEMA_VIEW_VALUES");
			dataBinder.putLocal("schViewName", nombreEsquema);

			logger.info("# PETICION GET_SCHEMA_VIEW_VALUES # nombreEsquema: {} , resulSet: {} ",nombreEsquema, resultSet);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			// logger.info("RESPUESTA -> ");
			// logger.info(con.binder2String(responseBinder));
			logger.debug("# RESUL SETS " + resultSet + " # ");
			logger.debug(con.result2String(responseBinder));

			return responseBinder.getResultSet(resultSet).getRows();

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que llama el servicio CHECKIN_UNIVERSAL para registrar documento en
	 * Oracle WebCenter Content
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @param metadataClaveValor Lista con la metadata personalizada del documento
	 * @param docTitle           Titulo del documento
	 * @param docName            Nombre del documento
	 * @param docType            Doctype del documento
	 * @param securityGroup      Grupo de seguridad del documento
	 * @param idcProfile         Perfil del documento
	 * @param archivo            Archivo en el file system que se va a subir
	 * @param autor              Autor que sube el archivo
	 * @return Verdadero si fue exitoso el cargue y Falso si ocurre algun error
	 */
	public String subirDocumento(List<MetadataClaveValor> metadataClaveValor, String docTitle, String dOriginalName,
			String docType, String securityGroup, String idcProfile, File archivo, String autor) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
			dataBinder.putLocal("dDocTitle", docTitle); // Titulo
			dataBinder.putLocal("dOriginalName", dOriginalName); // Nombre Original
			dataBinder.putLocal("dDocType", docType); // Tipo de documento
			dataBinder.putLocal("dSecurityGroup", securityGroup); // Grupo de seguridad
			dataBinder.putLocal("xIdcProfile", idcProfile); // Perfil del documento
			dataBinder.putLocal("dDocAuthor", autor); // Autor del documento

			// Metadata personalizada del documento
			for (MetadataClaveValor claveValor : metadataClaveValor) {
				dataBinder.putLocal(claveValor.getClave(), claveValor.getValor());
			}
			// binder.putLocal("dID", did); // Se utiliza para versionar existente
			// binder.putLocal("xFolderID", folderId);

			// Archivo a registrar
			dataBinder.addFile("primaryFile", new TransferFile(archivo));

			logger.info("# PETICION CHECKIN_UNIVERSAL # dDocTitle: {} ", docTitle);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA CHECKIN_UNIVERSAL # ");
			logger.debug(con.binder2String(responseBinder));

			// if
			// (serviceResponse.getResponseAsBinder().getLocal("StatusMessage").contains(documentoOk))
			// {

			return serviceResponse.getResponseAsBinder().getLocal("dID");
			// } else {
			// return null;
			// }

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio CHECKIN_UNIVERSAL de Oracle WCC para versionar
	 * documento
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param docTitle
	 * @param dOriginalName
	 * @param archivo
	 * @param idActual
	 * @param dDocName
	 * @return
	 */
	public boolean versionarDocumento(String docTitle, String dOriginalName, File archivo, String idActual,
			String dDocName) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
			dataBinder.putLocal("dID", idActual);
			dataBinder.putLocal("dDocName", dDocName);
			dataBinder.putLocal("dDocTitle", docTitle); // Titulo
			dataBinder.putLocal("dOriginalName", dOriginalName); // Nombre Original
			// dataBinder.putLocal("dDocType", docType); // Tipo de documento
			// dataBinder.putLocal("dSecurityGroup", securityGroup); // Grupo de seguridad
			// dataBinder.putLocal("xIdcProfile", idcProfile); // Perfil del documento

			// Metadata personalizada del documento
			// for (MetadataClaveValor claveValor : metadataClaveValor) {
			// dataBinder.putLocal(claveValor.getClave(), claveValor.getValor());
			// }
			// binder.putLocal("dID", did); // Se utiliza para versionar existente
			// binder.putLocal("xFolderID", folderId);

			// Archivo a registrar
			dataBinder.addFile("primaryFile", new TransferFile(archivo));

			logger.info("# PETICION CHECKIN_UNIVERSAL 2 # dID {} , dDocName: {} , dDocTitle {} ",idActual,dDocName,docTitle);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA CHECKIN_UNIVERSAL 2 # ");
			logger.debug(con.binder2String(responseBinder));

			if (serviceResponse.getResponseAsBinder().getLocal("StatusMessage").contains(documentoOk) || serviceResponse
					.getResponseAsBinder().getLocal("StatusMessage").contains("Successfully checked in content item")) {

				return true;
			} else {
				return false;
			}

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio CHECKIN_UNIVERSAL de Oracle WCC para versionar
	 * documento con metadata
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param docTitle
	 * @param dOriginalName
	 * @param archivo
	 * @param idActual
	 * @param dDocName
	 * @param metadataClaveValor
	 * @return
	 */
	public boolean versionarDocumentoConMetadata(String docTitle, String dOriginalName, File archivo, String idActual,
			String dDocName, List<MetadataClaveValor> metadataClaveValor,String autor) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
			dataBinder.putLocal("dID", idActual);
			dataBinder.putLocal("dDocName", dDocName);
			dataBinder.putLocal("dDocTitle", docTitle); // Titulo
			dataBinder.putLocal("dOriginalName", dOriginalName); // Nombre Original
			dataBinder.putLocal("dDocAuthor", autor); // Autor del documento
			// dataBinder.putLocal("dDocType", docType); // Tipo de documento
			// dataBinder.putLocal("dSecurityGroup", securityGroup); // Grupo de seguridad
			// dataBinder.putLocal("xIdcProfile", idcProfile); // Perfil del documento

			// Metadata personalizada del documento
			for (MetadataClaveValor claveValor : metadataClaveValor) {
				dataBinder.putLocal(claveValor.getClave(), claveValor.getValor());
			}
			// binder.putLocal("dID", did); // Se utiliza para versionar existente
			// binder.putLocal("xFolderID", folderId);

			// Archivo a registrar
			dataBinder.addFile("primaryFile", new TransferFile(archivo));

			logger.info("# PETICION CHECKIN_UNIVERSAL 2 # dID {} , dDocName: {} , dDocTitle {} ",idActual,dDocName,docTitle);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA CHECKIN_UNIVERSAL 2 # ");
			logger.debug(con.binder2String(responseBinder));

			if (serviceResponse.getResponseAsBinder().getLocal("StatusMessage").contains(documentoOk) || serviceResponse
					.getResponseAsBinder().getLocal("StatusMessage").contains("Successfully checked in content item")) {

				return true;
			} else {
				return false;
			}

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio CHECKIN_UNIVERSAL de Oracle WCC para registrar
	 * documento
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param metadataClaveValor
	 * @param docTitle
	 * @param dOriginalName
	 * @param docType
	 * @param securityGroup
	 * @param idcProfile
	 * @param archivo
	 * @param autor
	 * @return
	 */
	public String subirDocumento2(List<MetadataClaveValor> metadataClaveValor, String docTitle, String dOriginalName,
			String docType, String securityGroup, String idcProfile, File archivo, String autor) {

		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
			dataBinder.putLocal("dDocTitle", docTitle); // Titulo
			dataBinder.putLocal("dOriginalName", dOriginalName); // Nombre Original
			dataBinder.putLocal("dDocType", docType); // Tipo de documento
			dataBinder.putLocal("dSecurityGroup", securityGroup); // Grupo de seguridad
			dataBinder.putLocal("xIdcProfile", idcProfile); // Perfil del documento

			// Metadata personalizada del documento
			for (MetadataClaveValor claveValor : metadataClaveValor) {
				dataBinder.putLocal(claveValor.getClave(), claveValor.getValor());
			}
			// binder.putLocal("dID", did); // Se utiliza para versionar existente
			// binder.putLocal("xFolderID", folderId);

			// Archivo a registrar
			dataBinder.addFile("primaryFile", new TransferFile(archivo));

			logger.info("# PETICION CHECKIN_UNIVERSAL #  dDocTitle {} ", docTitle);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA CHECKIN_UNIVERSAL # ");
			logger.debug(con.binder2String(responseBinder));

			if (serviceResponse.getResponseAsBinder().getLocal("StatusMessage").contains(documentoOk)
					|| serviceResponse.getResponseAsBinder().getLocal("StatusMessage").contains(documentoOkIng)) {
				return serviceResponse.getResponseAsBinder().getLocal("dID");
			} else {
				return null;
			}

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio LOAD_WORKFLOW_QUEUE de Oracle WCC para obtener
	 * flujos en cola del usuario
	 * 
	 * @author Sergio Arias
	 * @param usuario   Nombre del usuario
	 * @param resultSet Nombre del resulset
	 * @return Lista de flujos en cola actuales
	 */
	public List<DataObject> obtenerInboxUsuario(String usuario, String resultSet) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"), env.getProperty(usuario));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "LOAD_WORKFLOW_QUEUE");

			logger.info("# PETICION LOAD_WORKFLOW_QUEUE # usuario {}, resulSet: {} ",usuario,resultSet);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, usuario);
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA LOAD_WORKFLOW_QUEUE # ");
			logger.debug(con.binder2String(responseBinder));

			List<DataObject> listaResultado = new ArrayList<>();
			// Se agrega informacion LocalData al resultado de la informacion
			if (responseBinder != null) {
				listaResultado.add(responseBinder.getLocalData());
				listaResultado = responseBinder.getResultSet(resultSet).getRows();

			}
			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio GET_WORKFLOW_INFO de Oracle WCC para obtener
	 * informacion de un workflow
	 * 
	 * @param usuario
	 * @param resultSet
	 * @param dWfStepID Id del paso del flujo (dWfStepID)
	 * @param dID       Id de la revision (dID)
	 * @return Lista con la metadata del resulset seleccionado correspondiente al
	 *         flujo
	 */
	public List<DataObject> obtenerInformacionDeUnFlujo(String usuario, String resultSet, String dWfStepID,
			String dID) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"), env.getProperty(usuario));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "GET_WORKFLOW_INFO");
			dataBinder.putLocal("dID", dID);
			dataBinder.putLocal("dWfStepID", dWfStepID);

			logger.info("# PETICION GET_WORKFLOW_INFO # usuario: {}, resulset: {}, dWfStepID: {}, dID: {}",usuario,resultSet,dWfStepID,dID);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, usuario);
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA GET_WORKFLOW_INFO # ");
			logger.debug(con.binder2String(responseBinder));

			// Se agrega informacion LocalData al resultado de la informacion
			List<DataObject> listaResultado = responseBinder.getResultSet(resultSet).getRows();
			listaResultado.add(responseBinder.getLocalData());
			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			}
			else if (idcce.getMessage().contains("The content item does not exist") ||
					idcce.getMessage().contains("Unable to execute service GET_WORKFLOW_INFO and function QworkflowForID")) {
				throw new RuntimeException("La solicitud que esta intentando abrir ya fue procesada",idcce);
			}else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que llama el servicio WORKFLOW_APPROVE de Oracle WCC para aprobar
	 * actividad de un workflow
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param dID     Id del flujo
	 * @param usuario Usuario que posee el flujo
	 * @return True si el flujo fue aprobado correctamente y False si el flujo no se
	 *         puede aprobar
	 */
	public boolean aprobarFlujo(String dID, String usuario) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"), env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "WORKFLOW_APPROVE");
			dataBinder.putLocal("dID", dID);

			logger.info("# PETICION WORKFLOW_APPROVE # usario {} , dID: {} ",env.getProperty("wcc.usuario.admin"),dID);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder,env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA WORKFLOW_APPROVE # ");
			logger.debug(con.binder2String(responseBinder));

			responseBinder.getLocalData();

			return true;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio GET_SEARCHS_RESULTS de Oracle WCC para realizar
	 * busquedas
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param query
	 * @param resulset
	 * @return
	 */
	public List<DataObject> buscar(String query, String resulset) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		

		String sIdcString = "";
		IdcClientManager clientManager = null;
		
		try {
		
			sIdcString = env.getProperty("wcc.auth");
			clientManager = new IdcClientManager();
			IdcClient idcClient = clientManager.createClient(sIdcString);
			IdcContext userContext = new IdcContext(env.getProperty("wcc.usuario.admin"), env.getProperty("wcc.usuario.pass"));

			dataBinder = idcClient.createBinder();
			dataBinder.putLocal("IdcService", "GET_SEARCH_RESULTS");
			dataBinder.putLocal("QueryText", query);

			logger.info("# PETICION GET_SEARCH_RESULTS # query: {} , resulset: {} ",query,resulset);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = idcClient.sendRequest(userContext,dataBinder);
	
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA GET_SEARCH_RESULTSget # ");
			logger.debug(con.binder2String(responseBinder));

			// Se agrega informacion LocalData al resultado de la informacion
			List<DataObject> listaResultado = responseBinder.getResultSet(resulset).getRows();
			if (!listaResultado.isEmpty()) {
				listaResultado.add(responseBinder.getLocalData());

			}

			sessionID = (userContext.getCookie(userContext.getSessionCookie())).getValue();
			System.out.println(sessionID);
			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	
	public List<DataObject> buscarExpirados(String query) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		

		String sIdcString = "";
		IdcClientManager clientManager = null;
		
		try {
		
			sIdcString = env.getProperty("wcc.auth");
			clientManager = new IdcClientManager();
			IdcClient idcClient = clientManager.createClient(sIdcString);
			IdcContext userContext = new IdcContext(env.getProperty("wcc.usuario.admin"), env.getProperty("wcc.usuario.pass"));

			dataBinder = idcClient.createBinder();
			dataBinder.putLocal("IdcService", "GET_EXPIRED");
			if(!query.equals("") && query != null) {
				dataBinder.putLocal("QueryText", query);
			}
			

			logger.info("# PETICION GET_EXPIRED # query {}",query);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = idcClient.sendRequest(userContext,dataBinder);
	
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA GET_EXPIRED # ");
			logger.debug(con.binder2String(responseBinder));

			// Se agrega informacion LocalData al resultado de la informacion
			List<DataObject> listaResultado = responseBinder.getResultSet("EXPIRED_LIST").getRows();
			if (!listaResultado.isEmpty()) {
				listaResultado.add(responseBinder.getLocalData());

			}

			sessionID = (userContext.getCookie(userContext.getSessionCookie())).getValue();
			System.out.println(sessionID);
			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio DOC_INFO de Oracle WCC para obtener la informacion
	 * al detalle de un documento
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param query
	 * @param dID
	 * @param dDocName
	 * @param resulset
	 * @return
	 */
	public List<DataObject> infoDoc(String dID, String dDocName, String resulset) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "DOC_INFO");
			dataBinder.putLocal("dID", dID);
			if (dDocName != null) {
				dataBinder.putLocal("dDocName", dDocName);
			}

			logger.info("# PETICION DOC_INFO # dDocName: {} , dID: {} , resulset: {}",dDocName,dID,resulset);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA DOC_INFO # ");
			logger.debug(con.binder2String(responseBinder));

			// Se agrega informacion LocalData al resultado de la informacion
			List<DataObject> listaResultado = new ArrayList<>();
			if(resulset != null) {
				listaResultado.add(responseBinder.getLocalData());
				listaResultado.addAll(responseBinder.getResultSet(resulset).getRows());
			}else {
				listaResultado.add(responseBinder.getLocalData());
			}
			

			
			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	
	public List<DataObject> infoDocSinLocalData(String dID, String dDocName, String resulset) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "DOC_INFO");
			dataBinder.putLocal("dID", dID);
			if (dDocName != null) {
				dataBinder.putLocal("dDocName", dDocName);
			}

			logger.info("# PETICION DOC_INFO # dDocName: {} , dID: {} , resulset: {}",dDocName,dID,resulset);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA DOC_INFO # ");
			logger.debug(con.binder2String(responseBinder));

			// Se agrega informacion LocalData al resultado de la informacion
			List<DataObject> listaResultado = new ArrayList<>();
			if(resulset != null) {
				listaResultado.addAll(responseBinder.getResultSet(resulset).getRows());
			}else {
				listaResultado.add(responseBinder.getLocalData());
			}
			

			
			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que llama el servicio WORKFLOW_REJECT de Oracle WCC para rechazar
	 * actividad de un workflow
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param dID
	 * @param usuario         Usuario que tiene la tarea del flujo
	 * @param wfRejectMessage Mensaje con la razon del rechazo
	 * @return True si el flujo es rechazado correctamente y False si el flujo no se
	 *         puede rechazar
	 */
	public boolean rechazar(String dID, String usuario, String wfRejectMessage) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"), env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "WORKFLOW_REJECT");
			dataBinder.putLocal("dID", dID);
			dataBinder.putLocal("wfRejectMessage", wfRejectMessage);

			logger.info("# PETICION WORKFLOW_REJECT # usuario: {} , wfRejectMessage: {} ",env.getProperty("wcc.usuario.admin"),wfRejectMessage);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA WORKFLOW_REJECT # ");
			logger.debug(con.binder2String(responseBinder));

			responseBinder.getLocalData();

			return true;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage());
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage());
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio GET_FILE de Oracle WCC para obtener un archivo
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param dID
	 * @return
	 */
	public Archivo obtenerArchivo(String dID) {
		DataBinder dataBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		File archivo = null;
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "GET_FILE");
			dataBinder.putLocal("dID", dID);
			// dataBinder.putLocal("allowInterrupt", "1");

			logger.info("# PETICION GET_FILE # dID: {}",dID);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			logger.debug("# RESPUESTA GET_FILE # ");

			/*
			 * FileOutputStream fos = new FileOutputStream("File.docx"); InputStream fis =
			 * serviceResponse.getResponseStream(); byte[] readData = new byte[1024]; int i
			 * = fis.read(readData);
			 * 
			 * while (i != -1) { fos.write(readData, 0, i); i = fis.read(readData); }
			 * serviceResponse.close();
			 */

			String nombreArchivo = con.getResponseStreamFilename();
			String mimeType = con.getResponseStreamMIMEType();
			InputStream inputStream = serviceResponse.getResponseStream();

			String rutaArchivo = env.getProperty("wcc.ruta.temporal") + "/File.docx";
			archivo = new File(rutaArchivo);

			FileUtils.copyInputStreamToFile(inputStream, archivo);

			byte[] bytesArchivo = Files.readAllBytes(archivo.toPath());

			Archivo objetoArchivo = new Archivo();
			objetoArchivo.setArchivo(bytesArchivo);
			objetoArchivo.setMimetype(mimeType);
			objetoArchivo.setTituloArchivo(nombreArchivo);
			logger.info("objetoArchivo: " + objetoArchivo.toString());

			return bytesArchivo != null ? objetoArchivo : null;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}

			if (archivo != null && archivo.exists()) {
				try {
					archivo.delete();
				} catch (Exception e) {
					logger.error("El documento no pudo ser eliminado.", e);
				}
			}
		}
	}

	/**
	 * Metodo que invoca servicio UPDATE_DOCINFO de Oracle WCC para actualizar
	 * informacion de un documento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param listaMetadataClaveValor
	 * @param id
	 * @param dDocName
	 * @param dRevLabel
	 * @param dSecurityGroup
	 * @return
	 */
	public boolean actualizarMetadata(List<MetadataClaveValor> metadataClaveValor, String id, String dDocName,
			String dRevLabel, String dSecurityGroup) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "UPDATE_DOCINFO");
			dataBinder.putLocal("dID", id); // Id
			dataBinder.putLocal("dDocName", dDocName); // Doc name
			dataBinder.putLocal("dRevLabel", dRevLabel); // Revision
			dataBinder.putLocal("dSecurityGroup", dSecurityGroup); // Grupo de seguridad

			// Metadata personalizada del documento
			for (MetadataClaveValor claveValor : metadataClaveValor) {
				dataBinder.putLocal(claveValor.getClave(), claveValor.getValor());
			}

			logger.info("# PETICION UPDATE_DOCINFO # dDocName: {}, dID: {} , Metadata: {} ",dDocName,id,metadataClaveValor);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA UPDATE_DOCINFO # ");
			logger.debug(con.binder2String(responseBinder));

			return true;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio ADD_RELATED_CONTENT de Oracle WCC para asociar
	 * anexo
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param idOrigen
	 * @param idDestino
	 * @return
	 */
	public boolean anexarDocumento(String idOrigen, String idDestino) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "ADD_RELATED_CONTENT");
			dataBinder.putLocal("dLinkTypeID", "3"); // Tipo de link
			dataBinder.putLocal("dID", idDestino); // Id del documento principal
			dataBinder.putLocal("addLinkID", idOrigen); // Id del documento que se anexa

			logger.info("# PETICION ADD_RELATED_CONTENT # dID: {}, addLinkID: {}",idDestino,idOrigen);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA ADD_RELATED_CONTENT # ");
			logger.debug(con.binder2String(responseBinder));

			return true;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio GET_USER_INFO de Oracle WCC para obtener
	 * informacion del usuario
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param idUsuario
	 * @return
	 */
	public List<DataObject> obtenerInfoUsuario(String idUsuario, String resulset) {

		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"), idUsuario);

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "GET_USER_INFO");

			logger.info("# PETICION GET_USER_INFO # idUsuario: {} , resulset: {}  ",idUsuario,resulset);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, idUsuario);
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA GET_USER_INFO # ");
			logger.debug(con.binder2String(responseBinder));

			List<DataObject> listaResultado = responseBinder.getResultSet(resulset).getRows();

			return listaResultado;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca el servicio CHECKOUT de Oracle WCC
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param id
	 * @return
	 * @throws ValidacionException 
	 */
	public boolean checkout(String id) throws ValidacionException {

		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "CHECKOUT");
			dataBinder.putLocal("dID", id);

			logger.info("# PETICION CHECKOUT # dID: {} ",id);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA CHECKOUT # ");
			logger.debug(con.binder2String(responseBinder));

			return true;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else if(idcce.getMessage().contains("The selected revision is not the most recent revision")){
				throw new ValidacionException("LA SOLICITUD NO PUEDE SER INICIADA DEBIDO A QUE EL DOCUMENTO YA TIENE UN FLUJO EN CURSO.");
			}else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} 
		// Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * Metodo que invoca servicio GET_FILE de Oracle WCC para obtener un archivo
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param dDocName
	 * @return
	 */
	public Archivo obtenerAnexo(String dDocName) {
		DataBinder dataBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		File archivo = null;
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),
					env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "GET_FILE");
			dataBinder.putLocal("dDocName", dDocName);
			dataBinder.putLocal("RevisionSelectionMethod", "Latest");
			// dataBinder.putLocal("allowInterrupt", "1");

			logger.info("# PETICION GET_FILE # dDocName: {} ",dDocName);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			logger.debug("# RESPUESTA GET_FILE # ");

			/*
			 * FileOutputStream fos = new FileOutputStream("File.docx"); InputStream fis =
			 * serviceResponse.getResponseStream(); byte[] readData = new byte[1024]; int i
			 * = fis.read(readData);
			 * 
			 * while (i != -1) { fos.write(readData, 0, i); i = fis.read(readData); }
			 * serviceResponse.close();
			 */

			String nombreArchivo = con.getResponseStreamFilename();
			String mimeType = con.getResponseStreamMIMEType();
			InputStream inputStream = serviceResponse.getResponseStream();

			String rutaArchivo = env.getProperty("wcc.ruta.temporal") + "/File.docx";
			archivo = new File(rutaArchivo);

			FileUtils.copyInputStreamToFile(inputStream, archivo);

			byte[] bytesArchivo = Files.readAllBytes(archivo.toPath());

			Archivo objetoArchivo = new Archivo();
			objetoArchivo.setArchivo(bytesArchivo);
			objetoArchivo.setMimetype(mimeType);
			objetoArchivo.setTituloArchivo(nombreArchivo);
			logger.info("objetoArchivo: " + objetoArchivo.toString());

			return bytesArchivo != null ? objetoArchivo : null;

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}

			if (archivo != null && archivo.exists()) {
				try {
					archivo.delete();
				} catch (Exception e) {
					logger.error("El documento no pudo ser eliminado.", e);
				}
			}
		}
	}
	
	
	/**
	 * Metodo que invoca servicio COLLECTION_INFO de Oracle WCC para obtener la informacion de una carpeta por path
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param usuario
	 * @param pathCarpeta
	 * @return
	 */
	public String obtenerIdCarpetaPorPath(String usuario, String pathCarpeta) {
		String id = null;
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();
			dataBinder.putLocal("IdcService", "FLD_INFO");
            dataBinder.putLocal("path", pathCarpeta);

			logger.info("# PETICION FLD_INFO # usuario: {}, path: {}",usuario,pathCarpeta);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA FLD_INFO # ");
			logger.debug(con.binder2String(responseBinder));

			// Se agrega informacion LocalData al resultado de la informacion

             
			DataResultSet resultSet = responseBinder.getResultSet("FolderInfo");
			DataObject dataObject  = resultSet.getRows().get(resultSet.getRows().size() - 1);
			id = dataObject.get("fFolderGUID"); 
			
			return id;

		} catch (IdcClientException idcce) {
			
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else if(idcce.getMessage().contains("does not exist")){
				return null;
			}else {
				logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	
	public void crearCarpetaPorPath(String usuario, String nombreCarpeta,String idCarpetaPadre) {
		DataBinder dataBinder;
		DataBinder responseBinder;
		RIDCIdcConnection con = new RIDCIdcConnection();
		try {

			con.getUCMConnection(env.getProperty("wcc.ip"), env.getProperty("wcc.port"),env.getProperty("wcc.usuario.admin"));

			dataBinder = con.createDataBinder();

			dataBinder.putLocal("IdcService", "FLD_CREATE_FOLDER");
			dataBinder.putLocal("fParentGUID", idCarpetaPadre);
			dataBinder.putLocal("fFolderName", nombreCarpeta);
			dataBinder.putLocal("fSecurityGroup", "Processes");

			logger.info("# PETICION FLD_CREATE_FOLDER # usuario: {}, nombreCarpeta: {}",usuario,nombreCarpeta);
			logger.debug(con.binder2String(dataBinder));
			ServiceResponse serviceResponse = con.sendRequest(dataBinder, env.getProperty("wcc.usuario.admin"));
			responseBinder = serviceResponse.getResponseAsBinder();
			logger.debug("# RESPUESTA FLD_CREATE_FOLDER # ");
			logger.debug(con.binder2String(responseBinder));
         
			   
            logger.info("CARPETA CREADA EXITOSAMENTE '" +nombreCarpeta +"'");
			

		} catch (IdcClientException idcce) {
			logger.error("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			if (idcce.getMessage().contains("does not have sufficient privileges")) {
				throw new BadCredentialsException(idcce.getMessage(),idcce);
			} else {
				throw new RuntimeException("Error WCC IdcClientException: " + idcce.getMessage(), idcce);
			} // Excepcion
		} catch (Exception e) {
			logger.error("Error WCC General Exception: " + e.getMessage(), e);
			throw new RuntimeException("Error WCC General Exception: " + e.getMessage(), e); // Excepcion
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



}
