/**
 * 
 */
package co.dpworld.pmsapp.apiwcc.connection;

import java.io.InputStream;

import oracle.stellent.ridc.IdcClient;
import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.IdcClientManager;
import oracle.stellent.ridc.IdcContext;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.protocol.ServiceResponse;
import oracle.stellent.ridc.protocol.ServiceResponse.ResponseType;

/**
 * Clase que gestiona la conexion con oracle wcc por la API RIDC
 * 
 * @author Sergio Arias
 * @date 10/11/2019
 *
 */
public class RIDCIdcConnection {

	public IdcClient<?, ?, ?> sIdcClient = null;

	String sIdcString = "";

	ServiceResponse aServiceResponse = null;

	IdcClientManager clientManager = null;

	IdcContext userContext = null;

	/**
	 * Metodo get para userContext
	 * 
	 * @author Sergio Arias
	 * @date 10/11/2019
	 * @return Retorna userContext
	 */
	public IdcContext getUserContext() {
		return userContext;
	}

	/**
	 * Metodo set para userContext
	 * 
	 * @author Sergio Arias
	 * @date 10/11/2019
	 * @param setea userContext
	 */
	public void setUserContext(IdcContext userContext) {
		this.userContext = userContext;
	}

	public IdcClient<?, ?, ?> getUCMConnection(String hostIP, String hostPort, String userName)
			throws IdcClientException {
		sIdcString = "idc://" + hostIP + ":" + hostPort;
		clientManager = new IdcClientManager();
		sIdcClient = clientManager.createClient(sIdcString);
		userContext = new IdcContext(userName);
		return sIdcClient;
	}

	// Configura la conexion con el UCM (de forma ea, con lo que solo se puede
	// conectar con un solo UCM)
	// mediante el protocolo "idc", el cual no requiere clave de usuario. Con el
	// protocolo "idc" se debe configurar
	// en el UCM (SystemProperties) la IP del equipo que tiene permiso de
	// comunicarse con este protocolo.
	// Y activar el puerto de escucha en $DOMAIN_HOME/ucm/idc/config/config.cfg
	// IntradocServerPort=4444
	public void ConfigConnection(String hostIP, String hostPort) throws IdcClientException {
		String idcString = "idc://" + hostIP + ":" + hostPort;
		System.out.println("idcString=" + idcString);
		if (idcString.compareTo(sIdcString) != 0) {
			IdcClientManager ICM = new IdcClientManager();
			sIdcClient = ICM.createClient(idcString);
		}
	}

	// Crea un databinder para configurar una solicitud
	public DataBinder createDataBinder() {
		return sIdcClient.createBinder();
	}

	// Ejecuta un servicio UCM bajo el perfil del usuario especificado
	public ServiceResponse sendRequest(DataBinder request, String username) throws IdcClientException {
		return aServiceResponse = sIdcClient.sendRequest(new IdcContext(username), request);
	}
	
	public ServiceResponse sendRequest2(DataBinder request,IdcContext idcContext) throws IdcClientException {
		return aServiceResponse = sIdcClient.sendRequest(idcContext, request);
	}

	/*
	 * public ServiceResponse sendRequest(DataBinder request) throws
	 * IdcClientException { //aServiceResponse = sIdcClient.sendRequest(new
	 * IdcContext(username), request); return aServiceResponse =
	 * sIdcClient.sendRequest(new IdcContext(sUsername), request); }
	 */

	// Ejecuta un servicio UCM bajo el perfil del usuario especificado
	public DataBinder execute(DataBinder request, String username) throws IdcClientException {
		// aServiceResponse = sIdcClient.sendRequest(new IdcContext(username), request);
		aServiceResponse = sIdcClient.sendRequest(new IdcContext(username), request);
		if (aServiceResponse.getResponseType() == ResponseType.BINDER)
			return aServiceResponse.getResponseAsBinder();
		return null;
	}

	// Retorna el DataBinder (si hay) de la ultima ejecuciÛn de execute
	public DataBinder getResponseBinder() throws IdcClientException {
		if (aServiceResponse.getResponseType() == ResponseType.BINDER)
			return aServiceResponse.getResponseAsBinder();
		return null;
	}

	// Retorna el InputStream (si hay) de la ultima ejecuciÛn de execute (para por
	// ej. GET_FILE)
	public InputStream getResponseStream() {
		if (aServiceResponse.getResponseType() == ResponseType.STREAM)
			return aServiceResponse.getResponseStream();
		return null;
	}

	// Retorna las longitud del stream de respuesta (si hay)
	public String getResponseStreamLenght() {
		return aServiceResponse.getHeader("Content-Length");
	}

	// Retorna el tipo de MIME del stream de respuesta (si hay)
	public String getResponseStreamMIMEType() {
		return aServiceResponse.getHeader("Content-type");
	}

	// Retorna el nombre del archivo en el UCM del stream de respuesta (si hay)
	public String getResponseStreamFilename() {
	String str = aServiceResponse.getHeader("Content-Disposition");
		//str = str.substring(str.indexOf("filename=\"") + "filename=\"".length());
		//str = str.replace("\"", "");
		
		//str = str.replace("attachment; filename*=UTF-8''", "");
		//str = str.replace("%20", " ");
		return str;
	}

	// Cierra y libera recursos
	public void close() {
		if (aServiceResponse != null) {
			aServiceResponse.close();
			aServiceResponse = null;
		}
	}

	protected void finalize() throws Throwable {
		close();
	}

	// Convierte la respuesta de la ultima ejecucion en una cadena de texto (para
	// depuracion y pruebas)
	public String serviceResponse2String() {
		if (aServiceResponse == null)
			return null;

		String str = "";
		// PENDIENTE: REVISAR LOS CARACTERES DE str SI SON LOS CORRECTOS O DEBEN ESTAR
		// EN UNICODE
		str += "ReponseType:\n";
		str += "  ∑" + aServiceResponse.getResponseType().name() + "\n";

		str += "Headers:\n";
		for (String item : aServiceResponse.getHeaderNames())
			str += "  ∑" + item + "=" + aServiceResponse.getHeader(item) + "\n";

		try {
			if (aServiceResponse.getResponseType() == ResponseType.BINDER)
				str += binder2String(aServiceResponse.getResponseAsBinder());
		} catch (Exception e) {
		}

		return str;
	}

	// Convierte un DataBinder en una cadena de texto (para depuracion y pruebas)
	public String binder2String(DataBinder db) {
		if (db == null)
			return null;

		String str = "";

		str += "LocalData:\n";
		for (String item : db.getLocalData().keySet())
			str += "  ∑" + item + "=" + db.getLocalData().get(item) + "\n";

		str += "FieldType:\n";
		for (String item : db.getFieldTypeNames())
			str += "  ∑" + item + "=" + db.getFieldType(item) + "\n";

		str += "FileNames:\n";
		for (String item : db.getFileNames())
			str += "  ∑" + item + "=" + db.getFile(item).getFileName() + ", " + db.getFile(item).getContentLength()
					+ " bytes\n";

		str += "OptionList:\n";
		for (String item : db.getOptionListNames()) {
			str += "  ∑" + item + ":\n";
			for (String opt : db.getOptionList(item))
				str += "    ∑" + opt + "\n";
		}

		str += "ResultSets:\n";
		for (String rs : db.getResultSetNames()) {
			str += "  ∑" + rs + ":\n";
			for (DataObject row : db.getResultSet(rs).getRows()) {
				str += "    -Registro:\n";
				for (String item : row.keySet()) {
					str += "      ∑" + item + ":" + row.get(item) + "\n";
				}
			}
		}

		return str;
	}

	/**
	 * @author Sergio Arias
	 * @date 10/11/2019
	 * @param resulset
	 * @return
	 */
	public String result2String(DataBinder DB) {
		String str = "";
		for (String rs : DB.getResultSetNames()) {
			str += "  ∑" + rs + ":\n";
			for (DataObject row : DB.getResultSet(rs).getRows()) {
				str += "    -Registro:\n";
				for (String item : row.keySet()) {
					str += "      ∑" + item + ":" + row.get(item) + "\n";
				}
			}
		}

		return str;
	}

}
