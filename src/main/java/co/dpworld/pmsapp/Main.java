/**
 * 
 */
package co.dpworld.pmsapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Sergio Arias
 * @date 15/03/2020
 *
 */
public class Main {

	/**
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @param args
	 */
	public static void main(String[] args) {
		
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a");
		//System.out.println(calcularFechasVencimientoYRevision("Semestral",formatter));

		if("PL-05-001->XX-01-XXX - Template Manual.doc".contains("->"))
		{
			System.out.println("PL-05-001->XX-01-XXX - Template Manual.doc".split("->")[1]);
		}
		
	}

	private static String calcularFechasVencimientoYRevision(String frecuenciaRevision, SimpleDateFormat formatter) {
		
		Date fechaActual = new Date();
		Calendar calendar =  Calendar.getInstance();
		calendar.setTime(fechaActual);
		
		switch(frecuenciaRevision)
		{
		case "Cada 2 Años":
			calendar.add(Calendar.YEAR, 2);
			break;
		
		case "Cada 3 Años":
			calendar.add(Calendar.YEAR, 2);
			break;
			
		case "Semestral":
			calendar.add(Calendar.MONTH, 6);
			break;
			
		default :
				System.out.println("La frecuencia revision: "+frecuenciaRevision+" no se encuentra mapeada para calcular fecha vencimiento y fecha de revision del documento");
				break;
		}

		return formatter.format(calendar.getTime());
	}
}
