/**
 * 
 */
package co.dpworld.pmsapp;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Clase de configuracion para Swagger
 * 
 * http://localhost:9090/v2/api-docs 
 * http://localhost:9090/swagger-ui.html
 * 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
@Configuration // Quieron que spring sepa que vamos a crear beans
@EnableSwagger2
public class AppPmsServicesSwaggerConfig {

	public static final Contact DEFAULT_CONTACT = new Contact("Sergio Arias", "www.linkedin.com/in/sergio-andres-arias-escandon",
			"sergioarias.fyah@gmail.com");
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("app-pms-services API Documentation DP World", "Documentación API de servicios REST para integración con Oracle WebCenterContent",
			"1.0", "PREMIUM", DEFAULT_CONTACT, "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0",
			new ArrayList<>());

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO);
	}
}
