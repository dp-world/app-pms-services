package co.dpworld.pmsapp.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.ActualizarPasoRq;
import co.dpworld.pmsapp.model.AnexoRq;
import co.dpworld.pmsapp.model.Archivo;
import co.dpworld.pmsapp.model.DocumentoRp;
import co.dpworld.pmsapp.model.DocumentoRq;
import co.dpworld.pmsapp.model.PasoRq;
import co.dpworld.pmsapp.service.ICreacionDocumento;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que representa el controlador rest /flujos/creacion encargado de
 * gestionar el flujo de trabajo de creacion de documentos
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@RestController
@RequestMapping("/flujos")
@Api(value = "/flujos", description = "Operaciones para gestion de flujos documentales - Oracle WCC")
public class WFDocumentoController {

	@Autowired
	private Environment env;
	
	@Autowired
	private ICreacionDocumento creacionDocumentoService;

	@Autowired
	private RidcAdapter ridcAdapter;

	private static final String APPLICATION_MS_WORD_VALUE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

	/**
	 * Servicio POST que crea un documento en Oracle WebCenter Content e inicia el
	 * flujo correspondiente
	 * 
	 * @param creacionDocumentoRq
	 * @return
	 * @throws Exception 
	 */
	@PostMapping(path = "/creacion", produces = "application/json")
	@ApiOperation(value = "Servicio que inicia el flujo de creacion de documentos", notes = "Los datos ingresados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de crear el flujo de creacion"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para registrar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public ResponseEntity<?> crearDocumento(@Valid @RequestBody DocumentoRq creacionDocumentoRq) throws Exception {

			return creacionDocumentoService.crearDocumento(creacionDocumentoRq) ? ResponseEntity.ok().build()
					: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	/**
	 * Servicio GET que permite retornar la metadata de un paso especifico en un
	 * worklofw activo en Oracle WCC
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param idPaso
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/paso/metadata", produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la metadata de un paso de un workflow especifico", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Workflows) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar la metatada del paso del workflow"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public DocumentoRp listarMetadataPasoFlujo(@RequestParam String idPaso, @RequestParam String id) throws Exception {
		return creacionDocumentoService.listarMetadataPasoWorkflow(idPaso, id);
	}

	/**
	 * Servicio POST que permite gestionar un paso de un workflow en WCC
	 * (Aprobar,Rechazar,Guardar)
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param pasoDocumentoRq
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@PostMapping(path = "/paso", produces = "application/json")
	@ApiOperation(value = "Servicio que gestiona la solicitud (Aprobación o Rechazo) de un workflow", notes = "Los datos ingresados por el servicio se encuentran en el ECM Oracle WebCenter Content (Workflow) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de gestionar correctamente la informacion del flujo y accion correspondiente"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para registrar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public ResponseEntity<?> enviarPaso(@Valid @RequestBody PasoRq pasoDocumentoRq, @RequestParam String id)
			throws Exception {
		return creacionDocumentoService.enviarPaso(pasoDocumentoRq, id) ? ResponseEntity.ok().build()
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

	@GetMapping(value = "/descargar/{id}", produces = APPLICATION_MS_WORD_VALUE)
	public ResponseEntity<byte[]> downloadFile(@PathVariable(value = "id") String id) throws IOException {

		// DESCARGAR ARCHIVO NORMAL
		Archivo archivo = creacionDocumentoService.descargarArchivo(id);
		byte[] contenido = archivo.getArchivo();
		if (contenido != null) {

			return ResponseEntity.ok().contentLength(contenido.length)
					.header(HttpHeaders.CONTENT_TYPE, archivo.getMimetype())
					// .header(HttpHeaders.CONTENT_DISPOSITION,
					// "filename=\""+archivo.getTituloArchivo().replaceAll("docx", "")+"\"")
					.header(HttpHeaders.CONTENT_DISPOSITION, archivo.getTituloArchivo()).body(contenido);

		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

	}
	
	/**
	 * Descarga documentos publicados PDF convertidos por IBR en Oracle WCC (WebLocation)
	 * @author Sergio Arias
	 * @date 7/04/2020
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@GetMapping(value = "/descargar/publicado/{id}", produces = APPLICATION_MS_WORD_VALUE)
	public ResponseEntity<byte[]> downloadFileFinal(@PathVariable(value = "id") String id) throws IOException {

		// DESCARGAR ARCHIVO PUBLICADO (WEB LOCATION)

		List<DataObject> resultado = ridcAdapter.infoDoc(id, null, "DOC_INFO");
		

		// OBTENER URL (PDF)
		// /cs/groups/processes/documents/documento_dp-world/awqt/mda2/~edisp/id-006893.pdf
		String url = "";
		String docTitle = "";
		for(DataObject data : resultado) {
			
			if(data.get("DocUrl") != null && !data.get("DocUrl").isEmpty())
			{
				url = data.get("DocUrl");
				
			}
			
			if(data.get("dDocTitle") != null && !data.get("dDocTitle").isEmpty())
			{
				docTitle = data.get("dDocTitle");
			}
			
		}

		String webLocation = url;

		
		byte[] base64 = UtilRidcWcc.obtenerBase64PorUri(webLocation,
														env.getProperty("wcc.usuario.admin"),
														env.getProperty("wcc.usuario.pass"),
													    ridcAdapter.sessionID);
		
		// ELIMINAR ARCHIVO EMPORAL
		
		if (base64 != null) {

			return ResponseEntity.ok().contentLength(base64.length)
					.header(HttpHeaders.CONTENT_TYPE, "application/pdf")
					// .header(HttpHeaders.CONTENT_DISPOSITION,
					// "filename=\""+archivo.getTituloArchivo().replaceAll("docx", "")+"\"")
					.header(HttpHeaders.CONTENT_DISPOSITION,"inline; filename="+docTitle+".pdf")
					.body(base64);

		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GetMapping(value = "/descargar/{id}/{idPaso}/{usuario}", produces = APPLICATION_MS_WORD_VALUE)
	public ResponseEntity<byte[]> downloadFile(@PathVariable(value = "id") String id,
			@PathVariable(value = "idPaso", required = false) String idPaso,
			@PathVariable(value = "usuario", required = false) String usuario) throws IOException {

		// DESCARGAR ANEXO

		// 4. integracion con oracle wcc para obtener informacion (metadata) de cada
		// flujo
		List<DataObject> listaMetadataFlujo = ridcAdapter.obtenerInformacionDeUnFlujo(usuario, "DOC_INFO", idPaso, id);

		String contenidoelacionado = listaMetadataFlujo.get(0).get("xRelatedContentList");
		if (contenidoelacionado != null && !contenidoelacionado.isEmpty()) {
			String[] contenidoRelacionadoSplit = contenidoelacionado.split(":");
			String dDocName = contenidoRelacionadoSplit[3];
			Archivo archivo = creacionDocumentoService.descargarAnexo(dDocName);
			byte[] contenido = archivo.getArchivo();
			if (contenido != null) {

				return ResponseEntity.ok().contentLength(contenido.length)
						.header(HttpHeaders.CONTENT_TYPE, archivo.getMimetype())
						// .header(HttpHeaders.CONTENT_DISPOSITION,
						// "filename=\""+archivo.getTituloArchivo().replaceAll("docx", "")+"\"")
						.header(HttpHeaders.CONTENT_DISPOSITION, archivo.getTituloArchivo()).body(contenido);

			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			return ResponseEntity.status(HttpStatus.ACCEPTED).build();
		}

	}

	@PutMapping(path = "/paso", produces = "application/json")
	@ApiOperation(value = "Servicio que guarda los datos de un paso de un workflow ", notes = "Los datos ingresados por el servicio se encuentran en el ECM Oracle WebCenter Content (Workflow) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de gestionar correctamente la informacion del flujo y accion correspondiente"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para registrar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public ResponseEntity<?> actualizarPaso(@Valid @RequestBody ActualizarPasoRq actualizarPasoRq,
			@RequestParam String id, @RequestParam String dDocName, @RequestParam String dRevLabel,
			@RequestParam String dSecurityGroup, @RequestParam(required = false) String idPaso) throws Exception {
		return creacionDocumentoService.actualizarPaso(actualizarPasoRq, id, dDocName, dRevLabel, dSecurityGroup,
				idPaso) ? ResponseEntity.ok().build() : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

	@PostMapping(path = "/anexo", produces = "application/json")
	@ApiOperation(value = "Servicio que anexa un archivo al documento principal", notes = "Los datos ingresados por el servicio se encuentran en el ECM Oracle WebCenter Content (Document) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de anexar correctamente el documento"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para registrar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public ResponseEntity<?> anexarDocumento(@Valid @RequestBody AnexoRq anexoRq) throws IOException {
		return creacionDocumentoService.anexarDocumento(anexoRq) ? ResponseEntity.ok().build()
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

}
