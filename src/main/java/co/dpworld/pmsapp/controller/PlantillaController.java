package co.dpworld.pmsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.model.Archivo;
import co.dpworld.pmsapp.service.IPlantillaService;
import io.swagger.annotations.Api;

/**
 * Controlador rest que representa /plantillas
 * @author Sergio.Arias
 *
 */
@RestController
@RequestMapping("/plantillas")
@Api(value = "plantillas", description = "Operaciones para gestion de plantillas")
public class PlantillaController {

	@Autowired
	private IPlantillaService plantillaService;
	
	private static final String APPLICATION_MS_WORD_VALUE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	
	
	@GetMapping(value = "/descargar/{codigoTipoDocumento}", produces = APPLICATION_MS_WORD_VALUE)
    public ResponseEntity<byte[]> descargarPlantilla(@PathVariable(value="codigoTipoDocumento") String codigoTipoDocumento) throws Exception {
    	Archivo archivo = plantillaService.descargarArchivo(codigoTipoDocumento);
    	byte[] contenido = archivo.getArchivo();
        if(contenido  != null) {
        
        	return ResponseEntity.ok()
                             .contentLength(contenido.length)
                             .header(HttpHeaders.CONTENT_TYPE, archivo.getMimetype())
                             .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\""+archivo.getTituloArchivo()+"\"")
                             .body(contenido);
        	
        }else {
        	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
	}
