/**
 * 
 */
package co.dpworld.pmsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.model.InformacionUsuarioRp;
import co.dpworld.pmsapp.service.IUsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase que representa el controlador REST para /usuarios
 * @author Sergio Arias
 * @date 21/12/2019
 *
 */
@RestController
@RequestMapping("/usuarios")
@Api(value = "/usuarios", description = "Operaciones para gestion usuarios")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@GetMapping(path = "/info",produces = "application/json")
	@ApiOperation(value = "Servicio que la informacion de un usuario", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Users) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el usuario"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public InformacionUsuarioRp obtenterInformacionUsuario() throws Exception
	{
		return usuarioService.obtenerInfoUsuario();
	}
}
