package co.dpworld.pmsapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.model.Tipologia;
import co.dpworld.pmsapp.service.ITipologiaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase que representa el controlador rest /tipologias
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@RestController
@RequestMapping("/tipologias")
@Api(value = "/tipologias", description = "Operaciones para tipologias documentales")
public class TipologiaController {

	@Autowired
	private ITipologiaService tipologiaService;

	/**
	 * Servicio GET que lista toda las tipologias documentales
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Lista de departamentos y areas configuradas en oracle web center
	 *         content
	 */
	//@PreAuthorize("@restAuthService.hasAccess('listar')")
	// @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de tipologias documentales", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el listado de tipologias documentales"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public List<Tipologia> listar() {
		return tipologiaService.listar();
	}
}
