/**
 * 
 */
package co.dpworld.pmsapp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.BuscarDocumentoRp;
import co.dpworld.pmsapp.model.Clasificacion;
import co.dpworld.pmsapp.model.FrecuenciaRevision;
import co.dpworld.pmsapp.model.InformacionUsuarioRp;
import co.dpworld.pmsapp.model.MedioAlmacenamiento;
import co.dpworld.pmsapp.model.TiempoRetencion;
import co.dpworld.pmsapp.service.IUsuarioService;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import oracle.stellent.ridc.model.DataObject;

/**
 * 
 * @author Sergio Arias
 * @date 4/01/2020
 *
 */
@RestController
@RequestMapping("/documentos")
@Api(value = "/documentos", description = "Operaciones para documentos")
public class DocumentoController {
	

	Logger logger = LoggerFactory.getLogger(DocumentoController.class);


	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private UtilRidcWcc util;
	
	@Autowired
	private IUsuarioService usuarioService;
	

	@GetMapping(path = "/busqueda/titulo",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa documentos que coinciden con el titulo de documento enviado", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Docs) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar documentos que coincidan con el criterio de busqueda"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public List<String> buscarTitulosDocumentosPorTitulo(@RequestParam String tituloDocumento,@RequestParam(required = true) Boolean documentosObsoletos) {
		
		// 1. Busca documento actual 
		String queryBuscarDocumentoPorNombre = "";
		
		if(!documentosObsoletos)
		{
			queryBuscarDocumentoPorNombre += "(xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> xProceso <matches> `04` ) <AND> "; // No obsoletizacion
		}else {
			queryBuscarDocumentoPorNombre += "(xProceso <matches> `03`) <AND> ";  // Si obsoletizacion
		}
		queryBuscarDocumentoPorNombre +=" ((xNombreDocumento <contains> `%"+tituloDocumento+"%`) <OR> (xNomenclatura <contains> `%"+tituloDocumento+"%`)) ";
		
		List<DataObject> resultado = ridcAdapter.buscar(queryBuscarDocumentoPorNombre, "SearchResults");
		if(documentosObsoletos)
		{
			if(resultado.isEmpty()) {
				resultado = new ArrayList<>();
			}
			resultado.addAll(obtenerDocsExpirados(tituloDocumento));
		}
		
		
		List<String> listaResultadoTitulos = new ArrayList<>();
		for(DataObject data : resultado)
		{
			if(data.get("dDocTitle") != null && !data.get("dDocTitle").isEmpty()) {
				
				logger.info("(dDocTitle <substring> `"+tituloDocumento+"`) -> "+data.get("dDocTitle") +" | "+data.get("xNomenclatura"));
				
				// Si no existe valor de nomenclatura no se concatena al nombre de documento para autocompletar
				if(data.get("xNomenclatura") != null && !data.get("xNomenclatura").isEmpty())
				{
					listaResultadoTitulos.add(data.get("xNomenclatura")+"->"+data.get("xNombreDocumento"));
				}else {
					listaResultadoTitulos.add(data.get("xNombreDocumento"));
				}
			}
		}
		
		return listaResultadoTitulos;
	}

	/**
	 * @author Sergio Arias
	 * @date 20/12/2020
	 * @param tituloDocumento
	 * @param resultado
	 */
	private List<DataObject> obtenerDocsExpirados(String tituloDocumento) {
		List<DataObject> expirados = ridcAdapter.buscarExpirados(" ((xNombreDocumento <contains> `%"+tituloDocumento+"%`) <OR> (xNomenclatura <contains> `%"+tituloDocumento+"%`)) "); // Retornar documentos expirados del sistema
		List<DataObject> resultado = new ArrayList<>();
		for(DataObject expirado : expirados)
		{
			if(expirado.get("xNombreDocumento") != null && (expirado.get("xNombreDocumento").contains(tituloDocumento) || expirado.get("xNomenclatura").contains(tituloDocumento) ) )
			{
				resultado.add(expirado);
			}
		}
		return resultado;
	}
	
	//@PreAuthorize("hasAuthority('PO')") // Procesos
	@GetMapping(path = "/busqueda",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa documentos que coinciden con los criterios de busqueda", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Docs) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar documentos que coincidan con los criterios de busqueda"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public List<BuscarDocumentoRp> buscarDocumentos() throws Exception {
		
		// 1. Query buscar documentos
		String query = "(xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> xProceso <matches> `04`) ";
				//+ " <AND> xIdcProfile <matches> `Creacion` <OR> xIdcProfile <matches> "
				//+ "`Actualizacion` <OR> xIdcProfile <matches> `Reactivacion` <OR> xIdcProfile <matches> `Nomenclatura`)";
		
		// Obtener el departamento del usuario logeado
		InformacionUsuarioRp infoUsuario = usuarioService.obtenerInfoUsuario();
		
		List<BuscarDocumentoRp> listaResultadoDocumentos = new ArrayList<>();		
		List<DataObject> resultadoDataWcc = ridcAdapter.buscar(query, "SearchResults");
		
		
		//Obtener hash codigos y descripciones vista de departamentos
		Map<String,String> hashDepartamentos = util.obtenerClaveValorDepartamentoPorCod();
		
		//Obtener hash codigos y descripciones vistas de tipos documentos
		Map<String,String> hashTipoDeDocumentos = util.obtenerClaveValorTipoDocumentosPorCod(); 
			
		
		//Obtener hash codigos y descripciones vistas razonCreacion
		Map<String,String> hashFuentesCreacion = util.obtenerClaveValorFuentesCreacionPorCod(); 
		
		
		for(DataObject data : resultadoDataWcc)
		{
			BuscarDocumentoRp datosDocumentoRp = new BuscarDocumentoRp();
			
			boolean tienePermisos = false;
			
			if(data.get("xNombreDocumento") != null && !data.get("xNombreDocumento").isEmpty()) {

				String clasificacionDoc = data.get("xClasificacion");
				
				// Validar permisos segun la clasificacion del documento
				if(clasificacionDoc != null && clasificacionDoc.equalsIgnoreCase("Confidencial"))
				{
					if(infoUsuario.getDepartamento() != null && infoUsuario.getDepartamento().equalsIgnoreCase("PO"))
					{
						tienePermisos = true;
					}
					
				// Si la clasificacion del documento es privada o restringido
				}else if(clasificacionDoc != null && 
						(clasificacionDoc.equalsIgnoreCase("Privado") || clasificacionDoc.equalsIgnoreCase("Restringido"))){
					tienePermisos = true;
				}
				
				// Si tiene permisos se muestra el documento
				if(tienePermisos) {
					
					asignarMetadataBusqueda(hashDepartamentos, hashTipoDeDocumentos, hashFuentesCreacion, data,	datosDocumentoRp);
					listaResultadoDocumentos.add(datosDocumentoRp);
				}
			}
			
		}
		return listaResultadoDocumentos;
	}

	/**
	 * Metodo que asigna la metadata a una lista que retorna la informacion para la busqueda de documentos
	 * @author Sergio Arias
	 * @date 23/04/2020
	 * @param hashDepartamentos
	 * @param hashTipoDeDocumentos
	 * @param hashFuentesCreacion
	 * @param data
	 * @param datosDocumentoRp
	 * @throws Exception
	 */
	private void asignarMetadataBusqueda(Map<String, String> hashDepartamentos,
			Map<String, String> hashTipoDeDocumentos, Map<String, String> hashFuentesCreacion, DataObject data,
			BuscarDocumentoRp datosDocumentoRp) throws Exception {
		datosDocumentoRp.setId(data.get("dID"));
		
		if(data.get("xsecuenciaDoc") != null) {
			datosDocumentoRp.setNumeroCambio(data.get("xsecuenciaDoc"));
		}
		
		//campos v1
		if(data.get("xNombreDocumento") != null) {
			datosDocumentoRp.setNombre(data.get("xNombreDocumento"));
		}
		if(data.get("dRevisionID") != null) {
			datosDocumentoRp.setVersion(data.get("dRevisionID"));
		}
		if(data.get("xDepartamento") != null) {
			datosDocumentoRp.setDepartamento(util.obtenerNombreListaPorCodHash(data.get("xDepartamento"),hashDepartamentos));
		}
		if(data.get("xSolicitante") != null) {
			datosDocumentoRp.setSolicitante(data.get("xSolicitante"));
		}

		if(data.get("dCreateDate") != null)
		{
			datosDocumentoRp.setFechaCreacion(data.get("dCreateDate"));
		}
		
		if(data.get("xNomenclatura") != null)
		{
			datosDocumentoRp.setNomenclatura(data.get("xNomenclatura"));
		}
		
		//campos v2
		if(data.get("xtipoDocumento") != null) {
			datosDocumentoRp.setTipoDocumento(util.obtenerNombreListaPorCodHash(data.get("xtipoDocumento"),hashTipoDeDocumentos));
		}
		
		if(data.get("xtipoCambio") != null) {
			datosDocumentoRp.setTipoCambio(data.get("xtipoCambio"));
		}
		
		if(data.get("xrazonCreacion") != null) {
			datosDocumentoRp.setRazonCreacion(util.obtenerNombreListaPorCodHash(data.get("xrazonCreacion"),hashFuentesCreacion));
		}
		
		if(data.get("xTiempoRetencion") != null) {
			datosDocumentoRp.setTiempoRetencion(data.get("xTiempoRetencion"));
		}
		
		if(data.get("xLiderProceso") != null) {
			datosDocumentoRp.setLiderProceso(data.get("xLiderProceso"));
		}
		
		if(data.get("xFrecuenciaRevision") != null) {
			datosDocumentoRp.setFrecuenciaRevision(data.get("xFrecuenciaRevision"));
		}
		
		if(data.get("xRevision") != null) {
			datosDocumentoRp.setRevision(data.get("xRevision"));
		}
		
		if(data.get("xResponsable") != null) {
			datosDocumentoRp.setResponsable(data.get("xResponsable"));
		}
		
		if(data.get("xFechaProximaRevision") != null) {
			datosDocumentoRp.setFechaProximaRevision(data.get("xFechaProximaRevision"));
		}
		
		if(data.get("xClasificacion") != null) {
			datosDocumentoRp.setClasificacion(data.get("xClasificacion"));
		}
		
		
		if(data.get("dOutDate") != null) {
			datosDocumentoRp.setFechaVencimiento(data.get("dOutDate"));
		}
	}
	
	@GetMapping(path = "/tiempoRetencion",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de opciones para tiempos de retencion", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el listado de tipologias documentales"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public List<TiempoRetencion> listarTiempoRetencion() {

		List<TiempoRetencion> listaTiempoRetenciones = new ArrayList<>();

		/* integracion con oracle wcc para obtener listas de una vista */
		List<DataObject> listaVistaWCC = ridcAdapter.obtenerValoresVistas("vistaTiempoDeRetencion",
				"TiempoRetencion");
		for (DataObject fila : listaVistaWCC) {
			listaTiempoRetenciones.add(new TiempoRetencion(fila.get("Codigo"), fila.get("nombreTiempoRetencion")));
		}
		return listaTiempoRetenciones;

	}
	
	
	@GetMapping(path = "/mediosDeAlmacenamiento",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de opciones para los medios de almacenamiento", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el listado de tipologias documentales"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public List<MedioAlmacenamiento> listarMedioAlmacenamiento() {
		
		List<MedioAlmacenamiento> listaMediosAlmacenamiento = new ArrayList<>();

		/* integracion con oracle wcc para obtener listas de una vista */
		List<DataObject> listaVistaWCC = ridcAdapter.obtenerValoresVistas("vistaMedioAlmacenamiento",
				"MedioAlmacenamiento");
		for (DataObject fila : listaVistaWCC) {
			listaMediosAlmacenamiento.add(new MedioAlmacenamiento(fila.get("Codigo"), fila.get("nombreMedioAlmacenamiento")));
		}
		return listaMediosAlmacenamiento;
	}
	
	
	@GetMapping(path = "/frecuenciasRevision",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de opciones para las frecuencias de revision", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el listado de tipologias documentales"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public List<FrecuenciaRevision> listarFrecuenciaRevision() {
		
		List<FrecuenciaRevision> listaFrecuenciasRevision = new ArrayList<>();

		/* integracion con oracle wcc para obtener listas de una vista */
		List<DataObject> listaVistaWCC = ridcAdapter.obtenerValoresVistas("vistaFrecuenciaRevision",
				"FrecuenciaRevision");
		for (DataObject fila : listaVistaWCC) {
			if(fila.get("Codigo") != null && !fila.get("nombreFrecuenciaRevision").isEmpty()) {
				listaFrecuenciasRevision.add(new FrecuenciaRevision(fila.get("Codigo"), fila.get("nombreFrecuenciaRevision")));
			}
		}
		return listaFrecuenciasRevision;
	}
	
	@GetMapping(path = "/clasificaciones",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de opciones para las frecuencias de revision", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el listado de tipologias documentales"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public List<Clasificacion> listarClasificaciones() {
		
		List<Clasificacion> listaClasificaciones = new ArrayList<>();

		/* integracion con oracle wcc para obtener listas de una vista */
		List<DataObject> listaVistaWCC = ridcAdapter.obtenerValoresVistas("vistaClasificacion",
				"Clasificacion");
		for (DataObject fila : listaVistaWCC) {
			listaClasificaciones.add(new Clasificacion(fila.get("Codigo"), fila.get("nombreClasificacion")));
		}
		return listaClasificaciones;
	}
	
}
