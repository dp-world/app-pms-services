package co.dpworld.pmsapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.model.Departamento;
import co.dpworld.pmsapp.service.IDepartamentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase que representa el controlador rest /departamentos
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@RestController
@RequestMapping("/departamentos")
@Api(value = "/departamentos", description = "Operaciones para departamentos y/o areas")
public class DepartamentoController {

	@Autowired
	private IDepartamentoService departamentoService;

	/**
	 * Servicio GET que lista todos los departamentos
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Lista de departamentos y areas configuradas en oracle web center
	 *         content
	 */
	//@PreAuthorize("@restAuthService.hasAccess('listar')")
	// @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de departamentos y/o areas", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar el listado de departamentos y/o areas"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public List<Departamento> listar() {
		return departamentoService.listar();
	}
}
