package co.dpworld.pmsapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.model.RazonCreacion;
import co.dpworld.pmsapp.service.IRazonCreacionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase que representa el controlador rest /razones
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@RestController
@RequestMapping("/razones")
@Api(value = "/razones", description = "Operaciones para razon/creacion de documentos")
public class RazonCreacionController {

	@Autowired
	private IRazonCreacionService razonCreacionService;

	/**
	 * Servicio GET que lista todas las razones y/o creaciones de docmentos
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Lista de razones y/o creaciones de documentos configuradas en oracle
	 *         web center content
	 */
	//@PreAuthorize("@restAuthService.hasAccess('listar')")
	// @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la lista de fuentes de creación", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Vistas) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar las fuentes de la creacion de documentos"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public List<RazonCreacion> listar() {
		return razonCreacionService.listar();
	}
}
