/**
 * 
 */
package co.dpworld.pmsapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.DocumentosObsoletosRp;
import co.dpworld.pmsapp.model.MaestroDocumentoRp;
import co.dpworld.pmsapp.model.MatrizRetencionesRp;
import co.dpworld.pmsapp.util.EMetadataOracleWCC;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import oracle.stellent.ridc.model.DataObject;

/**
 * Controlador que gestiona operaciones relacionadas con reportes
 * @author Sergio Arias
 * @date 18/01/2020
 *
 */
@RestController
@RequestMapping("/reportes")
@Api(value = "/reportes", description = "Operaciones para reportes")
public class ReporteController {

	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private UtilRidcWcc util;
	
	//@PreAuthorize("hasAuthority('PO')") // Procesos
	@GetMapping(path = "/matriz/retenciones",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa los datos del reporte matriz de retenciones",
	notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Docs) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar informacion del reporte de matriz de retenciones"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public List<MatrizRetencionesRp> obtenerMatrizRetenciones() throws Exception {
		
		// 1. Query buscar documentos
		String query = "xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> "
				+ "xProceso <matches> `04` <AND> xIdcProfile <matches> `Creacion` <OR> xIdcProfile <matches> "
				+ "`Actualizacion` <OR> xIdcProfile <matches> `Reactivacion` <AND> xIdcProfile <matches> `Nomenclatura`";
		
		List<MatrizRetencionesRp> listaResultadoDocumentos = new ArrayList<>();		
		List<DataObject> resultadoDataWcc = ridcAdapter.buscar(query, "SearchResults");
		
		
		//Obtener hash codigos y descripciones vista de departamentos
		Map<String,String> hashDepartamentos = util.obtenerClaveValorDepartamentoPorCod();
		
		for(DataObject data : resultadoDataWcc)
		{
			MatrizRetencionesRp datosDocumentoRp = new MatrizRetencionesRp();
			if(data.get("xNombreDocumento") != null && !data.get("xNombreDocumento").isEmpty()) {

				datosDocumentoRp.setId(data.get("dID"));
				
				if(data.get("xDepartamento") != null) {
					datosDocumentoRp.setDepartamento(util.obtenerNombreListaPorCodHash(data.get("xDepartamento"),hashDepartamentos));
				}
				if(data.get("xNomenclatura") != null) {
					datosDocumentoRp.setNoRegistro(data.get("xNomenclatura"));
				}
				if(data.get("xNombreDocumento") != null) {
					datosDocumentoRp.setNombreRegistro(data.get("xNombreDocumento"));
				}
				if(data.get("xTiempoRetencion") != null) {
					datosDocumentoRp.setTiempoDeRetencion(data.get("xTiempoRetencion"));
				}
				if(data.get("xMedioAlmacenamiento") != null) {
					datosDocumentoRp.setMedioAlmacenamiento(data.get("xMedioAlmacenamiento"));
				}
				if(data.get("xResponsable") != null) {
					datosDocumentoRp.setResponsable(data.get("xResponsable"));
				}
				
				
				listaResultadoDocumentos.add(datosDocumentoRp);
			}
			
		}
		return listaResultadoDocumentos;
	}
	
	//@PreAuthorize("hasAuthority('PO')") // Procesos
	@GetMapping(path = "/matriz/maestro",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa los datos del reporte listado maestro de documentos",
	notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Docs) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar informacion del reporte de matriz de retenciones"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public List<MaestroDocumentoRp> obtenerListadoMaestroDocumentos() throws Exception {
		
		// 1. Query buscar documentos
		String query = "xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> xProceso <matches> `03` <OR> "
				+ "xProceso <matches> `04` <AND> xIdcProfile <matches> `Creacion` <OR> xIdcProfile <matches> "
				+ "`Actualizacion` <OR> xIdcProfile <matches> `Reactivacion` <AND> xIdcProfile <matches> `Nomenclatura`";
		
		List<MaestroDocumentoRp> listaResultadoDocumentos = new ArrayList<>();		
		List<DataObject> resultadoDataWcc = ridcAdapter.buscar(query, "SearchResults");
		
		
		//Obtener hash codigos y descripciones vista de departamentos
		Map<String,String> hashDepartamentos = util.obtenerClaveValorDepartamentoPorCod();
		
		//Obtener hash codigos y descripciones vista de procesos
		Map<String,String> hashProcesos = new HashMap<>(); //TODO el hash se debe alimentar de las vistas de WCC
		hashProcesos.put("01", "Creación de documento");
		hashProcesos.put("02", "Actualización de documento");
		hashProcesos.put("03", "Obsoletización de documento");
		hashProcesos.put("04", "Reactivación de documento");
		hashProcesos.put("05", "Cambio de nomenclatura documento");
		
		// Recorrer resultado de los documentos
		for(DataObject docPrincipal : resultadoDataWcc)
		{
			
			if(docPrincipal.get("xNombreDocumento") != null && !docPrincipal.get("xNombreDocumento").isEmpty()) {

				
				//2. Buscar las versiones del documento
				//List<DataObject> listadoVersionesDocumento = ridcAdapter.infoDocSinLocalData( docPrincipal.get("dID"),docPrincipal.get("dDocName") , "REVISION_HISTORY");
				
				//3. Recorrer versiones del documento
				//for(DataObject docVersion : listadoVersionesDocumento)
				//{
					
					MaestroDocumentoRp datosDocumentoRp = new MaestroDocumentoRp();
					
					//4. Buscar la metadata la de la version especifica
					//List<DataObject> infoDocVersion = ridcAdapter.infoDoc( docVersion.get("dID"),null , "DOC_INFO");
					
					
					/*datosDocumentoRp.setNombreDocumento(infoDocVersion.get(1).get("xNombreDocumento"));
					datosDocumentoRp.setId(infoDocVersion.get(1).get("dID"));
					
					if(infoDocVersion.get(1).get("xDepartamento") != null) {
						datosDocumentoRp.setDepartamento(util.obtenerNombreListaPorCodHash(infoDocVersion.get(1).get("xDepartamento"),hashDepartamentos));
					}
					if(infoDocVersion.get(1).get("xClasificacion") != null) {
						datosDocumentoRp.setClasificacion(infoDocVersion.get(1).get("xClasificacion"));
					}
					
					datosDocumentoRp.setAprobadoPor(obtenerNombreAprobacion(infoDocVersion)); 
					
					if(infoDocVersion.get(1).get("xProceso") != null) {
						datosDocumentoRp.setDescripcionUltimoCambio(util.obtenerNombreProcesoPorCodHash(infoDocVersion.get(1).get("xProceso"),hashProcesos));
					}
					
					datosDocumentoRp.setFechaAprobacion(obtenerFechaAprobacion(infoDocVersion)); 
					
					if(infoDocVersion.get(1).get("xNomenclatura") != null) {
						datosDocumentoRp.setNomenclatura(infoDocVersion.get(1).get("xNomenclatura"));
					}
					
					if(infoDocVersion.get(1).get("xRevision") != null) {
						datosDocumentoRp.setRevision(infoDocVersion.get(1).get("xRevision"));
					}
					
					listaResultadoDocumentos.add(datosDocumentoRp); */
					
					datosDocumentoRp.setNombreDocumento(docPrincipal.get("xNombreDocumento"));
					datosDocumentoRp.setId(docPrincipal.get("dID"));
					
					if(docPrincipal.get("xDepartamento") != null) {
						datosDocumentoRp.setDepartamento(util.obtenerNombreListaPorCodHash(docPrincipal.get("xDepartamento"),hashDepartamentos));
					}
					if(docPrincipal.get("xClasificacion") != null) {
						datosDocumentoRp.setClasificacion(docPrincipal.get("xClasificacion"));
					}
					
					datosDocumentoRp.setAprobadoPor(obtenerNombreAprobacion2(docPrincipal)); 
					
					if(docPrincipal.get("xProceso") != null) {
						datosDocumentoRp.setDescripcionUltimoCambio(util.obtenerNombreProcesoPorCodHash(docPrincipal.get("xProceso"),hashProcesos));
					}
					
					datosDocumentoRp.setFechaAprobacion(obtenerFechaAprobacion2(docPrincipal)); 
					
					if(docPrincipal.get("xNomenclatura") != null) {
						datosDocumentoRp.setNomenclatura(docPrincipal.get("xNomenclatura"));
					}
					
					if(docPrincipal.get("xRevision") != null) {
						datosDocumentoRp.setRevision(docPrincipal.get("xRevision"));
					}
					
					listaResultadoDocumentos.add(datosDocumentoRp);
				//}
					

			}else {
				System.out.println("El nombre de documento esta vacio , se omite.");
			}
			
		}
		return listaResultadoDocumentos;
	}

	/**
	 * Metodo que permite obtener el nombre aprobador dependiendo del flujo de procedencia
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @param infoDocVersion
	 * @return
	 */
	private String obtenerNombreAprobacion(List<DataObject> infoDocVersion) {
		if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("01"))
		{
			// Creacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.NOMBRE_APROBACION_CREACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("02")) {
			
			// Actualizacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.NOMBRE_APROBACION_ACTUALIZACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("03")) {
			
			//Obsoletizacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.NOMBRE_APROBACION_OBSOLETIZACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("04")) {
			
			//Reactivacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.NOMBRE_APROBACION_REACTIVACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("05")) {
			
			//Cambio Nomenclatura
			return infoDocVersion.get(1).get(EMetadataOracleWCC.NOMBRE_APROBACION_NOMENCLATURA.getVal());
			
		}else {
			return "";
		}
		
	}
	
	private String obtenerNombreAprobacion2(DataObject infoDocVersion) {
		if(infoDocVersion.get("xProceso").equalsIgnoreCase("01"))
		{
			// Creacion
			return infoDocVersion.get(EMetadataOracleWCC.NOMBRE_APROBACION_CREACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("02")) {
			
			// Actualizacion
			return infoDocVersion.get(EMetadataOracleWCC.NOMBRE_APROBACION_ACTUALIZACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("03")) {
			
			//Obsoletizacion
			return infoDocVersion.get(EMetadataOracleWCC.NOMBRE_APROBACION_OBSOLETIZACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("04")) {
			
			//Reactivacion
			return infoDocVersion.get(EMetadataOracleWCC.NOMBRE_APROBACION_REACTIVACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("05")) {
			
			//Cambio Nomenclatura
			return infoDocVersion.get(EMetadataOracleWCC.NOMBRE_APROBACION_NOMENCLATURA.getVal());
			
		}else {
			return "";
		}
		
	}

	/**
	 * Metodo que permite obtener la fecha de aprobacion dependiendo del flujo de procedencia (Creacion, Actualizacion,Obsoletizacion,Reactivacion,Nomenclatura)
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @param infoDocVersion
	 * @return
	 */
	private String obtenerFechaAprobacion(List<DataObject> infoDocVersion) {
		
		if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("01"))
		{
			// Creacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.FECHA_APROBACION_CREACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("02")) {
			
			// Actualizacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.FECHA_APROBACION_ACTUALIZACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("03")) {
			
			//Obsoletizacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.FECHA_APROBACION_OBSOLETIZACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("04")) {
			
			//Reactivacion
			return infoDocVersion.get(1).get(EMetadataOracleWCC.FECHA_APROBACION_REACTIVACION.getVal());
			
		}else if(infoDocVersion.get(1).get("xProceso").equalsIgnoreCase("05")) {
			
			//Cambio Nomenclatura
			return infoDocVersion.get(1).get(EMetadataOracleWCC.FECHA_APROBACION_NOMENCLATURA.getVal());
			
		}else {
			return "";
		}
	}
	
private String obtenerFechaAprobacion2(DataObject infoDocVersion) {
		
		if(infoDocVersion.get("xProceso").equalsIgnoreCase("01"))
		{
			// Creacion
			return infoDocVersion.get(EMetadataOracleWCC.FECHA_APROBACION_CREACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("02")) {
			
			// Actualizacion
			return infoDocVersion.get(EMetadataOracleWCC.FECHA_APROBACION_ACTUALIZACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("03")) {
			
			//Obsoletizacion
			return infoDocVersion.get(EMetadataOracleWCC.FECHA_APROBACION_OBSOLETIZACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("04")) {
			
			//Reactivacion
			return infoDocVersion.get(EMetadataOracleWCC.FECHA_APROBACION_REACTIVACION.getVal());
			
		}else if(infoDocVersion.get("xProceso").equalsIgnoreCase("05")) {
			
			//Cambio Nomenclatura
			return infoDocVersion.get(EMetadataOracleWCC.FECHA_APROBACION_NOMENCLATURA.getVal());
			
		}else {
			return "";
		}
	}
	
	//@PreAuthorize("hasAuthority('PO')") // Procesos
	@GetMapping(path = "/matriz/obsoletizacion",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa los datos del reporte listado documentos obsoletos",
	notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Docs) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar informacion del reporte documentos obsoletos"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public List<DocumentosObsoletosRp> obtenerListadoDocumentosObsoletos() throws Exception {
		
		// 1. Query buscar documentos
		String query = "xProceso <matches> `03`";
		
		List<DocumentosObsoletosRp> listaResultadoDocumentos = new ArrayList<>();		
		List<DataObject> resultadoDataWcc = ridcAdapter.buscar(query, "SearchResults");
		if(resultadoDataWcc.isEmpty()) {
			resultadoDataWcc = new ArrayList<>();
		}
		resultadoDataWcc.addAll(ridcAdapter.buscarExpirados(""));
		
		//Obtener hash codigos y descripciones vista de departamentos
		Map<String,String> hashDepartamentos = util.obtenerClaveValorDepartamentoPorCod();
		
		for(DataObject data : resultadoDataWcc)
		{
			DocumentosObsoletosRp datosDocumentoRp = new DocumentosObsoletosRp();
			if(data.get("xNombreDocumento") != null && !data.get("xNombreDocumento").isEmpty()) {

				datosDocumentoRp.setId(data.get("dID"));
				
				if(data.get("xDepartamento") != null) {
					datosDocumentoRp.setDepartamento(util.obtenerNombreListaPorCodHash(data.get("xDepartamento"),hashDepartamentos));
				}
				
				datosDocumentoRp.setNombreDocumento(data.get("xNombreDocumento"));
				
				datosDocumentoRp.setAprobadoPor(data.get("xNombreAprobObsoletizacion")); 

				if(data.get("xFechaAprobObsoletizacion") != null && !data.get("xFechaAprobObsoletizacion").equals("")) {
					datosDocumentoRp.setFechaEfectiva(data.get("xFechaAprobObsoletizacion")); 
				}else {
					datosDocumentoRp.setFechaEfectiva(data.get("dOutDate")); 
				}
				
				if(data.get("xNomenclatura") != null) {
					datosDocumentoRp.setNomenclatura(data.get("xNomenclatura"));
				}
				
				if(data.get("xRevision") != null) {
					datosDocumentoRp.setRevision(data.get("xRevision"));
				}
				
				listaResultadoDocumentos.add(datosDocumentoRp);
			}
			
		}
		return listaResultadoDocumentos;
	}
	
}
