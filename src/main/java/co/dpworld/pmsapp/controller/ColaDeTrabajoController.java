package co.dpworld.pmsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.model.TablaColaDeTrabajo;
import co.dpworld.pmsapp.service.IColaDeTrabajoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Servicio que representa el controlador rest de /cola/
 * 
 * @author Sergio.Arias
 * @date 11/19/2019
 *
 */
@RestController
@RequestMapping("/cola")
@Api(value = "/cola", description = "Operaciones para colas de trabajos")
public class ColaDeTrabajoController {

	@Autowired
	private IColaDeTrabajoService colaDeTrabajoService;

	/**
	 * Servicio GET que lista la cola de trabajo (Inbox Workflows) de un usuario en
	 * Oracle WebCenter Content
	 * 
	 * @return
	 * @throws Exception 
	 */
	//@PreAuthorize("@restAuthService.hasAccess('listar')")
	// @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Servicio que lista la cola de trabajo de un usuario (Inbox)", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Workflows) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar la lista de actividades en un inbox de un usuario"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información") })
	public TablaColaDeTrabajo listarColaDeTrabajo() throws Exception {
		return colaDeTrabajoService.listarCola();
	}

}
