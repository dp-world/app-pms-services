/**
 * 
 */
package co.dpworld.pmsapp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.CantidadFlujosRp;
import co.dpworld.pmsapp.model.DataSet;
import co.dpworld.pmsapp.model.RespuestaDocumentosDepartamentoGraph;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que representa el controlador REST para acciones de dashboard
 * @author Sergio Arias
 * @date 1/02/2020
 *
 */
@RestController
@RequestMapping("/tableros")
@Api(value = "/tableros", description = "Operaciones relacionadas con tableros / dashboard")
public class DashboardController {
	
	Logger logger = LoggerFactory.getLogger(DocumentoController.class);

	@Autowired
	private UtilRidcWcc util;
	
	@Autowired
	private RidcAdapter ridcAdapter;
	
	@GetMapping(path = "/cantidad/flujos",produces = "application/json")
	@ApiOperation(value = "Servicio que regresa la cantidad de flujos activos ", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Workflow) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar documentos que coincidan con el criterio de busqueda"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public CantidadFlujosRp buscarCantidadesFlujos() {
		
		
		CantidadFlujosRp cantidadFlujosRp = new CantidadFlujosRp();
		
		
		//1. Obtener el username del JWT
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		
		//2. integracion con oracle wcc para obtener inbox de un usuario 
		List<DataObject> listaInbox = ridcAdapter.obtenerInboxUsuario(currentPrincipalName, "WorkflowInQueue");	
		
		
		//3. Se cuentan las cantidades de registros activos por cada flujo utilizando STREAM API JAVA
		long cantidadCreacion = listaInbox.stream()
                .filter(c -> "CREACION_DOCUMENTO".equalsIgnoreCase(c.get("dWfName")))
                .count();
		
		long cantidadActualizacion = listaInbox.stream()
                .filter(c -> "ACTUALIZAR_DOCUMENTO".equalsIgnoreCase(c.get("dWfName")))
                .count();
		
		long cantidadReactivacion = listaInbox.stream()
                .filter(c -> "REACTIVAR_DOCUMENTOS".equalsIgnoreCase(c.get("dWfName")))
                .count();
		
		long cantidadObsoletizacion = listaInbox.stream()
                .filter(c -> "OBSOLETIZACION_DOCUMENTO".equalsIgnoreCase(c.get("dWfName")))
                .count();
		
		long cantidadNomenclatura = listaInbox.stream()
                .filter(c -> "CAMBIO_NOMENCLATURA".equalsIgnoreCase(c.get("dWfName")))
                .count();
		
		cantidadFlujosRp.setCantidadActualizacion(cantidadActualizacion);
		cantidadFlujosRp.setCantidadCreacion(cantidadCreacion);
		cantidadFlujosRp.setCantidadNomenclatura(cantidadNomenclatura);
		cantidadFlujosRp.setCantidadObsoletizacion(cantidadObsoletizacion);
		cantidadFlujosRp.setCantidadReactivacion(cantidadReactivacion);
		
		return cantidadFlujosRp;
	}

	
	@GetMapping(path = "/cantidad/documentos/departamento",produces = "application/json")
	@ApiOperation(value = "Servicio que la cantidad de documentos agrupados por departamento ", notes = "Los datos retornados por el servicio se encuentran en el ECM Oracle WebCenter Content (Docs) ")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Sucede si falla al enviar la respuesta"),
			@ApiResponse(code = 200, message = "En caso de encontrar documentos que coincidan con el criterio de busqueda"),
			@ApiResponse(code = 401, message = "En caso de no esta autorizado para consultar informacion"),
			@ApiResponse(code = 404, message = "En caso de no encontrar información")})
	public RespuestaDocumentosDepartamentoGraph buscarCantidadDocumentosPorDepartamento() throws Exception {
		RespuestaDocumentosDepartamentoGraph respuestaGraph = new RespuestaDocumentosDepartamentoGraph();
		
		// 1. Query buscar documentos
		String queryBase = "(xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> "
				+ "xProceso <matches> `04` <AND> xIdcProfile <matches> `Creacion` <OR> xIdcProfile <matches> "
				+ "`Actualizacion` <OR> xIdcProfile <matches> `Reactivacion` <OR> xIdcProfile <matches> `Nomenclatura` ) ";
		
		//Obtener hash codigos y descripciones vista de departamentos
		Map<String,String> hashDepartamentos = util.obtenerClaveValorDepartamentoPorCod();
		
		// 2. Armar el obtejo de respuesta para documentos x departamento graph
		List<String> labels = new ArrayList<>();
		List<DataSet> datasets = new ArrayList<>();
		List<Long> listaCantidadDocsDep = new ArrayList<>();
		
		// Recorrer departamentos y realizar busqueda
		for (Map.Entry<String, String> entry : hashDepartamentos.entrySet()) {
			labels.add(entry.getValue());
			String queryDep = queryBase + " <AND> xDepartamento <matches> `"+entry.getKey()+"`";
			List<DataObject> resultadoDataWcc = ridcAdapter.buscar(queryDep, "SearchResults");
			Long cantidadDocsDep = resultadoDataWcc.stream()
					.filter(c -> c.get("xNombreDocumento") != null && !c.get("xNombreDocumento").isEmpty())
					.count();
			listaCantidadDocsDep.add(cantidadDocsDep);
		}	
		
		DataSet dataSetDocumentosDep = new DataSet();
		dataSetDocumentosDep.setLabel("Documentos");
		dataSetDocumentosDep.setData(listaCantidadDocsDep);
		
		datasets.add(dataSetDocumentosDep);
		respuestaGraph.setLabels(labels);
		respuestaGraph.setDatasets(datasets);
		
		return respuestaGraph;
	}
		
		
	
}
