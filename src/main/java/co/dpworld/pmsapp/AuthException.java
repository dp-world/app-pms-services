/**
 * 
 */
package co.dpworld.pmsapp;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.exception.ValidacionException;


/**
 * Clase que contiene las gestion de excepciones relacionadas con autorizacion y acceso
 * 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
@Component
@ControllerAdvice
public class AuthException implements AuthenticationEntryPoint {
	
	Logger logger = LoggerFactory.getLogger(RidcAdapter.class);
	
	/**
	 * Se sobreescribe excepcion para AuthenticationEntryPoint
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex)
			throws IOException, ServletException {
		final Map<String, Object> mapException = new HashMap<>();

		mapException.put("status", "401");
		mapException.put("error", "Insufficient Privileges");
		mapException.put("message", "No tienes permisos para acceder a este recurso web "+request.getServletPath());
		mapException.put("exception", "No autorizado");
		mapException.put("path", request.getServletPath());
		mapException.put("timestamp", (new Date()).getTime());

		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		final ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), mapException);
		logger.error("401 No tienes permisos para acceder a este recurso web "+request.getServletPath());
	}

	/**
	 * Se sobreescribe excepcion para ACCESO DENEGADO AccessDeniedException
	 */
	@ExceptionHandler(value = { AccessDeniedException.class })
	public void commence(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex ) throws IOException {
		final Map<String, Object> mapException = new HashMap<>();

		mapException.put("status", "401");
		mapException.put("error", "Insufficient Privileges");
		mapException.put("message", "No tienes permisos para acceder a este recurso "+request.getServletPath());
		mapException.put("exception", "No autorizado");
		mapException.put("path", request.getServletPath());
		mapException.put("timestamp", (new Date()).getTime());

		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		final ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), mapException);
		logger.error("401 No tienes permisos para acceder a este recurso "+request.getServletPath());
		
	}
	
	/**
	 * Se sobre escribe la excepcion ValidacionException para mostrar respuesta personalizada
	 * @author Sergio Arias
	 * @date 20/04/2020
	 * @param request
	 * @param response
	 * @param ex
	 * @throws IOException
	 */
	@ExceptionHandler(value = { ValidacionException.class })
	public void validarException(HttpServletRequest request, HttpServletResponse response, ValidacionException ex ) throws IOException {
		final Map<String, Object> mapException = new HashMap<>();

		mapException.put("status", "800");
		mapException.put("error", "Validación");
		mapException.put("message", ex.getMessage());
		//mapException.put("exception", "No autorizado");
		mapException.put("path", request.getServletPath());
		mapException.put("timestamp", (new Date()).getTime());

		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		final ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), mapException);
		
	}
	
}
