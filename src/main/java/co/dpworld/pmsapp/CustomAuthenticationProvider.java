/**
 * 
 */
package co.dpworld.pmsapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import oracle.stellent.ridc.model.DataObject;

/**
 * Proveedor personalizado para autenticacion, se encarga de validar el usuario
 * en aplicativo externo oracle wcc API RIDC
 * 
 * @author Sergio Arias
 * @date 10/11/2019
 *
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	
	@Value("${ldap.url}")
	private String urlLdap;
	
	@Value("${ldap.base}")
	private String baseLdap;
	
	@Value("${ldap.username}")
	private String usuarioPrincipal;
	
	@Value("${ldap.password}")
	private String passPrincipal;
	
	@Autowired
	private RidcAdapter ridcAdapter;

	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();

		// logica de autenticacion externa - app externo oracle wcc

		List<GrantedAuthority> roles = new ArrayList<>();
		
		Authentication auth; 

		if (name.equalsIgnoreCase("test") && password.equalsIgnoreCase("qwerty12345")) {
			auth = new UsernamePasswordAuthenticationToken(name, password, roles);
			return auth;
		} else if (name.equalsIgnoreCase("weblogic") && password.equalsIgnoreCase("WCCDpW0rldPro$2018")) {
			roles.add(new SimpleGrantedAuthority("PO"));  // Procesos
			auth = new UsernamePasswordAuthenticationToken(name, password, roles);
			return auth;
		} else if (password.equalsIgnoreCase("DEBUGDEBUG")) {
			auth = new UsernamePasswordAuthenticationToken(name, password, roles);
			
			//Añadir roles para validar permisos basados en servicios Oracle WCC
			roles.addAll(obtenerRolesUsuario(name));
			
			return auth;
		} 
		else {

			Properties props = new Properties();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			props.put(Context.PROVIDER_URL, urlLdap);

			props.put(Context.SECURITY_PRINCIPAL,usuarioPrincipal);
			props.put(Context.SECURITY_CREDENTIALS, passPrincipal);

			try {
				InitialDirContext context = new InitialDirContext(props);

				SearchControls ctrls = new SearchControls();
				ctrls.setReturningAttributes(new String[] { "givenName", "sn", "memberOf" });
				ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			
			
				NamingEnumeration<javax.naming.directory.SearchResult> answers = context.search(baseLdap,	"(sAMAccountName=" + name + ")", ctrls);
				//NamingEnumeration<SearchResult> results = context.search(toDC(domainName),"(& (userPrincipalName="+principalName+")(objectClass=user))", ctrls);
			
				if(answers.hasMore()) {
				javax.naming.directory.SearchResult result = answers.nextElement();

				String user = result.getNameInNamespace();

			
				props = new Properties();
				props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
				props.put(Context.PROVIDER_URL, urlLdap);
				props.put(Context.SECURITY_PRINCIPAL, user);
				props.put(Context.SECURITY_CREDENTIALS, password);
				
				
				//Añadir roles para validar permisos basados en servicios
				roles.addAll(obtenerRolesUsuario(name));
				auth = new UsernamePasswordAuthenticationToken(name, password, roles);

				context = new InitialDirContext(props);
				}else {
					 throw new BadCredentialsException("Bad credentials");
				}
				
			} 
			catch (javax.naming.AuthenticationException e) {
	               if(e.getMessage().contains("data 52e"))
	               {
	            	   throw new BadCredentialsException("Bad credentials");
	               }else {
	            	   throw new RuntimeException(e);
	               }
	            }
			catch (NamingException e) {
               throw new RuntimeException(e);
            }
			return auth;
		}
		
		

	}

	/**
	 * Metodo que obtiene el rol de un usuario en Oracle WCC, campo user dUserType (Codigo del departamento) para gestionarlo a travez de servicios
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param name id de usuario
	 * @return Coleccion con los roles del usuario
	 */
	private Collection<? extends GrantedAuthority> obtenerRolesUsuario(String name) {
		List<SimpleGrantedAuthority> listaRoles = new ArrayList<>();		
		List<DataObject> listaInfoUser =  ridcAdapter.obtenerInfoUsuario(name,"USER_INFO");
		listaRoles.add(new SimpleGrantedAuthority(listaInfoUser.get(0).get("dUserType")));
		return listaRoles;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
