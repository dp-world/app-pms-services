/**
 * 
 */
package co.dpworld.pmsapp.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import oracle.stellent.ridc.model.DataObject;

/**
 * Clase que contiene utilidades de la API RIDC Java Oracle WCC
 * 
 * @author Sergio Arias
 * @date 20/11/2019
 *
 */
@Service
public class UtilRidcWcc {

	@Autowired
	private RidcAdapter ridcUtilities;
	
	private static final int BUFFER_SIZE = 4096;

	/**
	 * Metodo que obtiene el nombre de un departamento segun su codigo
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param codigoDepartamento Codigo del departamento
	 * @return Descripcion/nombre del departamento
	 * @throws Exception
	 */
	@Deprecated()
	public String obtenerNombreDepartamentoPorCod(String codigoDepartamento) throws Exception {

		// Llamar servicio de listas departamentos
		List<DataObject> listaDoDepartamentos = ridcUtilities.obtenerValoresVistas("vistaDepartamento", "Departamento");
		for (DataObject fila : listaDoDepartamentos) {
			if (codigoDepartamento.equalsIgnoreCase(fila.get("Codigo")))
				return fila.get("Descripcion");
		}
		throw new Exception("No se ha encontrado el departamento codigo: " + codigoDepartamento);
	}

	/**
	 * Metodo que obtiene el nombre de una lista desplegable vistas segun su codigo,
	 * el metodo busca en un hashmap que contiene <br>
	 * key = codigo y value = descripcion
	 *
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param codigo Codigo del valor o lista a buscar
	 * @param hash   Hashmap con todos los datos en la vista de oracle wcc
	 * @return
	 * @throws Exception
	 */
	public String obtenerNombreListaPorCodHash(String codigo, Map<String, String> hash) throws Exception {

		for (Map.Entry<String, String> entry : hash.entrySet()) {
			if (codigo.equalsIgnoreCase(entry.getKey())) {
				return entry.getValue();
			}
		}
		return null;
	}

	public Map<String, String> obtenerClaveValorDepartamentoPorCod() throws Exception {
		Map<String, String> hashDepartamentos = new HashMap<>();

		// Llamar servicio de listas departamentos
		List<DataObject> listaDoDepartamentos = ridcUtilities.obtenerValoresVistas("vistaDepartamento", "Departamento");
		for (DataObject fila : listaDoDepartamentos) {
			hashDepartamentos.put(fila.get("Codigo"), fila.get("Descripcion"));
		}
		return hashDepartamentos;
	}

	public Map<String, String> obtenerClaveValorTipoDocumentosPorCod() throws Exception {
		Map<String, String> hashDocumentos = new HashMap<>();

		// Llamar servicio de lista tipoDocumentos
		List<DataObject> listaDoDepartamentos = ridcUtilities.obtenerValoresVistas("vistaTipoDocumentos",
				"tipoDocumento");
		for (DataObject fila : listaDoDepartamentos) {
			hashDocumentos.put(fila.get("docCode"), fila.get("docDescription"));
		}
		return hashDocumentos;
	}

	public Map<String, String> obtenerClaveValorFuentesCreacionPorCod() throws Exception {
		Map<String, String> hashDocumentos = new HashMap<>();

		// Llamar servicio de lista tipoDocumentos
		List<DataObject> listaDoDepartamentos = ridcUtilities.obtenerValoresVistas("vistaFuenteCreacion",
				"FuenteCreacion");
		for (DataObject fila : listaDoDepartamentos) {
			hashDocumentos.put(fila.get("IDFuente"), fila.get("DescripcionFuente"));
		}
		return hashDocumentos;
	}

	/**
	 * Metodo que obtiene el nombre de razon creacion segun su codigo
	 * 
	 * @author Sergio Arias
	 * @date 21/11/2019
	 * @param codRazonCreacion Codigo de la razon de creacion
	 * @return Descripcion/nombre de la razon o creacion
	 * @throws Exception
	 */
	public String obtenerNombreRazonCreacionPorCod(String codRazonCreacion) throws Exception {
		// Llamar servicio de listas razon creacion
		List<DataObject> listaRazonCreacion = ridcUtilities.obtenerValoresVistas("vistaFuenteCreacion",
				"FuenteCreacion");
		for (DataObject fila : listaRazonCreacion) {
			if (codRazonCreacion.equalsIgnoreCase(fila.get("IDFuente")))
				return fila.get("DescripcionFuente");
		}
		throw new Exception("No se ha encontrado razon creacion codigo: " + codRazonCreacion);
	}

	public String obtenerNombreTipologiaPorCo(String codTipologia) throws Exception {
		// Llamar servicio de listas tipologia
		List<DataObject> listaTipologias = ridcUtilities.obtenerValoresVistas("vistaTipoDocumentos", "tipoDocumento");
		for (DataObject fila : listaTipologias) {
			if (codTipologia.equalsIgnoreCase(fila.get("docCode")))
				return fila.get("docDescription");
		}
		return "";
	}

	public String obtenerCodigoTipoSolicitud(String descripcion) {
		// TODO Falta implementacion de vistas
		if (descripcion.equalsIgnoreCase("Creacion")) {
			return "01";
		} else if (descripcion.equalsIgnoreCase("Actualizacion")) {
			return "02";
		} else if (descripcion.equalsIgnoreCase("Obsoletizacion")) {
			return "03";
		} else if (descripcion.equalsIgnoreCase("Reactivacion")) {
			return "04";
		} else if (descripcion.equalsIgnoreCase("Nomenclatura")) {
			return "05";
		} else {
			return null;
		}
	}

	public String obtenerCodigoTipoSolicitudPorCodigo(String codSolicitud) {
		if (codSolicitud.equalsIgnoreCase("01")) {
			return "Creacion";
		} else if (codSolicitud.equalsIgnoreCase("02")) {
			return "Actualizacion";
		} else if (codSolicitud.equalsIgnoreCase("03")) {
			return "Obsoletizacion";
		} else if (codSolicitud.equalsIgnoreCase("04")) {
			return "Reactivacion";
		} else if (codSolicitud.equalsIgnoreCase("05")) {
			return "Nomenclatura";
		} else {
			return null;
		}
	}

	/**
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param string
	 * @param hashDepartamentos
	 * @return
	 */
	public String obtenerNombreProcesoPorCodHash(String codigo, Map<String, String> hashDepartamentos) {
		for (Map.Entry<String, String> entry : hashDepartamentos.entrySet()) {
			if (codigo.equalsIgnoreCase(entry.getKey())) {
				return entry.getValue();
			}
		}
		return null;
	}

	public static byte[] obtenerBase64PorUri(String docUrl, String user, String pass,String sessionID)
			throws IOException {
		System.out.println("URL: " + docUrl);
		URL url = new URL(docUrl);
		String inputLine;
		
		// Realizar peticion GET con autentication basic
		URLConnection hc = url.openConnection();
		HttpURLConnection httpConn = (HttpURLConnection) hc;
		httpConn.setRequestMethod("GET");
		hc.setDoInput(true);
		hc.setDoOutput(true);
		String authorization = "";
		authorization = user + ":" + pass;
		byte[] encodedBytes;
		encodedBytes = Base64.getEncoder().encode(authorization.getBytes());
		authorization = "Basic " + encodedBytes;
		httpConn.setRequestProperty("Authorization", authorization); 
		httpConn.setRequestProperty("Cookie","JSESSIONID="+sessionID); // Cookies del servicio API Oracle RIDC 
		httpConn.connect();
		

		
		int responseCode = httpConn.getResponseCode();
		System.out.println("HTTP RESPONSE: " + httpConn.getResponseCode());
		
		 // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();
            
            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = docUrl.substring(docUrl.lastIndexOf("/") + 1,
                		docUrl.length());
            }
            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);
            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            byte[] b = IOUtils.toByteArray(inputStream);
            
            //String saveFilePath = rutaDescarga;
            
            // opens an output stream to save into file
            //FileOutputStream outputStream = new FileOutputStream(saveFilePath);
 
            //int bytesRead = -1;
            //byte[] buffer = new byte[BUFFER_SIZE];
            //while ((bytesRead = inputStream.read(buffer)) != -1) {
              //  outputStream.write(buffer, 0, bytesRead);
            //}
 
            
            //outputStream.close();
            inputStream.close();
            return b;
            
        }else {
	        return null;
        }
	        
	}

}
