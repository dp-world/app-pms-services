/**
 * 
 */
package co.dpworld.pmsapp.util.wf;

import static co.dpworld.pmsapp.util.EMetadataOracleWCC.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.model.AnexoRq;
import co.dpworld.pmsapp.model.Departamento;
import co.dpworld.pmsapp.model.DocumentoRp;
import co.dpworld.pmsapp.model.DocumentoRq;
import co.dpworld.pmsapp.model.MetadataClaveValor;
import co.dpworld.pmsapp.model.RazonCreacion;
import co.dpworld.pmsapp.model.Tipologia;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import oracle.stellent.ridc.model.DataObject;

/**
 * Servicio de utilidades para flujo de creacion
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@Service
public class UtilWorkflowCreacion {

	Logger logger = LoggerFactory.getLogger(UtilWorkflowCreacion.class);

	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private UtilRidcWcc utilRidcWcc;

	@Autowired
	private Environment env;

	@Autowired
	private UtilWorkflowGeneral utilWorkflowGeneral;

	/**
	 * Metodo que valida e inicia la solicitud para creacion de un documento
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws IOException
	 */
	public boolean validarSolicitudCreacion(DocumentoRq creacionDocumentoRq) throws IOException {
		// 2. Realizar el checkin de la nueva version
		String id = tipoSolicitudCreacionDocumento(creacionDocumentoRq);
		if (id != null) {

			
			if (creacionDocumentoRq.getDocumentoAnexo() != null) {
				AnexoRq anexo = new AnexoRq();
				anexo.setDocumentoAnexo(creacionDocumentoRq.getDocumentoAnexo());
				anexo.setIdDocumentoPrincipal(id);
				anexo.setTituloDocumento(creacionDocumentoRq.getTituloDocumentoAnexo());
				return utilWorkflowGeneral.anexarDocumento(anexo);
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * Metodo que envia la solicitud para la creacion de un documento nuevo
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws IOException
	 */
	public String tipoSolicitudCreacionDocumento(DocumentoRq creacionDocumentoRq) throws IOException {
		// Ruta temporal para escribir los archivos en el file system
		String rutaTemporal = env.getProperty("wcc.ruta.temporal");

		try {

			// 1. Convertir metadata personalizada
			List<MetadataClaveValor> listaMetadata = new ArrayList<>();
			listaMetadata.add(
					new MetadataClaveValor(SOLICITANTE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getSolicitante()));
			listaMetadata
					.add(new MetadataClaveValor(TIPOCAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getTipoCambio()));
			listaMetadata.add(new MetadataClaveValor(TIPOPROCESO_SIMBOLICWCC.getVal(),
					utilRidcWcc.obtenerCodigoTipoSolicitud(creacionDocumentoRq.getTipoSolicitud())));
			listaMetadata.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getDepartamento().getCodigo()));
			listaMetadata.add(new MetadataClaveValor(RAZONCREACION_SIMBOLICWCC.getVal(),
					String.valueOf(creacionDocumentoRq.getRazonFuenteDeCreacion().getCodigo())));
			listaMetadata.add(new MetadataClaveValor(TIPODOCUMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getTipoDocumento().getCodigo()));
			listaMetadata.add(new MetadataClaveValor(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal(),
					(creacionDocumentoRq.isRequiereAdiestramiento() ? "Si" : "No")));
			listaMetadata.add(new MetadataClaveValor(REQUIERESOPORTE_SIMBOLICWCC.getVal(),
					(creacionDocumentoRq.isRequiereSoporte() ? "1" : "0")));
			listaMetadata.add(new MetadataClaveValor(JUSTIFICACION_SIMBOLICWCC.getVal(), creacionDocumentoRq.getJustificacion()));

			listaMetadata.add(new MetadataClaveValor(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getTituloDocumento()));
			listaMetadata.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getRazonCambio()));
			listaMetadata.add(new MetadataClaveValor(COMENTARIOS_SIMBOLICWCC.getVal(), creacionDocumentoRq.getComentarios()));
		
			
			// 2. Convertir base64 a File
			File archivo = new File(rutaTemporal + "\\" + creacionDocumentoRq.getTituloDocumento());

			FileUtils.writeByteArrayToFile(archivo, creacionDocumentoRq.getDocumentobase64());

			// 3. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();
			
			listaMetadata.add(
					new MetadataClaveValor("xAutorDoc", currentPrincipalName));

			// 4. Subir documento a Oracle WebCenter Content por la API RIDC
			String id = ridcAdapter.subirDocumento(listaMetadata, creacionDocumentoRq.getTituloDocumento(), // Doctitle
					creacionDocumentoRq.getTituloDocumento(), // Original Name
					"Documento_DP-World", // Doctype
					"Processes", // Grupo de seguridad
					creacionDocumentoRq.getTipoSolicitud(), // idcProfile
					archivo, // File
					currentPrincipalName); // Autor
			
			


			return id;

		} finally {
			// Borrar documentos en la ruta temporal si existen
			utilWorkflowGeneral.borrarDocumentosTemporales(rutaTemporal);
		}
	}

	/**
	 * Metodo que envia la solicitud de creacion de un documento para su
	 * versionamiento
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @param idActual
	 * @param dDocName
	 * @return
	 * @throws IOException
	 */
	public String tipoSolicitudVersionamientoDocumento(DocumentoRq creacionDocumentoRq, String idActual,
			String dDocName) throws IOException {
		// Ruta temporal para escribir los archivos en el file system
		String rutaTemporal = env.getProperty("wcc.ruta.temporal");

		try {

			// 1. Convertir metadata personalizada
			List<MetadataClaveValor> listaMetadata = new ArrayList<>();
			listaMetadata.add(
					new MetadataClaveValor(SOLICITANTE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getSolicitante()));
			listaMetadata
					.add(new MetadataClaveValor(TIPOCAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getTipoCambio()));
			listaMetadata.add(new MetadataClaveValor(TIPOPROCESO_SIMBOLICWCC.getVal(),
					utilRidcWcc.obtenerCodigoTipoSolicitud(creacionDocumentoRq.getTipoSolicitud())));

			if (creacionDocumentoRq.getDepartamento() != null) {
				listaMetadata.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(),
						creacionDocumentoRq.getDepartamento().getCodigo()));
			}

			if (creacionDocumentoRq.getRazonFuenteDeCreacion() != null) {
				listaMetadata.add(new MetadataClaveValor(RAZONCREACION_SIMBOLICWCC.getVal(),
						String.valueOf(creacionDocumentoRq.getRazonFuenteDeCreacion().getCodigo())));
			}

			if (creacionDocumentoRq.getTipoDocumento() != null) {
				listaMetadata.add(new MetadataClaveValor(TIPODOCUMENTO_SIMBOLICWCC.getVal(),
						creacionDocumentoRq.getTipoDocumento().getCodigo()));
			}

			listaMetadata.add(new MetadataClaveValor(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal(),
					(creacionDocumentoRq.isRequiereAdiestramiento() ? "Si" : "No")));
			listaMetadata.add(new MetadataClaveValor(REQUIERESOPORTE_SIMBOLICWCC.getVal(),
					(creacionDocumentoRq.isRequiereSoporte() ? "1" : "0")));
			listaMetadata.add(new MetadataClaveValor(JUSTIFICACION_SIMBOLICWCC.getVal(), creacionDocumentoRq.getJustificacion()));

			listaMetadata.add(new MetadataClaveValor(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getNombreDocumento()));
			listaMetadata.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getRazonCambio()));
			listaMetadata.add(new MetadataClaveValor(COMENTARIOS_SIMBOLICWCC.getVal(), creacionDocumentoRq.getComentarios()));

			// 2. Convertir base64 a File
			File archivo = new File(rutaTemporal + "\\" + creacionDocumentoRq.getTituloDocumento());

			FileUtils.writeByteArrayToFile(archivo, creacionDocumentoRq.getDocumentobase64());

			// 3. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();
			
			listaMetadata.add(
					new MetadataClaveValor("xAutorDoc", currentPrincipalName));
			
			// 4. Versionar documento a Oracle WebCenter Content por la API RIDC
			boolean resultado = ridcAdapter.versionarDocumentoConMetadata(creacionDocumentoRq.getTituloDocumento(),
					creacionDocumentoRq.getTituloDocumento(), archivo, idActual, dDocName, listaMetadata,currentPrincipalName);

			return resultado ? "0" : null;

		} finally {
			// Borrar documentos en la ruta temporal si existen
			utilWorkflowGeneral.borrarDocumentosTemporales(rutaTemporal);
		}
	}

	/**
	 * Metodo que mapea los datos del resulset al objeto de respuesta del servicio
	 * rest
	 * 
	 * @author Sergio Arias
	 * @date 21/11/2019
	 * @param listaDocInfo Lista con la metadata retornada por el resulset de oracle
	 *                     wcc
	 * @return
	 * @throws Exception
	 */
	public DocumentoRp mapearDatosCreacionDocumento(DataObject docInfo) throws Exception {

		DocumentoRp creacionDocumentoRp = new DocumentoRp();

		// 1. Departamento = xDepartamento
		String codDepartamento = docInfo.get(DEPARTAMENTO_SIMBOLICWCC.getVal());
		Departamento departamento = new Departamento();
		departamento.setCodigo(codDepartamento);
		departamento.setNombreDepartamento(utilRidcWcc.obtenerNombreDepartamentoPorCod(codDepartamento));
		creacionDocumentoRp.setDepartamento(departamento);

		// 2. Razon fuente de creacion = xrazonCreacion
		String codRazonCreacion = docInfo.get(RAZONCREACION_SIMBOLICWCC.getVal());
		RazonCreacion razonCreacion = new RazonCreacion();
		if (codRazonCreacion != null && !codRazonCreacion.isEmpty()) {
			razonCreacion.setCodigo(codRazonCreacion != null ? Integer.parseInt(codRazonCreacion) : 0);
			razonCreacion.setNombreRazonFuenteCreacion(utilRidcWcc.obtenerNombreRazonCreacionPorCod(codRazonCreacion));
			creacionDocumentoRp.setRazonFuenteDeCreacion(razonCreacion);
		}

		// 3. Requiere adiestramiento =
		if (docInfo.get(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal()) != null
				&& !docInfo.get(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal()).isEmpty()) {
			boolean requiereAdiestramiento = docInfo.get(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal())
					.equalsIgnoreCase("si") ? true : false;
			creacionDocumentoRp.setRequiereAdiestramiento(requiereAdiestramiento);
		}

		// 4. Requiere Soporte
		if (docInfo.get(REQUIERESOPORTE_SIMBOLICWCC.getVal()) != null
				&& !docInfo.get(REQUIERESOPORTE_SIMBOLICWCC.getVal()).isEmpty()) {
			boolean requiereSoporte = docInfo.get(REQUIERESOPORTE_SIMBOLICWCC.getVal()).equalsIgnoreCase("1") ? true
					: false;
			creacionDocumentoRp.setRequiereSoporte(requiereSoporte);
		}

		// 5. Solicitante
		creacionDocumentoRp.setSolicitante(docInfo.get(SOLICITANTE_SIMBOLICWCC.getVal()));

		// 6. Tipo de Cambio
		creacionDocumentoRp.setTipoCambio(docInfo.get(TIPOCAMBIO_SIMBOLICWCC.getVal()));

		// 7. Tipologia
		String codTipologia = docInfo.get(TIPODOCUMENTO_SIMBOLICWCC.getVal());
		if (codTipologia != null && !codTipologia.isEmpty()) {
			Tipologia tipologia = new Tipologia();
			tipologia.setCodigo(codTipologia);
			tipologia.setNombreTipologia(utilRidcWcc.obtenerNombreTipologiaPorCo(codTipologia));
			if (!tipologia.getNombreTipologia().isEmpty()) {
				creacionDocumentoRp.setTipoDocumento(tipologia);
			}
		}

		// 8. Tipo Solicitud

		String nombreSolicitud = utilRidcWcc
				.obtenerCodigoTipoSolicitudPorCodigo(docInfo.get(TIPOPROCESO_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setTipoSolicitud(nombreSolicitud);

		// 9. Titulo Documento
		creacionDocumentoRp.setTituloDocumento(docInfo.get(NOMBREDOCUMENTO_SIMBOLICWCC.getVal()));

		// 10. Nombre documento
		creacionDocumentoRp.setNombreDocumento(docInfo.get(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal()));

		// Nomenclatura
		creacionDocumentoRp.setNomenclatura(docInfo.get(NOMENCLATURA_SIMBOLICWCC.getVal()));

		// Campos flujo cambio de nomenclatura
		creacionDocumentoRp.setRazonCambio(docInfo.get(RAZON_CAMBIO_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setAreaActual(docInfo.get(AREA_ACTUAL_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setAreaPropuesta(docInfo.get(AREA_PROPUESTA_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setCambioTipoDocumentoA(docInfo.get(CAMBIO_TIPO_DOCUMENTO_A_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setCambioTipoDocumentoDe(docInfo.get(CAMBIO_TIPO_DOCUMENTO_DE_SIMBOLICWCC.getVal()));

		// Campo para ver rechazos
		creacionDocumentoRp.setJustificacion(docInfo.get(JUSTIFICACION_SIMBOLICWCC.getVal()));

		// Campos ficha de datos del documento
		creacionDocumentoRp.setTiempoRetencion(docInfo.get(TIEMPO_RETENCION_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setMedioAlmacenamiento(docInfo.get(MEDIO_ALMACENAMIENTO_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setRevision(docInfo.get(REVISION_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setResponsable(docInfo.get(RESPONSABLE_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setClasificacion(docInfo.get(CLASIFICACION_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setFechaProximaRevision(docInfo.get(FECHA_PROXIMA_REVISION_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setLiderProceso(docInfo.get(LIDER_PROCESO_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setFrecuenciaRevision(docInfo.get(FRECUENCIA_REVISION_SIMBOLICWCC.getVal()));
		creacionDocumentoRp.setFechaVencimiento(docInfo.get(FECHA_VENCIMIENTO_SIMBOLICWCC.getVal()));
		
		creacionDocumentoRp.setComentarios(docInfo.get(COMENTARIOS_SIMBOLICWCC.getVal()));

		return creacionDocumentoRp;
	}

}
