/**
 * 
 */
package co.dpworld.pmsapp.util.wf;

import static co.dpworld.pmsapp.util.EMetadataOracleWCC.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.exception.ValidacionException;
import co.dpworld.pmsapp.model.ActualizarPasoRq;
import co.dpworld.pmsapp.model.DocumentoRq;
import co.dpworld.pmsapp.model.MetadataClaveValor;
import co.dpworld.pmsapp.repo.CompositeKeyNomenclatura;
import co.dpworld.pmsapp.repo.PNomenclaturaCategoria;
import co.dpworld.pmsapp.repo.PNomenclaturaDepartamentoCategoria;
import co.dpworld.pmsapp.repo.i.IPNomenclaturaCategoriaRepo;
import co.dpworld.pmsapp.repo.i.IPNomenclaturaDepartamentoCategoriaRepo;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import oracle.stellent.ridc.model.DataObject;

/**
 * Servicio de utilidades para flujo cambio de nomenclatura
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@Service
public class UtilWorkflowCambioNomenclatura {

	Logger logger = LoggerFactory.getLogger(UtilWorkflowCambioNomenclatura.class);

	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private Environment env;

	@Autowired
	private UtilWorkflowGeneral utilWorkflowGeneral;

	@Autowired
	private UtilRidcWcc utilRidcWcc;
	
	@Autowired
	private IPNomenclaturaDepartamentoCategoriaRepo nomenclaturaDepCatRepo;
	
	@Autowired
	private IPNomenclaturaCategoriaRepo nomenclaturaCatRepo;

	/**
	 * Metodo que envia la solicitud para el cambio de nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws Exception 
	 */
	public boolean tipoSolicitudNomenclatura(DocumentoRq creacionDocumentoRq) throws Exception {

		// 1. Busca documento actual
		String queryBuscarDocumentoPorNombre = "(xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> xProceso <matches> `04` ) " // No
																											// obsoletizacion
				+ "<AND> xNombreDocumento <substring> `" + creacionDocumentoRq.getNombreDocumento() + "`";

		List<DataObject> resultado = ridcAdapter.buscar(queryBuscarDocumentoPorNombre, "SearchResults");

		if (!resultado.isEmpty()) {
			logger.info("DOCUMENTO ENCONTRADO dDocTitle -> " + creacionDocumentoRq.getNombreDocumento());
			logger.info("dID: " + resultado.get(0).get("dID"));
			logger.info("dDocName: " + resultado.get(0).get("dDocName"));

			String idExistente = resultado.get(0).get("dID");
			String dDocName = resultado.get(0).get("dDocName");

			// 2. Realizar checkout del documento encontrado
			ridcAdapter.checkout(idExistente);

			// 3. Realizar el checkin de la nueva version
			return tipoSolicitudVersionamientoCambioNomenclatura(creacionDocumentoRq, idExistente, dDocName) == null
					? false
					: true;

		} else {

			logger.info("DOCUMENTO NO ENCONTRADO dDocTitle -> " + creacionDocumentoRq.getNombreDocumento());
			
			if(env.getProperty("validate.wf.existencia.documento").equalsIgnoreCase("true")) {
				throw new ValidacionException("EL DOCUMENTO CON TITULO '"+creacionDocumentoRq.getNombreDocumento()+"' NO EXISTE EN EL SISTEMA.");
			}else {
				tipoSolicitudCambioNomenclatura(creacionDocumentoRq);
			}
		}
		return false;

	}

	/**
	 * Metodo auxiliar que obtiene secuencia de nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return
	 */
	public int obtenerSecuencia() {
		try {
			FileReader reader = new FileReader("C://tmp//MyFile.txt");
			BufferedReader bufferedReader = new BufferedReader(reader);
			String line;
			String dato = "";
			while ((line = bufferedReader.readLine()) != null) {
				dato += line;
			}
			reader.close();

			// Obtener secuencia
			int secuencia = Integer.parseInt(dato.trim());
			int secuenciaNueva = secuencia + 1;

			// Escribir secuencia
			/*
			 * FileWriter writer = new FileWriter("C://tmp//MyFile.txt", true);
			 * BufferedWriter bufferedWriter = new BufferedWriter(writer);
			 * bufferedWriter.write(secuencia); bufferedWriter.close();
			 */

			Path path = Paths.get("C://tmp//MyFile.txt");
			Charset charset = StandardCharsets.UTF_8;

			String content = new String(Files.readAllBytes(path), charset);
			content = content.replaceAll(String.valueOf(secuencia), String.valueOf(secuenciaNueva));
			Files.write(path, content.getBytes(charset));

			return secuenciaNueva;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Metodo auxiliar que realiza validaciones de la generacion de nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param actualizarPasoRq
	 * @param idPaso
	 * @param listaMetadataClaveValor
	 */
	public void validacionGeneracionNomenclatura(ActualizarPasoRq actualizarPasoRq, String idPaso,
			List<MetadataClaveValor> listaMetadataClaveValor) {
		
		// Se valida el paso para saber si se genera el numero de nomenclatura
		// 401 = Aprobación Gerente/Supervisor de procesos
		if (idPaso != null && idPaso.equalsIgnoreCase("401")) {
			
			// 1. Armar composite key para busqueda por 2 columnas (codigo categoria y codigo departamento)
			CompositeKeyNomenclatura k = new CompositeKeyNomenclatura();
			k.setCodigoCategoria(actualizarPasoRq.getTipoDocumento().getCodigo());
			k.setCodigoDepartamento(actualizarPasoRq.getDepartamento().getCodigo());
			
			// 2 Buscar secuencia nomenclatura (Departamento x Categoria) y aumentar la secuencia en base de datos tablas parametricas nomenclatura
		    PNomenclaturaDepartamentoCategoria parametricaDepCat = nomenclaturaDepCatRepo.findOne(k);
	        parametricaDepCat.setSecuenciaNomenclaturaDepCat(parametricaDepCat.getSecuenciaNomenclaturaDepCat() + 1);
	        nomenclaturaDepCatRepo.save(parametricaDepCat);
			
	       
	        // 3 Buscar secuencia nomenclatura (Categoria) y aumentar la secuencia en base de datos tablas parametricas nomenclatura
	        PNomenclaturaCategoria parametricaCat = nomenclaturaCatRepo.findOne(actualizarPasoRq.getTipoDocumento().getCodigo());
	        parametricaCat.setSecuenciaNomenclaturaCat(parametricaCat.getSecuenciaNomenclaturaCat() + 1);
	        nomenclaturaCatRepo.save(parametricaCat);
	        
	        // 4 Generar la nomenclatura segun un formato especifico y asignarla a la metadata
			generarFormatoNomenclatura(parametricaDepCat,parametricaCat, listaMetadataClaveValor);
		}
	}

	/**
	 * Metodo que genera el formato de la nomenclatura siguiendo lo siguiente: <br> <br>
	 * FORMATO NOMENCLATURA GENERADO: <b> XX-##-###-## </b> <br><br>
	 * 
	 * <b> XX = </b> Sigla del area perteneciente <br> <br>
	 * <b> ## = </b> Categoria del documento <br> <br>
	 * <b> ### = </b> Secuencia númerica iniciando con 001 inidicando el primero en su categoria y area <br> <br>
	 * <b> ## = </b> Secuencia númerica del formulario de ese documento inicando con el numero 01 para indicar que es el primer formulario relacionado al documento <br> <br>
	 * 
	 * @author Sergio Arias
	 * @date 15/03/2020
	 * @param parametricaDepCat Entidad que contiene los datos asociados a la secuencia de nomenclatura por departamento y categoria
	 * @param parametricaCat  Entidad que contiene los datos asociados a la secuencia de nomenclatura por categoria
	 * @param listaMetadataClaveValor Lista que contiene la metadata a actualizar en el workflow
	 */
	private void generarFormatoNomenclatura(PNomenclaturaDepartamentoCategoria parametricaDepCat,
			PNomenclaturaCategoria parametricaCat, List<MetadataClaveValor> listaMetadataClaveValor) {

		String secuenciaDepCat = String.valueOf(parametricaDepCat.getSecuenciaNomenclaturaDepCat());

		// TODO Se desativa temporalmente alica para formularios
		//String secuenciaCat = String.valueOf(parametricaCat.getSecuenciaNomenclaturaCat());
		
		String nomenclatura = parametricaDepCat.getCodigoDepartamento() + "-"
				            + parametricaDepCat.getCodigoCategoria() + "-" 
				            + ((secuenciaDepCat.length() <= 3) ? ("000" + secuenciaDepCat).substring(secuenciaDepCat.length()) : secuenciaDepCat); 
				            //+ "-"+ ((secuenciaCat.length() <= 2)    ? ("00"  + secuenciaCat).substring(secuenciaCat.length()) : secuenciaCat); 
		listaMetadataClaveValor.add(new MetadataClaveValor(NOMENCLATURA_SIMBOLICWCC.getVal(), nomenclatura));
	}

	/**
	 * Metodo que inicia solicitud de cambio nomeclatura
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws IOException
	 */
	private boolean tipoSolicitudCambioNomenclatura(DocumentoRq creacionDocumentoRq) throws IOException {
		// Ruta temporal para escribir los archivos en el file system
		String rutaTemporal = env.getProperty("wcc.ruta.temporal");

		try {

			// 1. Convertir metadata personalizada
			List<MetadataClaveValor> listaMetadata = new ArrayList<>();
			listaMetadata.add(
					new MetadataClaveValor(SOLICITANTE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getSolicitante()));
			listaMetadata.add(new MetadataClaveValor(TIPOPROCESO_SIMBOLICWCC.getVal(),
					utilRidcWcc.obtenerCodigoTipoSolicitud(creacionDocumentoRq.getTipoSolicitud())));
			listaMetadata.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getDepartamento().getCodigo()));
			listaMetadata.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getRazonCambio()));
			listaMetadata.add(new MetadataClaveValor(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getNombreDocumento()));
			listaMetadata.add(new MetadataClaveValor(JUSTIFICACION_SIMBOLICWCC.getVal(), creacionDocumentoRq.getJustificacion()));

			// Campos Nomenclatura
			listaMetadata.add(new MetadataClaveValor(AREA_ACTUAL_SIMBOLICWCC.getVal(), creacionDocumentoRq.getAreaActual()));
			listaMetadata.add(new MetadataClaveValor(AREA_PROPUESTA_SIMBOLICWCC.getVal(), creacionDocumentoRq.getAreaPropuesta()));
			listaMetadata.add(
					new MetadataClaveValor(CAMBIO_TIPO_DOCUMENTO_DE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getCambioTipoDocumentoDe()));
			listaMetadata.add(
					new MetadataClaveValor(CAMBIO_TIPO_DOCUMENTO_A_SIMBOLICWCC.getVal(), creacionDocumentoRq.getCambioTipoDocumentoA()));
			listaMetadata.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getRazonCambio()));

			// 2. Convertir base64 a File
			File archivo = new File(rutaTemporal + "\\" + creacionDocumentoRq.getTituloDocumento());

			FileUtils.writeByteArrayToFile(archivo, creacionDocumentoRq.getDocumentobase64());

			// 3. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();
		
			listaMetadata.add(
					new MetadataClaveValor("xAutorDoc", currentPrincipalName));
			
			// 4. Subir documento a Oracle WebCenter Content por la API RIDC
			ridcAdapter.subirDocumento(listaMetadata, creacionDocumentoRq.getTituloDocumento(), // Doctitle
					creacionDocumentoRq.getTituloDocumento(), // Original Name
					"Documento_DP-World", // Doctype
					"Processes", // Grupo de seguridad
					creacionDocumentoRq.getTipoSolicitud(), // idcProfile
					archivo, // File
					currentPrincipalName); // Autor

			return true;

		} finally {
			// Borrar documentos en la ruta temporal si existen
			utilWorkflowGeneral.borrarDocumentosTemporales(rutaTemporal);
		}
	}

	/**
	 * Metodo que inicia solicitud de versionamiento para cambio de nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @param creacionDocumentoRq
	 * @param idExistente
	 * @param dDocName
	 * @return
	 * @throws IOException
	 */
	public Object tipoSolicitudVersionamientoCambioNomenclatura(DocumentoRq creacionDocumentoRq, String idExistente,
			String dDocName) throws IOException {
		// Ruta temporal para escribir los archivos en el file system
		String rutaTemporal = env.getProperty("wcc.ruta.temporal");

		try {

			// 1. Convertir metadata personalizada
			List<MetadataClaveValor> listaMetadata = new ArrayList<>();
			listaMetadata.add(
					new MetadataClaveValor(SOLICITANTE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getSolicitante()));
			listaMetadata.add(new MetadataClaveValor(TIPOPROCESO_SIMBOLICWCC.getVal(),
					utilRidcWcc.obtenerCodigoTipoSolicitud(creacionDocumentoRq.getTipoSolicitud())));
			listaMetadata.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getDepartamento().getCodigo()));
			listaMetadata.add(new MetadataClaveValor(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getNombreDocumento()));
			listaMetadata.add(new MetadataClaveValor(JUSTIFICACION_SIMBOLICWCC.getVal(), creacionDocumentoRq.getJustificacion()));

			// Campos Nomenclatura
			listaMetadata.add(new MetadataClaveValor(AREA_ACTUAL_SIMBOLICWCC.getVal(), creacionDocumentoRq.getAreaActual()));
			listaMetadata.add(new MetadataClaveValor(AREA_PROPUESTA_SIMBOLICWCC.getVal(), creacionDocumentoRq.getAreaPropuesta()));
			listaMetadata.add(
					new MetadataClaveValor(CAMBIO_TIPO_DOCUMENTO_DE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getCambioTipoDocumentoDe()));
			listaMetadata.add(
					new MetadataClaveValor(CAMBIO_TIPO_DOCUMENTO_A_SIMBOLICWCC.getVal(), creacionDocumentoRq.getCambioTipoDocumentoA()));
			listaMetadata.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getRazonCambio()));
			listaMetadata.add(new MetadataClaveValor(RAZONCREACION_SIMBOLICWCC.getVal(),
					String.valueOf(creacionDocumentoRq.getRazonFuenteDeCreacion().getCodigo())));
			listaMetadata.add(new MetadataClaveValor(COMENTARIOS_SIMBOLICWCC.getVal(), creacionDocumentoRq.getComentarios()));
			
			
			// 2. Convertir base64 a File
			File archivo = new File(rutaTemporal + "\\" + creacionDocumentoRq.getTituloDocumento());

			FileUtils.writeByteArrayToFile(archivo, creacionDocumentoRq.getDocumentobase64());

			// 3. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();
			
			listaMetadata.add(
					new MetadataClaveValor("xAutorDoc", currentPrincipalName));
			
			// 4. Versionar documento a Oracle WebCenter Content por la API RIDC
			boolean resultado = ridcAdapter.versionarDocumentoConMetadata(creacionDocumentoRq.getTituloDocumento(),
					creacionDocumentoRq.getTituloDocumento(), archivo, idExistente, dDocName, listaMetadata,currentPrincipalName);

			return resultado ? "0" : null;

		} finally {
			// Borrar documentos en la ruta temporal si existen
			utilWorkflowGeneral.borrarDocumentosTemporales(rutaTemporal);
		}
	}

}
