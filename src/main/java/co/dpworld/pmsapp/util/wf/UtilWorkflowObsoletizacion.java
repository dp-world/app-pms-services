/**
 * 
 */
package co.dpworld.pmsapp.util.wf;

import static co.dpworld.pmsapp.util.EMetadataOracleWCC.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.exception.ValidacionException;
import co.dpworld.pmsapp.model.DocumentoRq;
import co.dpworld.pmsapp.model.MetadataClaveValor;
import co.dpworld.pmsapp.util.UtilRidcWcc;
import oracle.stellent.ridc.model.DataObject;

/**
 * Servicio de utilidades para flujo de obsoletizacion
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@Service
public class UtilWorkflowObsoletizacion {

	Logger logger = LoggerFactory.getLogger(UtilWorkflowObsoletizacion.class);

	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private Environment env;

	@Autowired
	private UtilWorkflowReactivacion utilWorkflowReactivacion;

	@Autowired
	private UtilRidcWcc utilRidcWcc;

	@Autowired
	private UtilWorkflowGeneral utilWorkflowGeneral;

	/**
	 * Metodo que envia la solciitud de obsoletizacion de un documento para su
	 * versionamiento
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @param idExistente
	 * @param dDocName
	 * @return
	 * @throws IOException
	 */
	public Object tipoSolicitudVersionamientoObsoletizacionDoucmento(DocumentoRq creacionDocumentoRq,
			String idExistente, String dDocName) throws IOException {
		// Ruta temporal para escribir los archivos en el file system
		String rutaTemporal = env.getProperty("wcc.ruta.temporal");

		try {

			// 1. Convertir metadata personalizada
			List<MetadataClaveValor> listaMetadata = new ArrayList<>();
			listaMetadata.add(
					new MetadataClaveValor(SOLICITANTE_SIMBOLICWCC.getVal(), creacionDocumentoRq.getSolicitante()));
			listaMetadata
					.add(new MetadataClaveValor(TIPOCAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getTipoCambio()));
			listaMetadata.add(new MetadataClaveValor(TIPOPROCESO_SIMBOLICWCC.getVal(),
					utilRidcWcc.obtenerCodigoTipoSolicitud(creacionDocumentoRq.getTipoSolicitud())));
			listaMetadata.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getDepartamento().getCodigo()));
			listaMetadata.add(new MetadataClaveValor(RAZONCREACION_SIMBOLICWCC.getVal(),
					String.valueOf(creacionDocumentoRq.getRazonFuenteDeCreacion().getCodigo())));
			listaMetadata.add(new MetadataClaveValor(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal(),
					(creacionDocumentoRq.isRequiereAdiestramiento() ? "Si" : "No")));
			listaMetadata.add(new MetadataClaveValor(REQUIERESOPORTE_SIMBOLICWCC.getVal(),
					(creacionDocumentoRq.isRequiereSoporte() ? "1" : "0")));
			listaMetadata.add(new MetadataClaveValor(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal(),
					creacionDocumentoRq.getNombreDocumento()));
			listaMetadata.add(new MetadataClaveValor(JUSTIFICACION_SIMBOLICWCC.getVal(), creacionDocumentoRq.getJustificacion()));
			listaMetadata.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), creacionDocumentoRq.getRazonCambio()));
			listaMetadata.add(new MetadataClaveValor(COMENTARIOS_SIMBOLICWCC.getVal(), creacionDocumentoRq.getComentarios()));

			
			// 2. Convertir base64 a File
			File archivo = new File(rutaTemporal + "\\" + creacionDocumentoRq.getTituloDocumento());

			FileUtils.writeByteArrayToFile(archivo, creacionDocumentoRq.getDocumentobase64());

			// 3. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();
			
			listaMetadata.add(
					new MetadataClaveValor("xAutorDoc", currentPrincipalName));
			
			// 4. Versionar documento a Oracle WebCenter Content por la API RIDC
			ridcAdapter.versionarDocumentoConMetadata(creacionDocumentoRq.getTituloDocumento(),
					creacionDocumentoRq.getTituloDocumento(), archivo, idExistente, dDocName, listaMetadata,currentPrincipalName);

			return true;

		} finally {
			// Borrar documentos en la ruta temporal si existen
			utilWorkflowGeneral.borrarDocumentosTemporales(rutaTemporal);
		}
	}

	/**
	 * Metodo que envia la solicitud para la obsoletizacion de un documento
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws IOException
	 */
	public boolean tipoSolicitudObsoletizacionDoucmento(DocumentoRq creacionDocumentoRq) throws IOException {
		return utilWorkflowReactivacion.tipoSolicitudReactivacionDocumento(creacionDocumentoRq);
	}

	/**
	 * Metodo que valida e inicia la solicitud de obsoletizacion de un documento
	 * existente en Oracle WCC
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws Exception 
	 */
	public boolean validarSolicitudObsoletizacion(DocumentoRq creacionDocumentoRq) throws Exception {
		// 1. Busca documento actual
		String queryBuscarDocumentoPorNombre = "(xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> xProceso <matches> `04` )" // No
																											// obsoletizacion
				+ "<AND> xNombreDocumento <substring> `" + creacionDocumentoRq.getNombreDocumento() + "`";

		List<DataObject> resultado = ridcAdapter.buscar(queryBuscarDocumentoPorNombre, "SearchResults");

		if (!resultado.isEmpty()) {
			logger.info("DOCUMENTO ENCONTRADO dDocTitle -> " + creacionDocumentoRq.getNombreDocumento());
			logger.info("dID: " + resultado.get(0).get("dID"));
			logger.info("dDocName: " + resultado.get(0).get("dDocName"));

			String idExistente = resultado.get(0).get("dID");
			String dDocName = resultado.get(0).get("dDocName");

			// 2. Realizar checkout del documento encontrado
			ridcAdapter.checkout(idExistente);

			// 3. Realizar el checkin de la nueva version
			return tipoSolicitudVersionamientoObsoletizacionDoucmento(creacionDocumentoRq, idExistente,
					dDocName) == null ? false : true;

		} else {

			logger.info("DOCUMENTO NO ENCONTRADO dDocTitle -> " + creacionDocumentoRq.getNombreDocumento());

			if(env.getProperty("validate.wf.existencia.documento").equalsIgnoreCase("true")) {
				throw new ValidacionException("EL DOCUMENTO CON TITULO '"+creacionDocumentoRq.getNombreDocumento()+"' NO EXISTE EN EL SISTEMA.");
			}else {
				return tipoSolicitudObsoletizacionDoucmento(creacionDocumentoRq);
			}
		}
	}

}
