/**
 * 
 */
package co.dpworld.pmsapp.util.wf;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.exception.ValidacionException;
import co.dpworld.pmsapp.model.DocumentoRq;
import oracle.stellent.ridc.model.DataObject;

/**
 * Servicio de utilidades para flujo de actualizacion
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@Service
public class UtilWorkflowActualizacion {

	Logger logger = LoggerFactory.getLogger(UtilWorkflowCambioNomenclatura.class);

	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private UtilWorkflowCreacion utilWorkflowCreacion;

	@Autowired
	private UtilWorkflowReactivacion utilWorkflowReactivacion;
	
	@Autowired
	private Environment env;
	

	/**
	 * Metodo que envia la solicitud para la actualizacion de un documento
	 * 
	 * @author Sergio Arias
	 * @date 19/12/2019
	 * @param creacionDocumentoRq
	 * @return
	 * @throws IOException
	 */
	public boolean tipoSolicitudActualizacionDoucmento(DocumentoRq creacionDocumentoRq) throws IOException {
		return utilWorkflowReactivacion.tipoSolicitudReactivacionDocumento(creacionDocumentoRq);
	}

	/**
	 * Metodo que valida e inicia la solicitud de actualizacion de un documento
	 * existente en Oracle WCC
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param creacionDocumentoRq
	 * @return
	 * @throws Exception 
	 */
	public boolean validarSolicitudActualizacion(DocumentoRq creacionDocumentoRq) throws Exception {
		// 1. Busca documento actual
		String queryBuscarDocumentoPorNombre = "(xProceso <matches> `05` <OR> xProceso <matches> `01` <OR> xProceso <matches> `02` <OR> xProceso <matches> `04` )" // No
																											// obsoletizacion

				+ "<AND> xNombreDocumento <substring> `" + creacionDocumentoRq.getNombreDocumento() + "`";
		List<DataObject> resultado = ridcAdapter.buscar(queryBuscarDocumentoPorNombre, "SearchResults");

		if (!resultado.isEmpty()) {
			logger.info("DOCUMENTO ENCONTRADO dDocTitle -> " + creacionDocumentoRq.getNombreDocumento());
			logger.info("dID: " + resultado.get(0).get("dID"));
			logger.info("dDocName: " + resultado.get(0).get("dDocName"));

			String idExistente = resultado.get(0).get("dID");
			String dDocName = resultado.get(0).get("dDocName");

			// 2. Realizar checkout del documento encontrado
			ridcAdapter.checkout(idExistente);

			// 3. Realizar el checkin de la nueva version
			return utilWorkflowCreacion.tipoSolicitudVersionamientoDocumento(creacionDocumentoRq, idExistente,
					dDocName) == null ? false : true;

		} else {

			logger.info("DOCUMENTO NO ENCONTRADO dDocTitle -> " + creacionDocumentoRq.getNombreDocumento());

			if(env.getProperty("validate.wf.existencia.documento").equalsIgnoreCase("true")) {
				throw new ValidacionException("EL DOCUMENTO CON TITULO '"+creacionDocumentoRq.getNombreDocumento()+"' NO EXISTE EN EL SISTEMA.");
			}else {
				return tipoSolicitudActualizacionDoucmento(creacionDocumentoRq);
			}
		}
	}

}
