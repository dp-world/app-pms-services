/**
 * 
 */
package co.dpworld.pmsapp.util.wf;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.service.IEmailService;

/**
 * @author Sergio Arias
 * @date 11/04/2020
 *
 */
@Service
public class UtilEmail {

	@Autowired
	private IEmailService emailService;

	
	Logger logger = LoggerFactory.getLogger(UtilEmail.class);
	
	
	/**
	 * Metodo que envia correo al solicitante de solicitud recibida
	 * @author Sergio Arias
	 * @throws MessagingException 
	 * @date 11/04/2020
	 */
	@Async("threadPoolTaskExecutor")
	public void enviarCorreoSolicitante(String[]listEmailTo, String subject,String messageHtml) throws MessagingException {

		
		logger.info("ENVIANDO CORREO ELECTRONICO | SOLICITANTE | : to: {} , subject: {} , message: {}",listEmailTo,subject,messageHtml);
		
		emailService.enviarEmail(listEmailTo, subject, messageHtml);
		
	}

}
