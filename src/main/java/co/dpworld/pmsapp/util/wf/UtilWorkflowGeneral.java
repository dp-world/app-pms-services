/**
 * 
 */
package co.dpworld.pmsapp.util.wf;

import static co.dpworld.pmsapp.util.EMetadataOracleWCC.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.dpworld.pmsapp.apiwcc.RidcAdapter;
import co.dpworld.pmsapp.exception.ValidacionException;
import co.dpworld.pmsapp.model.ActualizarPasoRq;
import co.dpworld.pmsapp.model.AnexoRq;
import co.dpworld.pmsapp.model.InformacionUsuarioRp;
import co.dpworld.pmsapp.model.MetadataClaveValor;
import co.dpworld.pmsapp.repo.PNomenclaturaDepartamentoCategoria;
import co.dpworld.pmsapp.repo.PSecuencia;
import co.dpworld.pmsapp.repo.i.IPSecuenciaRepo;
import co.dpworld.pmsapp.service.IUsuarioService;
import co.dpworld.pmsapp.util.UtilRidcWcc;

/**
 * Servicio de utilidades a nivel general y transversal para cualquier flujo
 * 
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
@Service
public class UtilWorkflowGeneral {

	Logger logger = LoggerFactory.getLogger(UtilWorkflowGeneral.class);

	@Autowired
	private RidcAdapter ridcAdapter;

	@Autowired
	private Environment env;

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private IPSecuenciaRepo secuenciaRepo;
	
	@Autowired
	private UtilRidcWcc utilRidcWcc;

	/**
	 * Metodo que borra todos los documentos en la ruta temporal del file system
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param rutaTemporal
	 */
	public void borrarDocumentosTemporales(String rutaTemporal) {
		File carpeta = new File(rutaTemporal);
		if (carpeta != null & carpeta.isDirectory()) {
			for (final File f : carpeta.listFiles()) {
				if (f.isFile()) {
					try {
						f.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		}
	}

	/**
	 * Metodo que valida si es necesario reemplazar el documento de un workflow
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param actualizarPasoRq
	 * @throws IOException
	 * @throws ValidacionException 
	 */
	public void reemplazarDocumento(ActualizarPasoRq actualizarPasoRq, String idActual, String dDocName)
			throws IOException, ValidacionException {
		// CHECKOUT
		ridcAdapter.checkout(idActual);

		// Ruta temporal para escribir los archivos en el file system
		String rutaTemporal = env.getProperty("wcc.ruta.temporal");

		// 2. Convertir base64 a File
		File archivo = new File(rutaTemporal + "\\" + actualizarPasoRq.getTituloDocumento());

		FileUtils.writeByteArrayToFile(archivo, actualizarPasoRq.getDocumentobase64());

		// CHECKIN REPLACE (En el flujo se configura para que remplace y no versione)
		ridcAdapter.versionarDocumento(actualizarPasoRq.getTituloDocumento(), actualizarPasoRq.getTituloDocumento(),
				archivo, idActual, dDocName);
	}

	/**
	 * Metodo que valida los id de paso en Oracle WCC para determinar la metadata
	 * automatica
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param idPaso                  Id de paso del flujo actual
	 * @param listaMetadataClaveValor Lista de la metadata del flujo asociado
	 * @param actualizarPasoRq 
	 * @throws Exception 
	 */
	public void revisionIdPasoAsignacionMetadataAutomatica(String idPaso,
			List<MetadataClaveValor> listaMetadataClaveValor, ActualizarPasoRq actualizarPasoRq) throws Exception {

		// Obtener fecha actual
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String strDate = formatter.format(date);

		// Obtener nombre del aprobador
		InformacionUsuarioRp informacionUsuario = usuarioService.obtenerInfoUsuario();

		// Se valida el paso para guardar informacion autoamtica del aprobador y fecha
		// de aprobacion creacion del documento
		// 205 director del area
		if (idPaso != null && idPaso.equalsIgnoreCase("205")) {

			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_APROBACION_CREACION.getVal(), strDate));
			listaMetadataClaveValor.add(new MetadataClaveValor(NOMBRE_APROBACION_CREACION.getVal(),
					informacionUsuario.getNombreCompleto()));
		}

		// Se valida el paso para asignar automaticamente fecha de revision y fecha de
		// vencimiento basado en la fecha revision
		// 403 revision final WF creacion
		// 608 ajustes finales WF modificacion
		// 1011 confirmar reactivacion
		// 1403 revision final nomenclatura
		if ((idPaso != null) && (idPaso.equalsIgnoreCase("403") || idPaso.equalsIgnoreCase("608")
				|| idPaso.equalsIgnoreCase("1011") || idPaso.equalsIgnoreCase("1403"))) {

			// Obtener la frecuencia de revision
			String frecuenciaRevision = "";
			for (MetadataClaveValor metadata : listaMetadataClaveValor) {
				if (metadata.getClave().equalsIgnoreCase(FRECUENCIA_REVISION_SIMBOLICWCC.getVal())) {
					frecuenciaRevision = metadata.getValor();
					break;
				}
			}
			// Si existe frecuencia revision se calcula fecha vencimiento y fecha proxima
			// revision
			if (!frecuenciaRevision.isEmpty()) {
				String fechaVencimientoRevision = calcularFechasVencimientoYRevision(frecuenciaRevision, formatter);
				listaMetadataClaveValor
						.add(new MetadataClaveValor(FECHA_VENCIMIENTO_SIMBOLICWCC.getVal(), fechaVencimientoRevision));
				listaMetadataClaveValor.add(
						new MetadataClaveValor(FECHA_PROXIMA_REVISION_SIMBOLICWCC.getVal(), fechaVencimientoRevision));
			}
		}

		// Se valida el paso para guardar la informacion automatica del aprobador y
		// fecha de aprobacion obsoletizacion
		// 805 verificar documento director de area
		if (idPaso != null && idPaso.equalsIgnoreCase("805")) {

			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_APROBACION_OBSOLETIZACION.getVal(), strDate));
			listaMetadataClaveValor.add(new MetadataClaveValor(NOMBRE_APROBACION_OBSOLETIZACION.getVal(),
					informacionUsuario.getNombreCompleto()));
		}

		// Se valida el paso para guardar la informacion automatica del aprobador y
		// fecha de aprobacion cambio de nomenclatura
		// 1402 Aprobacion director de area
		if (idPaso != null && idPaso.equalsIgnoreCase("1402")) {

			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_APROBACION_NOMENCLATURA.getVal(), strDate));
			listaMetadataClaveValor.add(new MetadataClaveValor(NOMBRE_APROBACION_NOMENCLATURA.getVal(),
					informacionUsuario.getNombreCompleto()));
		}
		
		// Se validar el paso final de nomenclatura para asignar nuevo departamento y tipo documemnto seleccionado como propuesta
		if (idPaso != null && idPaso.equalsIgnoreCase("1403")) {

			listaMetadataClaveValor.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(), actualizarPasoRq.getAreaPropuesta()));
			listaMetadataClaveValor.add(new MetadataClaveValor(TIPODOCUMENTO_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getCambioTipoDocumentoA()));
		}


		// Se valida el paso para guardar la informacion automatica del aprobador y
		// fecha de aprobacion cambio de reactivacion
		// 1009 Aprobacion director del area
		if (idPaso != null && idPaso.equalsIgnoreCase("1009")) {

			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_APROBACION_REACTIVACION.getVal(), strDate));
			listaMetadataClaveValor.add(new MetadataClaveValor(NOMBRE_APROBACION_REACTIVACION.getVal(),
					informacionUsuario.getNombreCompleto()));
		}

		// Se valida el paso para guardar la informacion automatica del aprobador y
		// fecha de aprobacion cambio de actualizacion
		// 608 Revisió final
		if (idPaso != null && idPaso.equalsIgnoreCase("608")) {

			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_APROBACION_ACTUALIZACION.getVal(), strDate));
			listaMetadataClaveValor.add(new MetadataClaveValor(NOMBRE_APROBACION_ACTUALIZACION.getVal(),
					informacionUsuario.getNombreCompleto()));
		}

		// ASIGNAR SECUENCIAS # DE CAMBIO
		asignarSecuenciaDocumentoPasosFinales(idPaso, listaMetadataClaveValor);

		// ASGINACION DE CARPETAS
		//asignarCarpetasDocumentoPasosFinales(idPaso, listaMetadataClaveValor, actualizarPasoRq);

	}

	/**
	 * Metodo que obtiene la carpeta segun metadata (Clasificacion/TipoDoc/Departamento), si no existe la crea y la asigna a la metadata del documento 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param idPaso
	 * @param listaMetadataClaveValor
	 * @param actualizarPasoRq
	 * @throws Exception 
	 */
	private void asignarCarpetasDocumentoPasosFinales(String idPaso, List<MetadataClaveValor> listaMetadataClaveValor,
			ActualizarPasoRq actualizarPasoRq) throws Exception {

		
		String clasificacion = actualizarPasoRq.getClasificacion();
		String tipoDoc = (actualizarPasoRq.getTipoDocumento() != null) ? utilRidcWcc.obtenerNombreTipologiaPorCo(actualizarPasoRq.getTipoDocumento().getCodigo()) : null;
		
		@SuppressWarnings("deprecation")
		String departamento = (actualizarPasoRq.getDepartamento() != null) ? utilRidcWcc.obtenerNombreDepartamentoPorCod(actualizarPasoRq.getDepartamento().getCodigo()):null;
		

		
		String pathCarpeta = "";
		
		
		if(clasificacion != null && tipoDoc != null && departamento != null) {
			
			// Estructura PATH -> Clasificacion/TipoDoc/Departamento
			pathCarpeta += clasificacion+"/"+tipoDoc+"/"+departamento;
			
			// Se valida paso final de todos los flujos
			if (idPaso != null && (idPaso.equalsIgnoreCase("403") || idPaso.equalsIgnoreCase("608") || idPaso.equalsIgnoreCase("807") 
					|| idPaso.equalsIgnoreCase("1011") || idPaso.equalsIgnoreCase("1403"))) {

				List<String> listaCarpetas = Arrays.asList(pathCarpeta.split("/"));
				String cadenaCarpeta = "/Enterprise Libraries";
				int contadorCarpetas = 0;

				String id = "";

				for(String nombreCarpeta : listaCarpetas) {
					
					cadenaCarpeta += "/"+nombreCarpeta;
					
					// Obtener id de la carpeta por path
					if(ridcAdapter.obtenerIdCarpetaPorPath("", cadenaCarpeta) != null)
					{
						id = ridcAdapter.obtenerIdCarpetaPorPath("", cadenaCarpeta); 
					}
					
					logger.info("id -> "+id);
					
					if(ridcAdapter.obtenerIdCarpetaPorPath("", cadenaCarpeta) == null)
					{
						// Crear y asignar carpeta
						ridcAdapter.crearCarpetaPorPath("", nombreCarpeta, (contadorCarpetas == 0) ? "FLD_ENTERPRISE_LIBRARY": id);
						id = ridcAdapter.obtenerIdCarpetaPorPath("", nombreCarpeta);
					}
					contadorCarpetas++;
				}
				
				// Asignar carpeta
				listaMetadataClaveValor.add(new MetadataClaveValor("fParentPath","/Enterprise Libraries/"+pathCarpeta));
				
			}
		}else {
			logger.info("No se puede armar el path porque existen datos nulos.  Clasificacion {} , TipoDoc {}, Departamento  {}",clasificacion,tipoDoc,departamento);
		}
		


	}

	/**
	 * Metodo que setea la secuencia de documento si es el paso final del flujo
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param idPaso
	 * @param listaMetadataClaveValor
	 */
	private void asignarSecuenciaDocumentoPasosFinales(String idPaso,
			List<MetadataClaveValor> listaMetadataClaveValor) {

		SimpleDateFormat formatoAnio = new SimpleDateFormat("yyyy");
		Date fechaActual = new Date();

		// Se valida paso final CREACION
		if (idPaso != null && idPaso.equalsIgnoreCase("403")) {

			PSecuencia parametricaSecuencia = secuenciaRepo.findOne("DOCS");
			parametricaSecuencia.setSecuencia_doc(parametricaSecuencia.getSecuencia_doc() + 1);
			secuenciaRepo.save(parametricaSecuencia);
			

			// YYYY-####
			String numeroSecuencia = String.valueOf(parametricaSecuencia.getSecuencia_doc());
			String secuenciaFormato = ((numeroSecuencia.length() <= 4)
					? ("0000" + numeroSecuencia).substring(numeroSecuencia.length())
					: numeroSecuencia);

			listaMetadataClaveValor.add(
					new MetadataClaveValor("xsecuenciaDoc", formatoAnio.format(fechaActual) + "-" + secuenciaFormato));
		}

		// Se valida paso final MODIFICACION
		if (idPaso != null && idPaso.equalsIgnoreCase("608")) {

			PSecuencia parametricaSecuencia = secuenciaRepo.findOne("DOCS");
			parametricaSecuencia.setSecuencia_doc(parametricaSecuencia.getSecuencia_doc() + 1);
			secuenciaRepo.save(parametricaSecuencia);

			// YYYY-####
			String numeroSecuencia = String.valueOf(parametricaSecuencia.getSecuencia_doc());
			String secuenciaFormato = ((numeroSecuencia.length() <= 4)
					? ("0000" + numeroSecuencia).substring(numeroSecuencia.length())
					: numeroSecuencia);

			listaMetadataClaveValor.add(
					new MetadataClaveValor("xsecuenciaDoc", formatoAnio.format(fechaActual) + "-" + secuenciaFormato));
		}

		// Se valida paso final OBSOLETIZACION
		if (idPaso != null && idPaso.equalsIgnoreCase("807")) {

			PSecuencia parametricaSecuencia = secuenciaRepo.findOne("DOCS");
			parametricaSecuencia.setSecuencia_doc(parametricaSecuencia.getSecuencia_doc() + 1);
			secuenciaRepo.save(parametricaSecuencia);

			// YYYY-####
			String numeroSecuencia = String.valueOf(parametricaSecuencia.getSecuencia_doc());
			String secuenciaFormato = ((numeroSecuencia.length() <= 4)
					? ("0000" + numeroSecuencia).substring(numeroSecuencia.length())
					: numeroSecuencia);

			listaMetadataClaveValor.add(
					new MetadataClaveValor("xsecuenciaDoc", formatoAnio.format(fechaActual) + "-" + secuenciaFormato));
		}

		// Se valida paso final REACTIVACION
		if (idPaso != null && idPaso.equalsIgnoreCase("1011")) {

			PSecuencia parametricaSecuencia = secuenciaRepo.findOne("DOCS");
			parametricaSecuencia.setSecuencia_doc(parametricaSecuencia.getSecuencia_doc() + 1);
			secuenciaRepo.save(parametricaSecuencia);

			// YYYY-####
			String numeroSecuencia = String.valueOf(parametricaSecuencia.getSecuencia_doc());
			String secuenciaFormato = ((numeroSecuencia.length() <= 4)
					? ("0000" + numeroSecuencia).substring(numeroSecuencia.length())
					: numeroSecuencia);

			listaMetadataClaveValor.add(
					new MetadataClaveValor("xsecuenciaDoc", formatoAnio.format(fechaActual) + "-" + secuenciaFormato));
		}

		// Se valida paso final NOMENCLATURA
		if (idPaso != null && idPaso.equalsIgnoreCase("1403")) {

			PSecuencia parametricaSecuencia = secuenciaRepo.findOne("DOCS");
			parametricaSecuencia.setSecuencia_doc(parametricaSecuencia.getSecuencia_doc() + 1);
			secuenciaRepo.save(parametricaSecuencia);

			// YYYY-####
			String numeroSecuencia = String.valueOf(parametricaSecuencia.getSecuencia_doc());
			String secuenciaFormato = ((numeroSecuencia.length() <= 4)
					? ("0000" + numeroSecuencia).substring(numeroSecuencia.length())
					: numeroSecuencia);

			listaMetadataClaveValor.add(
					new MetadataClaveValor("xsecuenciaDoc", formatoAnio.format(fechaActual) + "-" + secuenciaFormato));
		}

	}

	/**
	 * Metodo que calcula la fecha de vencimiento y fecha revision segun el campo
	 * frecuencia revision
	 * 
	 * @author Sergio Arias
	 * @date 11/04/2020
	 * @param frecuenciaRevision Valor de frecuencia de revision de los documentos
	 *                           que contiene (2 años, 3 años, semestral)
	 * @param formatter          Formato de fecha pra calcular la fecha
	 * @return Fecha de vencimiento y revision que aplica para el documento
	 */
	private String calcularFechasVencimientoYRevision(String frecuenciaRevision, SimpleDateFormat formatter) {

		Date fechaActual = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaActual);

		switch (frecuenciaRevision) {
		case "Cada 2 Años":
			calendar.add(Calendar.YEAR, 2);
			break;

		case "Cada 3 Años":
			calendar.add(Calendar.YEAR, 3);
			break;

		case "Semestral":
			calendar.add(Calendar.MONTH, 6);
			break;

		default:
			logger.info(
					"La frecuencia revision: '{}' no se encuentra mapeada para calcular fecha vencimiento y fecha de revision del documento",
					frecuenciaRevision);
			break;
		}

		return formatter.format(calendar.getTime());
	}

	/**
	 * Metodo que valida si es necesario reemplazar el archivo actual del workflow
	 * en Oracle WCC
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param actualizarPasoRq
	 * @param id
	 * @param dDocName
	 * @throws IOException
	 * @throws ValidacionException 
	 */
	public void validacionRemplazoDocumento(ActualizarPasoRq actualizarPasoRq, String id, String dDocName)
			throws IOException, ValidacionException {
		// Validar si requiere remplazar el documento (replace)
		if (actualizarPasoRq.getDocumentobase64() != null && actualizarPasoRq.getTituloDocumento() != null) {
			reemplazarDocumento(actualizarPasoRq, id, dDocName);
		}
	}

	/**
	 * Metodo que realiza el mapeo de datos recibidos al servicio a los metadatos
	 * esperados en el workflow de Oracle WCC
	 * 
	 * @author Sergio Arias
	 * @date 12/03/2020
	 * @param actualizarPasoRq Objeto que contiene los datos a actualizar en el
	 *                         worklow existente
	 * @return Lista de metadatos clave valor de Oracle WCC
	 */
	public List<MetadataClaveValor> mapeoMetadataActualizacion(ActualizarPasoRq actualizarPasoRq) {
		List<MetadataClaveValor> listaMetadataClaveValor = new ArrayList<>();
		listaMetadataClaveValor
				.add(new MetadataClaveValor(SOLICITANTE_SIMBOLICWCC.getVal(), actualizarPasoRq.getSolicitante()));
		listaMetadataClaveValor
				.add(new MetadataClaveValor(TIPOCAMBIO_SIMBOLICWCC.getVal(), actualizarPasoRq.getTipoCambio()));

		if (actualizarPasoRq.getDepartamento() != null) {
			listaMetadataClaveValor.add(new MetadataClaveValor(DEPARTAMENTO_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getDepartamento().getCodigo()));
		}

		if (actualizarPasoRq.getRazonFuenteDeCreacion() != null) {
			listaMetadataClaveValor.add(new MetadataClaveValor(RAZONCREACION_SIMBOLICWCC.getVal(),
					String.valueOf(actualizarPasoRq.getRazonFuenteDeCreacion().getCodigo())));
		}

		if (actualizarPasoRq.getTipoDocumento() != null) {
			listaMetadataClaveValor.add(new MetadataClaveValor(TIPODOCUMENTO_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getTipoDocumento().getCodigo()));
		}

		listaMetadataClaveValor.add(new MetadataClaveValor(REQUIEREADIESTRAMIENTO_SIMBOLICWCC.getVal(),
				(actualizarPasoRq.isRequiereAdiestramiento() ? "Si" : "No")));
		listaMetadataClaveValor.add(new MetadataClaveValor(REQUIERESOPORTE_SIMBOLICWCC.getVal(),
				(actualizarPasoRq.isRequiereSoporte() ? "1" : "0")));

		// DATOS DEL DOCUMENTO - REVISION FINAL
		if (actualizarPasoRq.getNombreDocumento() != null && !actualizarPasoRq.getNombreDocumento().isEmpty()) {
			listaMetadataClaveValor.add(new MetadataClaveValor(NOMBRE_DOCUMENTO_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getNombreDocumento()));
			listaMetadataClaveValor.add(new MetadataClaveValor(TITULO_DOCUMENTO_SUMBOLICWCC.getVal(),
					actualizarPasoRq.getNombreDocumento()));
		}

		if (actualizarPasoRq.getTiempoRetencion() != null && !actualizarPasoRq.getTiempoRetencion().isEmpty()) {
			listaMetadataClaveValor.add(new MetadataClaveValor(TIEMPO_RETENCION_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getTiempoRetencion()));
		}

		if (actualizarPasoRq.getMedioAlmacenamiento() != null && !actualizarPasoRq.getMedioAlmacenamiento().isEmpty()) {
			listaMetadataClaveValor.add(new MetadataClaveValor(MEDIO_ALMACENAMIENTO_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getMedioAlmacenamiento()));
		}

		if (actualizarPasoRq.getRevision() != null && !actualizarPasoRq.getRevision().isEmpty()) {
			listaMetadataClaveValor
					.add(new MetadataClaveValor(REVISION_SIMBOLICWCC.getVal(), actualizarPasoRq.getRevision()));
		}

		if (actualizarPasoRq.getResponsable() != null && !actualizarPasoRq.getResponsable().isEmpty()) {
			listaMetadataClaveValor
					.add(new MetadataClaveValor(RESPONSABLE_SIMBOLICWCC.getVal(), actualizarPasoRq.getResponsable()));
		}

		if (actualizarPasoRq.getClasificacion() != null && !actualizarPasoRq.getClasificacion().isEmpty()) {
			listaMetadataClaveValor.add(
					new MetadataClaveValor(CLASIFICACION_SIMBOLICWCC.getVal(), actualizarPasoRq.getClasificacion()));
		}

		if (actualizarPasoRq.getFechaProximaRevision() != null
				&& !actualizarPasoRq.getFechaProximaRevision().isEmpty()) {
			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_PROXIMA_REVISION_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getFechaProximaRevision()));
		}
		if (actualizarPasoRq.getLiderProceso() != null && !actualizarPasoRq.getLiderProceso().isEmpty()) {
			listaMetadataClaveValor.add(
					new MetadataClaveValor(LIDER_PROCESO_SIMBOLICWCC.getVal(), actualizarPasoRq.getLiderProceso()));
		}
		if (actualizarPasoRq.getFrecuenciaRevision() != null && !actualizarPasoRq.getFrecuenciaRevision().isEmpty()) {
			listaMetadataClaveValor.add(new MetadataClaveValor(FRECUENCIA_REVISION_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getFrecuenciaRevision()));
		}

		if (actualizarPasoRq.getFechaVencimiento() != null && !actualizarPasoRq.getFechaVencimiento().isEmpty()) {
			listaMetadataClaveValor.add(new MetadataClaveValor(FECHA_VENCIMIENTO_SIMBOLICWCC.getVal(),
					actualizarPasoRq.getFechaVencimiento()));
		}

		if (actualizarPasoRq.getJustificacion() != null) {
			listaMetadataClaveValor.add(
					new MetadataClaveValor(JUSTIFICACION_SIMBOLICWCC.getVal(), actualizarPasoRq.getJustificacion()));
		}

		// Datos nomenclatura
		listaMetadataClaveValor
				.add(new MetadataClaveValor(AREA_ACTUAL_SIMBOLICWCC.getVal(), actualizarPasoRq.getAreaActual()));
		listaMetadataClaveValor
				.add(new MetadataClaveValor(AREA_PROPUESTA_SIMBOLICWCC.getVal(), actualizarPasoRq.getAreaPropuesta()));
		listaMetadataClaveValor.add(new MetadataClaveValor(CAMBIO_TIPO_DOCUMENTO_DE_SIMBOLICWCC.getVal(),
				actualizarPasoRq.getCambioTipoDocumentoDe()));
		listaMetadataClaveValor.add(new MetadataClaveValor(CAMBIO_TIPO_DOCUMENTO_A_SIMBOLICWCC.getVal(),
				actualizarPasoRq.getCambioTipoDocumentoA()));
		listaMetadataClaveValor
				.add(new MetadataClaveValor(RAZON_CAMBIO_SIMBOLICWCC.getVal(), actualizarPasoRq.getRazonCambio()));


		listaMetadataClaveValor.add(new MetadataClaveValor(COMENTARIOS_SIMBOLICWCC.getVal(), actualizarPasoRq.getComentarios()));
		

		return listaMetadataClaveValor;
	}

	public boolean anexarDocumento(AnexoRq anexoRq) throws IOException {

		String rutaTemporal = env.getProperty("wcc.ruta.temporal");
		try {
			// 1. Obtener el username del JWT
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String currentPrincipalName = authentication.getName();

			// 2. Convertir base64 a File

			File archivo = new File(rutaTemporal + "\\" + anexoRq.getTituloDocumento());
			FileUtils.writeByteArrayToFile(archivo, anexoRq.getDocumentoAnexo());

			// 2. Registrar el documento anexo en Oracle WCC
			String dIDDestino = ridcAdapter.subirDocumento2(new ArrayList<>(), anexoRq.getTituloDocumento(),
					anexoRq.getTituloDocumento(), "Documento_DP-World", "Processes", "ProfileSupport", archivo,
					currentPrincipalName);

			// 3. Anexar documento registrado al documento principal
			return ridcAdapter.anexarDocumento(anexoRq.getIdDocumentoPrincipal(), dIDDestino);

		} finally {
			// Borrar documentos
			borrarDocumentosTemporales(rutaTemporal);
		}
	}

}
