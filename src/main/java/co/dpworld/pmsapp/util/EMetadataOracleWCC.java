/**
 * 
 */
package co.dpworld.pmsapp.util;

/**
 * Enum que contiene el mapeo de metadata Oracle WCC
 * @author Sergio Arias
 * @date 14/03/2020
 *
 */
public enum EMetadataOracleWCC {

	/** Metadatos Oracle WCC usados en el proceso de workflows */
	DEPARTAMENTO_SIMBOLICWCC ( "xDepartamento"),
	RAZONCREACION_SIMBOLICWCC ( "xrazonCreacion"),
	REQUIEREADIESTRAMIENTO_SIMBOLICWCC ( "xRequiereAdiestramiento"),
	REQUIERESOPORTE_SIMBOLICWCC ( "xRequiereSoporte"),
	SOLICITANTE_SIMBOLICWCC ( "xSolicitante"),
	TIPOCAMBIO_SIMBOLICWCC ( "xtipoCambio"),
	TIPODOCUMENTO_SIMBOLICWCC ( "xtipoDocumento"),
	TIPOPROCESO_SIMBOLICWCC ( "xProceso"),
	NOMBRE_DOCUMENTO_SIMBOLICWCC ( "xNombreDocumento"),
	NOMENCLATURA_SIMBOLICWCC ( "xNomenclatura"),
	TIEMPO_RETENCION_SIMBOLICWCC ( "xTiempoRetencion"),
	MEDIO_ALMACENAMIENTO_SIMBOLICWCC ( "xMedioAlmacenamiento"),
	REVISION_SIMBOLICWCC ( "xRevision"),
	RESPONSABLE_SIMBOLICWCC ( "xResponsable"),
	CLASIFICACION_SIMBOLICWCC ( "xClasificacion"),
	FECHA_PROXIMA_REVISION_SIMBOLICWCC ( "xFechaProximaRevision"),
	LIDER_PROCESO_SIMBOLICWCC ( "xLiderProceso"),
	FRECUENCIA_REVISION_SIMBOLICWCC ( "xFrecuenciaRevision"),
	FECHA_APROBACION_CREACION ( "xFechaAprobacionCreacion"),
	NOMBRE_APROBACION_CREACION ( "xNombreAprobadorCreacion"),
	FECHA_APROBACION_OBSOLETIZACION ( "xFechaAprobObsoletizacion"),
	NOMBRE_APROBACION_OBSOLETIZACION ( "xNombreAprobObsoletizacion"),
	FECHA_APROBACION_NOMENCLATURA ( "xFechaAprobNomenclatura"),
	NOMBRE_APROBACION_NOMENCLATURA ( "xNombreAprobNomenclatura"),
	FECHA_APROBACION_REACTIVACION ( "xFechaAprobReactivacion"),
	NOMBRE_APROBACION_REACTIVACION ( "xNombreAprobReactivacion"),
	FECHA_APROBACION_ACTUALIZACION ( "xFechaAprobActualizacion"),
	NOMBRE_APROBACION_ACTUALIZACION ( "xNombreAprobActualizacion"),
	FECHA_VENCIMIENTO_SIMBOLICWCC ( "dOutDate"),
	NOMBREDOCUMENTO_SIMBOLICWCC ( "dOriginalName"),
	RAZON_CAMBIO_SIMBOLICWCC("xRazonCambio"),
	AREA_ACTUAL_SIMBOLICWCC("xAreaActual"),
	AREA_PROPUESTA_SIMBOLICWCC("xAreaPropuesta"),
	CAMBIO_TIPO_DOCUMENTO_DE_SIMBOLICWCC("xCambioTipoDocumentoDe"),
	CAMBIO_TIPO_DOCUMENTO_A_SIMBOLICWCC("xCambioTipoDocumentoA"),
	JUSTIFICACION_SIMBOLICWCC("xJustificacion"),
	COMENTARIOS_SIMBOLICWCC("xComments"),
	TITULO_DOCUMENTO_SUMBOLICWCC("dDocTitle");
	
	private String propiedad;
	
	/**
	 * Constructor del enum para metadata
	 * @author Sergio Arias
	 * @date 14/03/2020
	 */
	private EMetadataOracleWCC(String valueMetadata) {
		this.propiedad = valueMetadata;
	}

	/**
	 * Metodo get para propiedad
	 * @author Sergio Arias
	 * @date 14/03/2020
	 * @return Retorna propiedad
	 */
	public String getVal() {
		return propiedad;
	}

	

}
