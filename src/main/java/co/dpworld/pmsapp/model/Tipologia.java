/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que contiene la informacion de una tipologia documental
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
@Validated
public class Tipologia {

	/* Codigo de la tipologia */
	@NotEmpty(message = "El campo codigo (tipologia) no puede ser vacio ")
	@NotNull(message = "El campo codigo (tipologia) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo de la tipologia", allowEmptyValue = false, required = true)
	private String codigo;
	/* Nombre de la tipologia */
	@NotNull(message = "El campo nombreTipologia no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Nombre de la tipologia", allowEmptyValue = false, required = true)
	private String nombreTipologia;

	/**
	 * Constructor vacio para tipologia
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 */
	public Tipologia() {

	}

	/**
	 * Constructor con campos para tipologia
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param codigo          Codigo de la tipologia
	 * @param nombreTipologia Nombre de la tipologia
	 */
	public Tipologia(String codigo, String nombreTipologia) {
		super();
		this.codigo = codigo;
		this.nombreTipologia = nombreTipologia;
	}

	/**
	 * Metodo get para codigo
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Metodo set para codigo
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Metodo get para nombreTipologia
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna nombreTipologia
	 */
	public String getNombreTipologia() {
		return nombreTipologia;
	}

	/**
	 * Metodo set para nombreTipologia
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea nombreTipologia
	 */
	public void setNombreTipologia(String nombreTipologia) {
		this.nombreTipologia = nombreTipologia;
	}

	
	
	
	

}
