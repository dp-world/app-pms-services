/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * Enumerado que tiene las acciones posibles para un flujo
 * @author Sergio Arias
 * @date 23/11/2019
 *
 */
public enum TIPO_DE_ACCION_WF {

	GUARDAR,APROBAR,RECHAZAR
}
