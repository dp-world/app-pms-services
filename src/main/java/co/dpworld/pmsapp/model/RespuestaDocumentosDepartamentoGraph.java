/**
 * 
 */
package co.dpworld.pmsapp.model;

import java.util.List;

/**
 * Clase que contiene los datos de respuesta para los documentos agrupados por
 * departamento grafica
 * 
 * @author Sergio Arias
 * @date 23/02/2020
 *
 */
public class RespuestaDocumentosDepartamentoGraph {

	private List<String> labels;
	private List<DataSet> datasets;

	/**
	 * Metodo get para labels
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna labels
	 */
	public List<String> getLabels() {
		return labels;
	}

	/**
	 * Metodo set para labels
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea labels
	 */
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	/**
	 * Metodo get para datasets
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna datasets
	 */
	public List<DataSet> getDatasets() {
		return datasets;
	}

	/**
	 * Metodo set para datasets
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea datasets
	 */
	public void setDatasets(List<DataSet> datasets) {
		this.datasets = datasets;
	}

}
