/**
 * 
 */
package co.dpworld.pmsapp.model;


import java.util.List;

/**
 * Clase que que contiene la informacion de un usuario en el sistema
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
public class Usuario {


	private int idUsuario;


	private String username;


	private String password;


	private boolean enabled;

	private List<Rol> roles;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", username=" + username + ", password=" + password + ", enabled="
				+ enabled + ", roles=" + roles + "]";
	}
	
	

}
