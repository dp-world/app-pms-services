/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergio Arias
 * @date 29/03/2020
 *
 */
@Validated
public class Clasificacion {


	@NotEmpty(message = "El campo codigo (clasificacion) no puede ser vacio ")
	@NotNull(message = "El campo codigo (clasificacion) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo clasificacion", allowEmptyValue = false, required = true)
	private String codigo;

	@NotNull(message = "El campo nombreClasificacion no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Nombre clasificacion", allowEmptyValue = false, required = true)
	private String nombreClasificacion;

	
	
	public Clasificacion(String codigo, String nombreFrecuenciaRevision) {
		super();
		this.codigo = codigo;
		this.nombreClasificacion = nombreFrecuenciaRevision;
	}



	/**
	 * Metodo get para codigo
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna codigo
	 */
	public String getCodigo() {
		return codigo;
	}



	/**
	 * Metodo set para codigo
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	/**
	 * Metodo get para nombreClasificacion
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna nombreClasificacion
	 */
	public String getNombreClasificacion() {
		return nombreClasificacion;
	}



	/**
	 * Metodo set para nombreClasificacion
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea nombreClasificacion
	 */
	public void setNombreClasificacion(String nombreClasificacion) {
		this.nombreClasificacion = nombreClasificacion;
	}

	
}
