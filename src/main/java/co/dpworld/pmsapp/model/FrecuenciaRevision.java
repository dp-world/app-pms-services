/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergio Arias
 * @date 29/03/2020
 *
 */
@Validated
public class FrecuenciaRevision {

	/* Codigo de frecuencia de revision */
	@NotEmpty(message = "El campo codigo (frecuencia de revision) no puede ser vacio ")
	@NotNull(message = "El campo codigo (frecuencia de revision) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo de frecuencia de revision", allowEmptyValue = false, required = true)
	private String codigo;

	/* Nombre de frecuencia de revision */
	@NotNull(message = "El campo nombreFrecuenciaRevision no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Nombre frecuencia de revision", allowEmptyValue = false, required = true)
	private String nombreFrecuenciaRevision;

	
	
	public FrecuenciaRevision(String codigo, String nombreFrecuenciaRevision) {
		super();
		this.codigo = codigo;
		this.nombreFrecuenciaRevision = nombreFrecuenciaRevision;
	}

	/**
	 * Metodo get para codigo
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Metodo set para codigo
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Metodo get para nombreFrecuenciaRevision
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna nombreFrecuenciaRevision
	 */
	public String getNombreFrecuenciaRevision() {
		return nombreFrecuenciaRevision;
	}

	/**
	 * Metodo set para nombreFrecuenciaRevision
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea nombreFrecuenciaRevision
	 */
	public void setNombreFrecuenciaRevision(String nombreFrecuenciaRevision) {
		this.nombreFrecuenciaRevision = nombreFrecuenciaRevision;
	}
	
	

}
