/**
 * 
 */
package co.dpworld.pmsapp.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergio Arias
 * @date 24/11/2019
 *
 */
public class ActualizarPasoRq {

	/* Nombre del solicitante de la creacion del documento */
	// @NotEmpty(message = "El campo solicitante no puede ser vacio ")
	// @NotNull(message = "El campo solicitante no puede ser nulo ")
	@ApiModelProperty(value = "Nombre del solicitante de la creacion del documento", allowEmptyValue = false, required = true)
	private String solicitante;

	/* Departamento o categoria del documento */
	// @NotNull(message = "El objeto departamento no puede ser nulo ")
	@ApiModelProperty(value = "Departamento o categoria del documento", allowEmptyValue = false, required = true)
	private Departamento departamento;

	/* Tipologia documental */
	@ApiModelProperty(value = "Tipologia documental", allowEmptyValue = false, required = true)
	private Tipologia tipoDocumento;

	/*
	 * Indica si requiere soporte o no , se debe asignar una persona para el soporte
	 */
	// @NotNull(message = "El campo requiereSoporte no puede ser nulo ")
	@ApiModelProperty(value = "Indica si requiere soporte o no , se debe asignar una persona para el soporte", allowEmptyValue = false, required = true)
	private boolean requiereSoporte;

	/* Contiene la razón y/o fuente de la creación del documento */
	// @NotNull(message = "El objeto razonFuenteDeCreacion no puede ser nulo ")
	@ApiModelProperty(value = " Contiene la razón y/o fuente de la creación del documento", allowEmptyValue = false, required = true)
	private RazonCreacion razonFuenteDeCreacion;

	/* Contiene el tipo de cambio del documento */
	// @NotEmpty(message = "El campo tipoCambio no puede ser vacio ")
	// @NotNull(message = "El campo tipoCambio no puede ser nulo ")
	@ApiModelProperty(value = "Contiene el tipo de cambio del documento", allowEmptyValue = false, required = true)
	private String tipoCambio;

	/* Indica si requiere adiestramiento el documento */
	// @NotNull(message = "El campo requiereAdiestramiento no puede ser nulo y/o
	// vacio")
	@ApiModelProperty(value = "Indica si requiere adiestramiento el documento", allowEmptyValue = false, required = true)
	private boolean requiereAdiestramiento;

	// Datos del documento
	private String nombreDocumento;
	private String tiempoRetencion;
	private String medioAlmacenamiento;
	private String revision;
	private String responsable;
	private String clasificacion;
	private String fechaProximaRevision;
	private String liderProceso;
	private String frecuenciaRevision;
	private String fechaVencimiento;

	private String justificacion;

	// ########################
	// Data nomenclatura
	// #######################
	private String areaActual;
	private String areaPropuesta;
	private String cambioTipoDocumentoDe;
	private String cambioTipoDocumentoA;
	private String razonCambio;

	/* Cadena de texto en base 64 con el documento que se esta creando */
	// @NotEmpty(message = "El campo documentobase64 no puede ser vacio ")
	// @NotNull(message = "El campo documentobase64 no puede ser nulo ")
	@ApiModelProperty(value = "Cadena de texto en base 64 con el documento que se esta creando", allowEmptyValue = false, required = true)
	private byte[] documentobase64;

	// @NotEmpty(message = "El campo tituloDocumento no puede ser vacio ")
	// @NotNull(message = "El campo tituloDocumento no puede ser nulo ")
	@ApiModelProperty(value = "Titulo del documento en base 64", allowEmptyValue = false, required = false, notes = "El titulo debe contener la extension del documento")
	private String tituloDocumento;
	
	private String comentarios;
	
	

	
	/**
	 * Metodo get para comentarios
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @return Retorna comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * Metodo set para comentarios
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param setea comentarios
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * Metodo get para fechaVencimiento
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna fechaVencimiento
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * Metodo set para fechaVencimiento
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea fechaVencimiento
	 */
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * Metodo get para areaActual
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @return Retorna areaActual
	 */
	public String getAreaActual() {
		return areaActual;
	}

	/**
	 * Metodo set para areaActual
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @param setea areaActual
	 */
	public void setAreaActual(String areaActual) {
		this.areaActual = areaActual;
	}

	/**
	 * Metodo get para areaPropuesta
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @return Retorna areaPropuesta
	 */
	public String getAreaPropuesta() {
		return areaPropuesta;
	}

	/**
	 * Metodo set para areaPropuesta
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @param setea areaPropuesta
	 */
	public void setAreaPropuesta(String areaPropuesta) {
		this.areaPropuesta = areaPropuesta;
	}

	/**
	 * Metodo get para cambioTipoDocumentoDe
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @return Retorna cambioTipoDocumentoDe
	 */
	public String getCambioTipoDocumentoDe() {
		return cambioTipoDocumentoDe;
	}

	/**
	 * Metodo set para cambioTipoDocumentoDe
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @param setea cambioTipoDocumentoDe
	 */
	public void setCambioTipoDocumentoDe(String cambioTipoDocumentoDe) {
		this.cambioTipoDocumentoDe = cambioTipoDocumentoDe;
	}

	/**
	 * Metodo get para cambioTipoDocumentoA
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @return Retorna cambioTipoDocumentoA
	 */
	public String getCambioTipoDocumentoA() {
		return cambioTipoDocumentoA;
	}

	/**
	 * Metodo set para cambioTipoDocumentoA
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @param setea cambioTipoDocumentoA
	 */
	public void setCambioTipoDocumentoA(String cambioTipoDocumentoA) {
		this.cambioTipoDocumentoA = cambioTipoDocumentoA;
	}

	/**
	 * Metodo get para razonCambio
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @return Retorna razonCambio
	 */
	public String getRazonCambio() {
		return razonCambio;
	}

	/**
	 * Metodo set para razonCambio
	 * 
	 * @author Sergio Arias
	 * @date 7/01/2020
	 * @param setea razonCambio
	 */
	public void setRazonCambio(String razonCambio) {
		this.razonCambio = razonCambio;
	}

	/**
	 * Metodo get para justificacion
	 * 
	 * @author Sergio Arias
	 * @date 4/01/2020
	 * @return Retorna justificacion
	 */
	public String getJustificacion() {
		return justificacion;
	}

	/**
	 * Metodo set para justificacion
	 * 
	 * @author Sergio Arias
	 * @date 4/01/2020
	 * @param setea justificacion
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}

	/**
	 * Metodo get para documentobase64
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @return Retorna documentobase64
	 */
	public byte[] getDocumentobase64() {
		return documentobase64;
	}

	/**
	 * Metodo set para documentobase64
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param setea documentobase64
	 */
	public void setDocumentobase64(byte[] documentobase64) {
		this.documentobase64 = documentobase64;
	}

	/**
	 * Metodo get para tituloDocumento
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @return Retorna tituloDocumento
	 */
	public String getTituloDocumento() {
		return tituloDocumento;
	}

	/**
	 * Metodo set para tituloDocumento
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param setea tituloDocumento
	 */
	public void setTituloDocumento(String tituloDocumento) {
		this.tituloDocumento = tituloDocumento;
	}

	/**
	 * Metodo get para responsable
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna responsable
	 */
	public String getResponsable() {
		return responsable;
	}

	/**
	 * Metodo set para responsable
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea responsable
	 */
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	/**
	 * Metodo get para fechaProximaRevision
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna fechaProximaRevision
	 */
	public String getFechaProximaRevision() {
		return fechaProximaRevision;
	}

	/**
	 * Metodo set para fechaProximaRevision
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea fechaProximaRevision
	 */
	public void setFechaProximaRevision(String fechaProximaRevision) {
		this.fechaProximaRevision = fechaProximaRevision;
	}

	/**
	 * Metodo get para liderProceso
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna liderProceso
	 */
	public String getLiderProceso() {
		return liderProceso;
	}

	/**
	 * Metodo set para liderProceso
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea liderProceso
	 */
	public void setLiderProceso(String liderProceso) {
		this.liderProceso = liderProceso;
	}

	/**
	 * Metodo get para frecuenciaRevision
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna frecuenciaRevision
	 */
	public String getFrecuenciaRevision() {
		return frecuenciaRevision;
	}

	/**
	 * Metodo set para frecuenciaRevision
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea frecuenciaRevision
	 */
	public void setFrecuenciaRevision(String frecuenciaRevision) {
		this.frecuenciaRevision = frecuenciaRevision;
	}

	/**
	 * Metodo get para tiempoRetencion
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna tiempoRetencion
	 */
	public String getTiempoRetencion() {
		return tiempoRetencion;
	}

	/**
	 * Metodo set para tiempoRetencion
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea tiempoRetencion
	 */
	public void setTiempoRetencion(String tiempoRetencion) {
		this.tiempoRetencion = tiempoRetencion;
	}

	/**
	 * Metodo get para medioAlmacenamiento
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna medioAlmacenamiento
	 */
	public String getMedioAlmacenamiento() {
		return medioAlmacenamiento;
	}

	/**
	 * Metodo set para medioAlmacenamiento
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea medioAlmacenamiento
	 */
	public void setMedioAlmacenamiento(String medioAlmacenamiento) {
		this.medioAlmacenamiento = medioAlmacenamiento;
	}

	/**
	 * Metodo get para revision
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna revision
	 */
	public String getRevision() {
		return revision;
	}

	/**
	 * Metodo set para revision
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea revision
	 */
	public void setRevision(String revision) {
		this.revision = revision;
	}

	/**
	 * Metodo get para clasificacion
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna clasificacion
	 */
	public String getClasificacion() {
		return clasificacion;
	}

	/**
	 * Metodo set para clasificacion
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea clasificacion
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	/**
	 * Metodo get para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 19/12/2019
	 * @return Retorna nombreDocumento
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}

	/**
	 * Metodo set para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 19/12/2019
	 * @param setea nombreDocumento
	 */
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	/**
	 * Metodo get para solicitante
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna solicitante
	 */
	public String getSolicitante() {
		return solicitante;
	}

	/**
	 * Metodo set para solicitante
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea solicitante
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna departamento
	 */
	public Departamento getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea departamento
	 */
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para tipoDocumento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna tipoDocumento
	 */
	public Tipologia getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Metodo set para tipoDocumento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea tipoDocumento
	 */
	public void setTipoDocumento(Tipologia tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Metodo get para requiereSoporte
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna requiereSoporte
	 */
	public boolean isRequiereSoporte() {
		return requiereSoporte;
	}

	/**
	 * Metodo set para requiereSoporte
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea requiereSoporte
	 */
	public void setRequiereSoporte(boolean requiereSoporte) {
		this.requiereSoporte = requiereSoporte;
	}

	/**
	 * Metodo get para razonFuenteDeCreacion
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna razonFuenteDeCreacion
	 */
	public RazonCreacion getRazonFuenteDeCreacion() {
		return razonFuenteDeCreacion;
	}

	/**
	 * Metodo set para razonFuenteDeCreacion
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea razonFuenteDeCreacion
	 */
	public void setRazonFuenteDeCreacion(RazonCreacion razonFuenteDeCreacion) {
		this.razonFuenteDeCreacion = razonFuenteDeCreacion;
	}

	/**
	 * Metodo get para tipoCambio
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna tipoCambio
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * Metodo set para tipoCambio
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea tipoCambio
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	/**
	 * Metodo get para requiereAdiestramiento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna requiereAdiestramiento
	 */
	public boolean isRequiereAdiestramiento() {
		return requiereAdiestramiento;
	}

	/**
	 * Metodo set para requiereAdiestramiento
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea requiereAdiestramiento
	 */
	public void setRequiereAdiestramiento(boolean requiereAdiestramiento) {
		this.requiereAdiestramiento = requiereAdiestramiento;
	}

}
