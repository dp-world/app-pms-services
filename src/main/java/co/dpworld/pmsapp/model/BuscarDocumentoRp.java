/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 16/01/2020
 *
 */
public class BuscarDocumentoRp {

	// campos v2
	private String id;
	private String nombre;
	private String version;
	private String solicitante;
	private String departamento;
	private String nomenclatura;
	private String fechaCreacion;

	// campos nuevos v2
	private String tipoDocumento;
	private String tipoCambio;
	private String razonCreacion;
	private String tiempoRetencion;
	private String liderProceso;
	private String frecuenciaRevision;
	private String revision;
	private String responsable;
	private String fechaProximaRevision;
	private String clasificacion;
	private String fechaVencimiento;

	private String numeroCambio;

	/**
	 * Metodo get para numeroCambio
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @return Retorna numeroCambio
	 */
	public String getNumeroCambio() {
		return numeroCambio;
	}

	/**
	 * Metodo set para numeroCambio
	 * 
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param setea numeroCambio
	 */
	public void setNumeroCambio(String numeroCambio) {
		this.numeroCambio = numeroCambio;
	}

	/**
	 * Metodo get para tipoDocumento
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Metodo set para tipoDocumento
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea tipoDocumento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Metodo get para tipoCambio
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna tipoCambio
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * Metodo set para tipoCambio
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea tipoCambio
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	/**
	 * Metodo get para razonCreacion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna razonCreacion
	 */
	public String getRazonCreacion() {
		return razonCreacion;
	}

	/**
	 * Metodo set para razonCreacion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea razonCreacion
	 */
	public void setRazonCreacion(String razonCreacion) {
		this.razonCreacion = razonCreacion;
	}

	/**
	 * Metodo get para tiempoRetencion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna tiempoRetencion
	 */
	public String getTiempoRetencion() {
		return tiempoRetencion;
	}

	/**
	 * Metodo set para tiempoRetencion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea tiempoRetencion
	 */
	public void setTiempoRetencion(String tiempoRetencion) {
		this.tiempoRetencion = tiempoRetencion;
	}

	/**
	 * Metodo get para liderProceso
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna liderProceso
	 */
	public String getLiderProceso() {
		return liderProceso;
	}

	/**
	 * Metodo set para liderProceso
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea liderProceso
	 */
	public void setLiderProceso(String liderProceso) {
		this.liderProceso = liderProceso;
	}

	/**
	 * Metodo get para frecuenciaRevision
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna frecuenciaRevision
	 */
	public String getFrecuenciaRevision() {
		return frecuenciaRevision;
	}

	/**
	 * Metodo set para frecuenciaRevision
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea frecuenciaRevision
	 */
	public void setFrecuenciaRevision(String frecuenciaRevision) {
		this.frecuenciaRevision = frecuenciaRevision;
	}

	/**
	 * Metodo get para revision
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna revision
	 */
	public String getRevision() {
		return revision;
	}

	/**
	 * Metodo set para revision
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea revision
	 */
	public void setRevision(String revision) {
		this.revision = revision;
	}

	/**
	 * Metodo get para responsable
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna responsable
	 */
	public String getResponsable() {
		return responsable;
	}

	/**
	 * Metodo set para responsable
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea responsable
	 */
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	/**
	 * Metodo get para fechaProximaRevision
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna fechaProximaRevision
	 */
	public String getFechaProximaRevision() {
		return fechaProximaRevision;
	}

	/**
	 * Metodo set para fechaProximaRevision
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea fechaProximaRevision
	 */
	public void setFechaProximaRevision(String fechaProximaRevision) {
		this.fechaProximaRevision = fechaProximaRevision;
	}

	/**
	 * Metodo get para clasificacion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna clasificacion
	 */
	public String getClasificacion() {
		return clasificacion;
	}

	/**
	 * Metodo set para clasificacion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea clasificacion
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	/**
	 * Metodo get para fechaVencimiento
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna fechaVencimiento
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * Metodo set para fechaVencimiento
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea fechaVencimiento
	 */
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * Metodo get para nombre
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Metodo get para id
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Metodo set para id
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Metodo set para nombre
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Metodo get para version
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Metodo set para version
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Metodo get para solicitante
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna solicitante
	 */
	public String getSolicitante() {
		return solicitante;
	}

	/**
	 * Metodo set para solicitante
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea solicitante
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna nomenclatura
	 */
	public String getNomenclatura() {
		return nomenclatura;
	}

	/**
	 * Metodo set para nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea nomenclatura
	 */
	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	/**
	 * Metodo get para fechaCreacion
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @return Retorna fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * Metodo set para fechaCreacion
	 * 
	 * @author Sergio Arias
	 * @date 16/01/2020
	 * @param setea fechaCreacion
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
