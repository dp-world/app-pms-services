/**
 * 
 */
package co.dpworld.pmsapp.model;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa el objeto para el datatable de cola de trabajo
 * 
 * @author Sergio Arias
 * @date 20/11/2019
 *
 */
public class TablaColaDeTrabajo {

	/**
	 * Contiene la cantidad de registros de la tabla
	 */
	@ApiModelProperty(value = "Cantidad de registros retornados en la lista de cola de trabajo")
	private int cantidad;
	/**
	 * Contiene la lista con los objetos de la cola de trabajo
	 */
	@ApiModelProperty(value = "Objeto que contiene la lista con los datos de la cola de trabajo")
	private List<ColaDeTrabajo> listaColaDeTrabajo;

	/**
	 * Constructor vacio
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 */
	public TablaColaDeTrabajo() {

	}

	/**
	 * Metodo get para cantidad
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * Metodo set para cantidad
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea cantidad
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * Metodo get para listaColaDeTrabajo
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna listaColaDeTrabajo
	 */
	public List<ColaDeTrabajo> getListaColaDeTrabajo() {
		return listaColaDeTrabajo;
	}

	/**
	 * Metodo set para listaColaDeTrabajo
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea listaColaDeTrabajo
	 */
	public void setListaColaDeTrabajo(List<ColaDeTrabajo> listaColaDeTrabajo) {
		this.listaColaDeTrabajo = listaColaDeTrabajo;
	}

}
