/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 23/11/2019
 *
 */
public class Archivo {

	private byte[] archivo;

	private String tituloArchivo;

	private String mimetype;

	/**
	 * Metodo get para archivo
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @return Retorna archivo
	 */
	public byte[] getArchivo() {
		return archivo;
	}

	/**
	 * Metodo set para archivo
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param setea archivo
	 */
	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	/**
	 * Metodo get para tituloArchivo
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @return Retorna tituloArchivo
	 */
	public String getTituloArchivo() {
		return tituloArchivo;
	}

	/**
	 * Metodo set para tituloArchivo
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param setea tituloArchivo
	 */
	public void setTituloArchivo(String tituloArchivo) {
		this.tituloArchivo = tituloArchivo;
	}

	/**
	 * Metodo get para mimetype
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @return Retorna mimetype
	 */
	public String getMimetype() {
		return mimetype;
	}

	/**
	 * Metodo set para mimetype
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param setea mimetype
	 */
	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	@Override
	public String toString() {
		return "Archivo [tituloArchivo=" + tituloArchivo + ", mimetype=" + mimetype + "]";
	}

}
