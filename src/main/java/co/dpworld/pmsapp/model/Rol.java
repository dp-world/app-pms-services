
package co.dpworld.pmsapp.model;

/**
 * Clase que contiene la informacion de un rol asociado a un usuario
 * 
 * @author Sergio Arias
 * @date 7/11/2019
 *
 */
public class Rol {

	private Integer idRol;

	private String nombre;

	private String descripcion;

	public Integer getIdRol() {
		return idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}