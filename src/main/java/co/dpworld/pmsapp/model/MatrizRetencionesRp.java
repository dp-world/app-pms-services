/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 18/01/2020
 *
 */
public class MatrizRetencionesRp {

	private String id;
	private String departamento;
	private String noRegistro; // nomeclatura
	private String nombreRegistro; // nombre documento
	private String tiempoDeRetencion;
	private String medioAlmacenamiento;
	private String responsable;

	/**
	 * Metodo get para id
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Metodo set para id
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para noRegistro
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna noRegistro
	 */
	public String getNoRegistro() {
		return noRegistro;
	}

	/**
	 * Metodo set para noRegistro
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea noRegistro
	 */
	public void setNoRegistro(String noRegistro) {
		this.noRegistro = noRegistro;
	}

	/**
	 * Metodo get para nombreRegistro
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna nombreRegistro
	 */
	public String getNombreRegistro() {
		return nombreRegistro;
	}

	/**
	 * Metodo set para nombreRegistro
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea nombreRegistro
	 */
	public void setNombreRegistro(String nombreRegistro) {
		this.nombreRegistro = nombreRegistro;
	}

	/**
	 * Metodo get para tiempoDeRetencion
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna tiempoDeRetencion
	 */
	public String getTiempoDeRetencion() {
		return tiempoDeRetencion;
	}

	/**
	 * Metodo set para tiempoDeRetencion
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea tiempoDeRetencion
	 */
	public void setTiempoDeRetencion(String tiempoDeRetencion) {
		this.tiempoDeRetencion = tiempoDeRetencion;
	}

	/**
	 * Metodo get para medioAlmacenamiento
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna medioAlmacenamiento
	 */
	public String getMedioAlmacenamiento() {
		return medioAlmacenamiento;
	}

	/**
	 * Metodo set para medioAlmacenamiento
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea medioAlmacenamiento
	 */
	public void setMedioAlmacenamiento(String medioAlmacenamiento) {
		this.medioAlmacenamiento = medioAlmacenamiento;
	}

	/**
	 * Metodo get para responsable
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @return Retorna responsable
	 */
	public String getResponsable() {
		return responsable;
	}

	/**
	 * Metodo set para responsable
	 * 
	 * @author Sergio Arias
	 * @date 18/01/2020
	 * @param setea responsable
	 */
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

}
