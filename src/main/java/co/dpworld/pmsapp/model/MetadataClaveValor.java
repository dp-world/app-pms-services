/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 11/11/2019
 *
 */
public class MetadataClaveValor {

	private String clave;
	private String valor;

	public MetadataClaveValor() {

	}

	public MetadataClaveValor(String clave, String valor) {
		super();
		this.clave = clave;
		this.valor = valor;
	}

	/**
	 * Metodo get para clave
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @return Retorna clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Metodo set para clave
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @param setea clave
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * Metodo get para valor
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @return Retorna valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Metodo set para valor
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @param setea valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "MetadataClaveValor [clave=" + clave + ", valor=" + valor + "]";
	}
	
	

}
