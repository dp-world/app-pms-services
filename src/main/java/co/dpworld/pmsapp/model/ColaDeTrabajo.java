/**
 * 
 */
package co.dpworld.pmsapp.model;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa los datos de la cola de trabajo
 * 
 * @author Sergio Arias
 * @date 20/11/2019
 *
 */
public class ColaDeTrabajo {

	/** Id de la cola de trabajo (ID del workflow) */
	@ApiModelProperty(value = "Id del registro de la cola de trabajo")
	private String id;

	@ApiModelProperty(value = "Id del paso en el que se encuentra el flujo (Se debe guardar para buscar la info del flujo)")
	private String idPaso;

	/** Doc name del documento */
	@ApiModelProperty(value = "Doc name del documento (Se debe guardar para actualizar metadata - guardar)")
	private String dDocName;

	/** Numero de revision del documento */
	@ApiModelProperty(value = "Numero de revision del documento (Se debe guardar para actualizar metadata - guardar)")
	private String dRevLabel;

	/** Grupo de seguridad del documento */
	@ApiModelProperty(value = "Grupo de seguridad del documento (Se debe guardar para actualizar metadata - guardar)")
	private String dSecurityGroup;

	/**
	 * Tipo de solicitud del flujo (Creacion, Actualizacion, Obsoletizacion,
	 * Reactivacion)
	 */
	@ApiModelProperty(value = "Tipo de solicitud del flujo ")
	private String tipoSolicitud;

	/** Nombre del departamento */
	@ApiModelProperty(value = "Nombre del departamento y/o area")
	private String departamento;

	/** Fecha */
	@ApiModelProperty(value = "Fecha")
	private String fecha;

	/** Nombre del paso del flujo */
	@ApiModelProperty(value = "Nombre del paso actual del flujo")
	private String nombrePaso;

	/** Indica si requiere algun tipo de soporte */
	@ApiModelProperty(value = "Indica si requiere soporte")
	private boolean requiereSoporte;

	@ApiModelProperty(value = "Nombre del documento")
	private String nombreDocumento;

	@ApiModelProperty(value = "Contiene la lista de usuarios que estan en el token para revisar esta actividad del flujo")
	private List<String> listaUsuarios;
	
	private String comentarios;
	
	

	/**
	 * Metodo get para comentarios
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @return Retorna comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * Metodo set para comentarios
	 * @author Sergio Arias
	 * @date 18/04/2020
	 * @param setea comentarios
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * Metodo get para dDocName
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna dDocName
	 */
	public String getdDocName() {
		return dDocName;
	}

	/**
	 * Metodo set para dDocName
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea dDocName
	 */
	public void setdDocName(String dDocName) {
		this.dDocName = dDocName;
	}

	/**
	 * Metodo get para dRevLabel
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna dRevLabel
	 */
	public String getdRevLabel() {
		return dRevLabel;
	}

	/**
	 * Metodo set para dRevLabel
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea dRevLabel
	 */
	public void setdRevLabel(String dRevLabel) {
		this.dRevLabel = dRevLabel;
	}

	/**
	 * Metodo get para dSecurityGroup
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @return Retorna dSecurityGroup
	 */
	public String getdSecurityGroup() {
		return dSecurityGroup;
	}

	/**
	 * Metodo set para dSecurityGroup
	 * 
	 * @author Sergio Arias
	 * @date 24/11/2019
	 * @param setea dSecurityGroup
	 */
	public void setdSecurityGroup(String dSecurityGroup) {
		this.dSecurityGroup = dSecurityGroup;
	}

	/**
	 * Metodo get para idPaso
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna idPaso
	 */
	public String getIdPaso() {
		return idPaso;
	}

	/**
	 * Metodo set para idPaso
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea idPaso
	 */
	public void setIdPaso(String idPaso) {
		this.idPaso = idPaso;
	}

	/**
	 * Metodo get para listaUsuarios
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna listaUsuarios
	 */
	public List<String> getListaUsuarios() {
		return listaUsuarios;
	}

	/**
	 * Metodo set para listaUsuarios
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea listaUsuarios
	 */
	public void setListaUsuarios(List<String> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	/**
	 * Metodo get para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna nombreDocumento
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}

	/**
	 * Metodo set para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea nombreDocumento
	 */
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	/**
	 * Constructor vacio
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 */
	public ColaDeTrabajo() {

	}

	/**
	 * Metodo get para id
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Metodo set para id
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Metodo get para tipoSolicitud
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna tipoSolicitud
	 */
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	/**
	 * Metodo set para tipoSolicitud
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea tipoSolicitud
	 */
	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para fecha
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Metodo set para fecha
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea fecha
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * Metodo get para nombrePaso
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna nombrePaso
	 */
	public String getNombrePaso() {
		return nombrePaso;
	}

	/**
	 * Metodo set para nombrePaso
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea nombrePaso
	 */
	public void setNombrePaso(String nombrePaso) {
		this.nombrePaso = nombrePaso;
	}

	/**
	 * Metodo get para requiereSoporte
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @return Retorna requiereSoporte
	 */
	public boolean isRequiereSoporte() {
		return requiereSoporte;
	}

	/**
	 * Metodo set para requiereSoporte
	 * 
	 * @author Sergio Arias
	 * @date 20/11/2019
	 * @param setea requiereSoporte
	 */
	public void setRequiereSoporte(boolean requiereSoporte) {
		this.requiereSoporte = requiereSoporte;
	}

	@Override
	public String toString() {
		return "ColaDeTrabajo [id=" + id + ", tipoSolicitud=" + tipoSolicitud + ", departamento=" + departamento
				+ ", fecha=" + fecha + ", nombrePaso=" + nombrePaso + ", requiereSoporte=" + requiereSoporte
				+ ", nombreDocumento=" + nombreDocumento + "]";
	}

}
