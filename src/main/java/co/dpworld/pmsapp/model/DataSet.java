/**
 * 
 */
package co.dpworld.pmsapp.model;

import java.util.List;

/**
 * @author Sergio Arias
 * @date 23/02/2020
 *
 */
public class DataSet {

	private String label;
	private String fillColor;
	private String strokeColor;
	private String pointColor;
	private String pointStrokeColor;
	private String pointHighlightFill;
	private String pointHighlightStroke;
	private List<Long> data;

	
	public DataSet() {
		this.fillColor = "rgba(210, 214, 222, 1)";
		this.strokeColor = "rgba(210, 214, 222, 1)";
		this.pointColor = "rgba(210, 214, 222, 1)";
		this.pointStrokeColor = "#c1c7d1";
		this.pointHighlightFill = "#fff";
		this.pointHighlightStroke = "rgba(220,220,220,1)";
	}
	
	/**
	 * Metodo get para label
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Metodo set para label
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Metodo get para fillColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna fillColor
	 */
	public String getFillColor() {
		return fillColor;
	}

	/**
	 * Metodo set para fillColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea fillColor
	 */
	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}

	/**
	 * Metodo get para strokeColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna strokeColor
	 */
	public String getStrokeColor() {
		return strokeColor;
	}

	/**
	 * Metodo set para strokeColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea strokeColor
	 */
	public void setStrokeColor(String strokeColor) {
		this.strokeColor = strokeColor;
	}

	/**
	 * Metodo get para pointColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna pointColor
	 */
	public String getPointColor() {
		return pointColor;
	}

	/**
	 * Metodo set para pointColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea pointColor
	 */
	public void setPointColor(String pointColor) {
		this.pointColor = pointColor;
	}

	/**
	 * Metodo get para pointStrokeColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna pointStrokeColor
	 */
	public String getPointStrokeColor() {
		return pointStrokeColor;
	}

	/**
	 * Metodo set para pointStrokeColor
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea pointStrokeColor
	 */
	public void setPointStrokeColor(String pointStrokeColor) {
		this.pointStrokeColor = pointStrokeColor;
	}

	/**
	 * Metodo get para pointHighlightFill
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna pointHighlightFill
	 */
	public String getPointHighlightFill() {
		return pointHighlightFill;
	}

	/**
	 * Metodo set para pointHighlightFill
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea pointHighlightFill
	 */
	public void setPointHighlightFill(String pointHighlightFill) {
		this.pointHighlightFill = pointHighlightFill;
	}

	/**
	 * Metodo get para pointHighlightStroke
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna pointHighlightStroke
	 */
	public String getPointHighlightStroke() {
		return pointHighlightStroke;
	}

	/**
	 * Metodo set para pointHighlightStroke
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea pointHighlightStroke
	 */
	public void setPointHighlightStroke(String pointHighlightStroke) {
		this.pointHighlightStroke = pointHighlightStroke;
	}

	/**
	 * Metodo get para data
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @return Retorna data
	 */
	public List<Long> getData() {
		return data;
	}

	/**
	 * Metodo set para data
	 * 
	 * @author Sergio Arias
	 * @date 23/02/2020
	 * @param setea data
	 */
	public void setData(List<Long> data) {
		this.data = data;
	}

}
