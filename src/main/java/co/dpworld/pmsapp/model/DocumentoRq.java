/**
 * 
 */
package co.dpworld.pmsapp.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que contiene la informacion para la creacion de un documento
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
public class DocumentoRq {

	/* Nombre del solicitante de la creacion del documento */
	// @NotEmpty(message = "El campo solicitante no puede ser vacio ")
	// @NotNull(message = "El campo solicitante no puede ser nulo ")
	@ApiModelProperty(value = "Nombre del solicitante de la creacion del documento", allowEmptyValue = false, required = true)
	private String solicitante;

	/* Departamento o categoria del documento */
	// @NotNull(message = "El objeto departamento no puede ser nulo ")
	@ApiModelProperty(value = "Departamento o categoria del documento", allowEmptyValue = false, required = true)
	private Departamento departamento;

	/* Tipologia documental */
	@ApiModelProperty(value = "Tipologia documental", allowEmptyValue = false, required = true)
	private Tipologia tipoDocumento;

	/*
	 * Indica si requiere soporte o no , se debe asignar una persona para el soporte
	 */
	// @NotNull(message = "El campo requiereSoporte no puede ser nulo ")
	@ApiModelProperty(value = "Indica si requiere soporte o no , se debe asignar una persona para el soporte", allowEmptyValue = false, required = true)
	private boolean requiereSoporte;

	/* Contiene la razón y/o fuente de la creación del documento */
	// @NotNull(message = "El objeto razonFuenteDeCreacion no puede ser nulo ")
	@ApiModelProperty(value = " Contiene la razón y/o fuente de la creación del documento", allowEmptyValue = false, required = true)
	private RazonCreacion razonFuenteDeCreacion;

	/*
	 * Contiene el tipo de solicitud en el sistema (Creacion, Actualización,
	 * Obsoletización, Reactivacion )
	 */
	// @NotEmpty(message = "El campo tipoSolicitud no puede ser vacio ")
	// @NotNull(message = "El campo tipoSolicitud no puede ser nulo ")
	@ApiModelProperty(value = "Contiene el tipo de solicitud en el sistema (C,A,O,R)", allowEmptyValue = false, required = true)
	private String tipoSolicitud;

	/* Contiene el tipo de cambio del documento */
	// @NotEmpty(message = "El campo tipoCambio no puede ser vacio ")
	// @NotNull(message = "El campo tipoCambio no puede ser nulo ")
	@ApiModelProperty(value = "Contiene el tipo de cambio del documento", allowEmptyValue = false, required = true)
	private String tipoCambio;

	/* Indica si requiere adiestramiento el documento */
	// @NotNull(message = "El campo requiereAdiestramiento no puede ser nulo y/o
	// vacio")
	@ApiModelProperty(value = "Indica si requiere adiestramiento el documento", allowEmptyValue = false, required = true)
	private boolean requiereAdiestramiento;

	/* Cadena de texto en base 64 con el documento que se esta creando */
	// @NotEmpty(message = "El campo documentobase64 no puede ser vacio ")
	// @NotNull(message = "El campo documentobase64 no puede ser nulo ")
	@ApiModelProperty(value = "Cadena de texto en base 64 con el documento que se esta creando", allowEmptyValue = false, required = true)
	private byte[] documentobase64;

	// @NotEmpty(message = "El campo tituloDocumento no puede ser vacio ")
	// @NotNull(message = "El campo tituloDocumento no puede ser nulo ")
	@ApiModelProperty(value = "Titulo del documento en base 64", allowEmptyValue = false, required = true, notes = "El titulo debe contener la extension del documento")
	private String tituloDocumento;

	@ApiModelProperty(value = "Nombre del documento", allowEmptyValue = false, required = true, notes = "Nombre del documento")
	private String nombreDocumento;

	@ApiModelProperty(value = "Titulo del documento anexo en base 64", allowEmptyValue = false, required = true, notes = "El titulo debe contener la extension del documento")
	private String tituloDocumentoAnexo;

	@ApiModelProperty(value = "Cadena de texto en base 64 con el documento anexo", allowEmptyValue = false, required = true)
	private byte[] documentoAnexo;

	//########################
	//   Data nomenclatura
	//#######################
	private String areaActual;
	private String areaPropuesta;
	private String cambioTipoDocumentoDe;
	private String cambioTipoDocumentoA;
	private String razonCambio;
	
	//Opcional
	private String justificacion;
	
	private String comentarios;
	
	
	

	/**
	 * Metodo get para comentarios
	 * @author Sergio Arias
	 * @date 23/04/2020
	 * @return Retorna comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * Metodo set para comentarios
	 * @author Sergio Arias
	 * @date 23/04/2020
	 * @param setea comentarios
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * Metodo get para justificacion
	 * @author Sergio Arias
	 * @date 4/01/2020
	 * @return Retorna justificacion
	 */
	public String getJustificacion() {
		return justificacion;
	}

	/**
	 * Metodo set para justificacion
	 * @author Sergio Arias
	 * @date 4/01/2020
	 * @param setea justificacion
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}

	/**
	 * Metodo get para razonCambio
	 * 
	 * @author Sergio Arias
	 * @date 2/01/2020
	 * @return Retorna razonCambio
	 */
	public String getRazonCambio() {
		return razonCambio;
	}

	/**
	 * Metodo set para razonCambio
	 * 
	 * @author Sergio Arias
	 * @date 2/01/2020
	 * @param setea razonCambio
	 */
	public void setRazonCambio(String razonCambio) {
		this.razonCambio = razonCambio;
	}

	public String getAreaActual() {
		return areaActual;
	}

	public void setAreaActual(String areaActual) {
		this.areaActual = areaActual;
	}

	public String getAreaPropuesta() {
		return areaPropuesta;
	}

	public void setAreaPropuesta(String areaPropuesta) {
		this.areaPropuesta = areaPropuesta;
	}

	public String getCambioTipoDocumentoDe() {
		return cambioTipoDocumentoDe;
	}

	public void setCambioTipoDocumentoDe(String cambioTipoDocumentoDe) {
		this.cambioTipoDocumentoDe = cambioTipoDocumentoDe;
	}

	public String getCambioTipoDocumentoA() {
		return cambioTipoDocumentoA;
	}

	public void setCambioTipoDocumentoA(String cambioTipoDocumentoA) {
		this.cambioTipoDocumentoA = cambioTipoDocumentoA;
	}

	/**
	 * Metodo get para tituloDocumentoAnexo
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @return Retorna tituloDocumentoAnexo
	 */
	public String getTituloDocumentoAnexo() {
		return tituloDocumentoAnexo;
	}

	/**
	 * Metodo set para tituloDocumentoAnexo
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param setea tituloDocumentoAnexo
	 */
	public void setTituloDocumentoAnexo(String tituloDocumentoAnexo) {
		this.tituloDocumentoAnexo = tituloDocumentoAnexo;
	}

	/**
	 * Metodo get para documentoAnexo
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @return Retorna documentoAnexo
	 */
	public byte[] getDocumentoAnexo() {
		return documentoAnexo;
	}

	/**
	 * Metodo set para documentoAnexo
	 * 
	 * @author Sergio Arias
	 * @date 28/12/2019
	 * @param setea documentoAnexo
	 */
	public void setDocumentoAnexo(byte[] documentoAnexo) {
		this.documentoAnexo = documentoAnexo;
	}

	/**
	 * Metodo get para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 19/12/2019
	 * @return Retorna nombreDocumento
	 */
	public String getNombreDocumento() {
		
		if(this.nombreDocumento != null && nombreDocumento.contains("->"))
		{
			return nombreDocumento.split("->")[1]; // Retornar el nombre de documento 
		}
		
		return nombreDocumento;
	}

	/**
	 * Metodo set para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 19/12/2019
	 * @param setea nombreDocumento
	 */
	public void setNombreDocumento(String nombreDocumento) {
		
		if(this.nombreDocumento != null && nombreDocumento.contains("->"))
		{
			this.nombreDocumento = nombreDocumento.split("->")[1]; 
		}
		
		this.nombreDocumento = nombreDocumento;
	}

	/**
	 * Constructor vacio para creacion documento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 */
	public DocumentoRq() {

	}

	/**
	 * Constructor con campos para creacion documento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param solicitante
	 * @param departamento
	 * @param tipoDocumento
	 * @param requiereSoporte
	 * @param razonFuenteDeCreacion
	 * @param tipoSolicitud
	 * @param tipoCambio
	 * @param requiereAdiestramiento
	 * @param documentoBase64
	 */
	public DocumentoRq(String solicitante, Departamento departamento, Tipologia tipoDocumento, boolean requiereSoporte,
			RazonCreacion razonFuenteDeCreacion, String tipoSolicitud, String tipoCambio,
			boolean requiereAdiestramiento, byte[] documentoBase64, String tituloDocumento) {
		super();
		this.solicitante = solicitante;
		this.departamento = departamento;
		this.tipoDocumento = tipoDocumento;
		this.requiereSoporte = requiereSoporte;
		this.razonFuenteDeCreacion = razonFuenteDeCreacion;
		this.tipoSolicitud = tipoSolicitud;
		this.tipoCambio = tipoCambio;
		this.requiereAdiestramiento = requiereAdiestramiento;
		this.documentobase64 = documentoBase64;
		this.tituloDocumento = tituloDocumento;
	}

	/**
	 * Metodo get para tituloDocumento
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @return Retorna tituloDocumento
	 */
	public String getTituloDocumento() {
		return tituloDocumento;
	}

	/**
	 * Metodo set para tituloDocumento
	 * 
	 * @author Sergio Arias
	 * @date 11/11/2019
	 * @param setea tituloDocumento
	 */
	public void setTituloDocumento(String tituloDocumento) {
		this.tituloDocumento = tituloDocumento;
	}

	/**
	 * Metodo get para solicitante
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna solicitante
	 */
	public String getSolicitante() {
		return solicitante;
	}

	/**
	 * Metodo set para solicitante
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea solicitante
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna departamento
	 */
	public Departamento getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea departamento
	 */
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para tipoDocumento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna tipoDocumento
	 */
	public Tipologia getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Metodo set para tipoDocumento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea tipoDocumento
	 */
	public void setTipoDocumento(Tipologia tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Metodo get para requiereSoporte
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna requiereSoporte
	 */
	public boolean isRequiereSoporte() {
		return requiereSoporte;
	}

	/**
	 * Metodo set para requiereSoporte
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea requiereSoporte
	 */
	public void setRequiereSoporte(boolean requiereSoporte) {
		this.requiereSoporte = requiereSoporte;
	}

	/**
	 * Metodo get para razonFuenteDeCreacion
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna razonFuenteDeCreacion
	 */
	public RazonCreacion getRazonFuenteDeCreacion() {
		return razonFuenteDeCreacion;
	}

	/**
	 * Metodo set para razonFuenteDeCreacion
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea razonFuenteDeCreacion
	 */
	public void setRazonFuenteDeCreacion(RazonCreacion razonFuenteDeCreacion) {
		this.razonFuenteDeCreacion = razonFuenteDeCreacion;
	}

	/**
	 * Metodo get para tipoSolicitud
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna tipoSolicitud
	 */
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	/**
	 * Metodo set para tipoSolicitud
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea tipoSolicitud
	 */
	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	/**
	 * Metodo get para tipoCambio
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna tipoCambio
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * Metodo set para tipoCambio
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea tipoCambio
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	/**
	 * Metodo get para requiereAdiestramiento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna requiereAdiestramiento
	 */
	public boolean isRequiereAdiestramiento() {
		return requiereAdiestramiento;
	}

	/**
	 * Metodo set para requiereAdiestramiento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea requiereAdiestramiento
	 */
	public void setRequiereAdiestramiento(boolean requiereAdiestramiento) {
		this.requiereAdiestramiento = requiereAdiestramiento;
	}

	/**
	 * Metodo get para documentobase64
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna documentobase64
	 */
	public byte[] getDocumentobase64() {
		return documentobase64;
	}

	/**
	 * Metodo set para documentobase64
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea documentobase64
	 */
	public void setDocumentobase64(byte[] documentobase64) {
		this.documentobase64 = documentobase64;
	}

}
