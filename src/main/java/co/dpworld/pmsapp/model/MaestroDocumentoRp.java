/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 23/01/2020
 *
 */
public class MaestroDocumentoRp {

	private String id;
	private String departamento;
	private String nomenclatura;
	private String revision;
	private String descripcionUltimoCambio;
	private String fechaAprobacion;
	private String clasificacion;
	private String aprobadoPor;
	private String nombreDocumento;

	/**
	 * Metodo get para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna nombreDocumento
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}

	/**
	 * Metodo set para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea nombreDocumento
	 */
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	/**
	 * Metodo get para id
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Metodo set para id
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna nomenclatura
	 */
	public String getNomenclatura() {
		return nomenclatura;
	}

	/**
	 * Metodo set para nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea nomenclatura
	 */
	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	/**
	 * Metodo get para revision
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna revision
	 */
	public String getRevision() {
		return revision;
	}

	/**
	 * Metodo set para revision
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea revision
	 */
	public void setRevision(String revision) {
		this.revision = revision;
	}

	/**
	 * Metodo get para descripcionUltimoCambio
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna descripcionUltimoCambio
	 */
	public String getDescripcionUltimoCambio() {
		return descripcionUltimoCambio;
	}

	/**
	 * Metodo set para descripcionUltimoCambio
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea descripcionUltimoCambio
	 */
	public void setDescripcionUltimoCambio(String descripcionUltimoCambio) {
		this.descripcionUltimoCambio = descripcionUltimoCambio;
	}

	/**
	 * Metodo get para fechaAprobacion
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna fechaAprobacion
	 */
	public String getFechaAprobacion() {
		return fechaAprobacion;
	}

	/**
	 * Metodo set para fechaAprobacion
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea fechaAprobacion
	 */
	public void setFechaAprobacion(String fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	/**
	 * Metodo get para clasificacion
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna clasificacion
	 */
	public String getClasificacion() {
		return clasificacion;
	}

	/**
	 * Metodo set para clasificacion
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea clasificacion
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	/**
	 * Metodo get para aprobadoPor
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @return Retorna aprobadoPor
	 */
	public String getAprobadoPor() {
		return aprobadoPor;
	}

	/**
	 * Metodo set para aprobadoPor
	 * 
	 * @author Sergio Arias
	 * @date 23/01/2020
	 * @param setea aprobadoPor
	 */
	public void setAprobadoPor(String aprobadoPor) {
		this.aprobadoPor = aprobadoPor;
	}

}
