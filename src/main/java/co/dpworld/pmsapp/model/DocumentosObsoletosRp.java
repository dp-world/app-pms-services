/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 29/02/2020
 *
 */
public class DocumentosObsoletosRp {

	private String Id;
	private String departamento;
	private String nomenclatura;
	private String nombreDocumento;
	private String revision;
	private String fechaEfectiva;
	private String aprobadoPor;

	/**
	 * Metodo get para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna nombreDocumento
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}

	/**
	 * Metodo set para nombreDocumento
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea nombreDocumento
	 */
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	/**
	 * Metodo get para revision
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna revision
	 */
	public String getRevision() {
		return revision;
	}

	/**
	 * Metodo set para revision
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea revision
	 */
	public void setRevision(String revision) {
		this.revision = revision;
	}

	/**
	 * Metodo get para fechaEfectiva
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna fechaEfectiva
	 */
	public String getFechaEfectiva() {
		return fechaEfectiva;
	}

	/**
	 * Metodo set para fechaEfectiva
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea fechaEfectiva
	 */
	public void setFechaEfectiva(String fechaEfectiva) {
		this.fechaEfectiva = fechaEfectiva;
	}

	/**
	 * Metodo get para aprobadoPor
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna aprobadoPor
	 */
	public String getAprobadoPor() {
		return aprobadoPor;
	}

	/**
	 * Metodo set para aprobadoPor
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea aprobadoPor
	 */
	public void setAprobadoPor(String aprobadoPor) {
		this.aprobadoPor = aprobadoPor;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * Metodo get para nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna nomenclatura
	 */
	public String getNomenclatura() {
		return nomenclatura;
	}

	/**
	 * Metodo set para nomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea nomenclatura
	 */
	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	/**
	 * Metodo get para id
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @return Retorna id
	 */
	public String getId() {
		return Id;
	}

	/**
	 * Metodo set para id
	 * 
	 * @author Sergio Arias
	 * @date 29/02/2020
	 * @param setea id
	 */
	public void setId(String id) {
		Id = id;
	}

}
