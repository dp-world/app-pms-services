/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergio Arias
 * @date 29/03/2020
 *
 */
@Validated
public class TiempoRetencion {

	/* Codigo del tiempo de retencion */
	@NotEmpty(message = "El campo codigo (tiempo retencion) no puede ser vacio ")
	@NotNull(message = "El campo codigo (tiempo retencion) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo de la tipologia", allowEmptyValue = false, required = true)
	private String codigo;

	/* Nombre de la tipologia */
	@NotNull(message = "El campo nombre tiempo retencion no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Nombre tiempo de retencion", allowEmptyValue = false, required = true)
	private String nombreTiempoRetencion;

	
	
	public TiempoRetencion(String codigo, String nombreTiempoRetencion) {
		super();
		this.codigo = codigo;
		this.nombreTiempoRetencion = nombreTiempoRetencion;
	}

	/**
	 * Metodo get para codigo
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Metodo set para codigo
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Metodo get para nombreTiempoRetencion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna nombreTiempoRetencion
	 */
	public String getNombreTiempoRetencion() {
		return nombreTiempoRetencion;
	}

	/**
	 * Metodo set para nombreTiempoRetencion
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea nombreTiempoRetencion
	 */
	public void setNombreTiempoRetencion(String nombreTiempoRetencion) {
		this.nombreTiempoRetencion = nombreTiempoRetencion;
	}

}
