/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 23/11/2019
 *
 */
public class PasoRq {
	
	private TIPO_DE_ACCION_WF tipoDeAccion;
	private String razonRechazo;

	/**
	 * Metodo get para tipoDeAccion
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @return Retorna tipoDeAccion
	 */
	public TIPO_DE_ACCION_WF getTipoDeAccion() {
		return tipoDeAccion;
	}

	/**
	 * Metodo set para tipoDeAccion
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param setea tipoDeAccion
	 */
	public void setTipoDeAccion(TIPO_DE_ACCION_WF tipoDeAccion) {
		this.tipoDeAccion = tipoDeAccion;
	}

	/**
	 * Metodo get para razonRechazo
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @return Retorna razonRechazo
	 */
	public String getRazonRechazo() {
		return razonRechazo;
	}

	/**
	 * Metodo set para razonRechazo
	 * 
	 * @author Sergio Arias
	 * @date 23/11/2019
	 * @param setea razonRechazo
	 */
	public void setRazonRechazo(String razonRechazo) {
		this.razonRechazo = razonRechazo;
	}

}
