/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergio Arias
 * @date 30/11/2019
 *
 */
public class AnexoRq {

	@NotEmpty(message = "El campo tituloDocumento no puede ser vacio ")
	@NotNull(message = "El campo tituloDocumento no puede ser nulo ")
	@ApiModelProperty(value = "Titulo del documento en base 64", allowEmptyValue = false, required = true, notes = "El titulo debe contener la extension del documento")
	private String tituloDocumento;

	/* Id del documento principal al cual se le asocia un anexo */
	@NotEmpty(message = "El campo documentobase64 no puede ser vacio ")
	@NotNull(message = "El campo documentobase64 no puede ser nulo ")
	@ApiModelProperty(value = "dID del documento principal al cual se le asocia un anexo", allowEmptyValue = false, required = true)
	private String idDocumentoPrincipal;

	/* Cadena de texto en base 64 con el documento que se esta anexando */
	@NotEmpty(message = "El campo documentobase64 no puede ser vacio ")
	@NotNull(message = "El campo documentobase64 no puede ser nulo ")
	@ApiModelProperty(value = "Cadena de texto en base 64 con el documento", allowEmptyValue = false, required = true)
	private byte[] documentoAnexo;

	/**
	 * Metodo get para tituloDocumento
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @return Retorna tituloDocumento
	 */
	public String getTituloDocumento() {
		return tituloDocumento;
	}

	/**
	 * Metodo set para tituloDocumento
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param setea tituloDocumento
	 */
	public void setTituloDocumento(String tituloDocumento) {
		this.tituloDocumento = tituloDocumento;
	}

	/**
	 * Metodo get para idDocumentoPrincipal
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @return Retorna idDocumentoPrincipal
	 */
	public String getIdDocumentoPrincipal() {
		return idDocumentoPrincipal;
	}

	/**
	 * Metodo set para idDocumentoPrincipal
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param setea idDocumentoPrincipal
	 */
	public void setIdDocumentoPrincipal(String idDocumentoPrincipal) {
		this.idDocumentoPrincipal = idDocumentoPrincipal;
	}

	/**
	 * Metodo get para documentoAnexo
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @return Retorna documentoAnexo
	 */
	public byte[] getDocumentoAnexo() {
		return documentoAnexo;
	}

	/**
	 * Metodo set para documentoAnexo
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param setea documentoAnexo
	 */
	public void setDocumentoAnexo(byte[] documentoAnexo) {
		this.documentoAnexo = documentoAnexo;
	}

}
