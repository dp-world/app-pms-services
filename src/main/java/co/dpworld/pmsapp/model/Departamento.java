/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que contiene la información de un departamento / area
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
public class Departamento {

	/* Codigo del departamento y/o area */
	@Size(min = 1, message = "El campo codigo (Departamento)  debe tener minimo 1 caracteres")
	@NotNull(message = "El campo codigo (Departamento) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo del departamento y/o area", allowEmptyValue = false, required = true)
	private String codigo;
	/* Nombre del departamento y/o area */
	@Size(min = 3, message = "El campo nombreDepartamento (Departamento) debe tener minimo 3 caracteres")
	@NotNull(message = "El campo nombreDepartamento no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Cadena de texto con el nombre del departamento y/o area", allowEmptyValue = false, required = true)
	private String nombreDepartamento;

	/**
	 * Constructor vacio para departamento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 */
	public Departamento() {

	}

	/**
	 * Constructor usando campos de departamento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param codigo             Codigo del departamento y/o area
	 * @param nombreDepartamento Nombre del departamento y/o area
	 */
	public Departamento(String codigo, String nombreDepartamento) {
		super();
		this.codigo = codigo;
		this.nombreDepartamento = nombreDepartamento;
	}

	/**
	 * Metodo get para codigo
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Metodo set para codigo
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Metodo get para nombreDepartamento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna nombreDepartamento
	 */
	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	/**
	 * Metodo set para nombreDepartamento
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea nombreDepartamento
	 */
	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

}
