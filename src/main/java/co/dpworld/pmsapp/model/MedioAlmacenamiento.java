/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergio Arias
 * @date 29/03/2020
 *
 */
@Validated
public class MedioAlmacenamiento {

	/* Codigo del medio */
	@NotEmpty(message = "El campo codigo (medio de almacenamiento) no puede ser vacio ")
	@NotNull(message = "El campo codigo (medio de almacenamiento) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo del medio de almacenamiento", allowEmptyValue = false, required = true)
	private String codigo;

	/* Nombre de la tipologia */
	@NotNull(message = "El campo nombreMedioAlmacenamiento no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Nombre del medio de almacenamiento", allowEmptyValue = false, required = true)
	private String nombreMedioAlmacenamiento;

	public MedioAlmacenamiento(String codigo, String nombreMedioAlmacenamiento) {
		super();
		this.codigo = codigo;
		this.nombreMedioAlmacenamiento = nombreMedioAlmacenamiento;
	}

	/**
	 * Metodo get para codigo
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Metodo set para codigo
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Metodo get para nombreMedioAlmacenamiento
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @return Retorna nombreMedioAlmacenamiento
	 */
	public String getNombreMedioAlmacenamiento() {
		return nombreMedioAlmacenamiento;
	}

	/**
	 * Metodo set para nombreMedioAlmacenamiento
	 * 
	 * @author Sergio Arias
	 * @date 29/03/2020
	 * @param setea nombreMedioAlmacenamiento
	 */
	public void setNombreMedioAlmacenamiento(String nombreMedioAlmacenamiento) {
		this.nombreMedioAlmacenamiento = nombreMedioAlmacenamiento;
	}

}
