/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 1/02/2020
 *
 */
public class CantidadFlujosRp {

	private long cantidadCreacion;
	private long cantidadActualizacion;
	private long cantidadObsoletizacion;
	private long cantidadReactivacion;
	private long cantidadNomenclatura;

	/**
	 * Metodo get para cantidadCreacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @return Retorna cantidadCreacion
	 */
	public long getCantidadCreacion() {
		return cantidadCreacion;
	}

	/**
	 * Metodo set para cantidadCreacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @param setea cantidadCreacion
	 */
	public void setCantidadCreacion(long cantidadCreacion) {
		this.cantidadCreacion = cantidadCreacion;
	}

	/**
	 * Metodo get para cantidadActualizacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @return Retorna cantidadActualizacion
	 */
	public long getCantidadActualizacion() {
		return cantidadActualizacion;
	}

	/**
	 * Metodo set para cantidadActualizacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @param setea cantidadActualizacion
	 */
	public void setCantidadActualizacion(long cantidadActualizacion) {
		this.cantidadActualizacion = cantidadActualizacion;
	}

	/**
	 * Metodo get para cantidadObsoletizacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @return Retorna cantidadObsoletizacion
	 */
	public long getCantidadObsoletizacion() {
		return cantidadObsoletizacion;
	}

	/**
	 * Metodo set para cantidadObsoletizacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @param setea cantidadObsoletizacion
	 */
	public void setCantidadObsoletizacion(long cantidadObsoletizacion) {
		this.cantidadObsoletizacion = cantidadObsoletizacion;
	}

	/**
	 * Metodo get para cantidadReactivacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @return Retorna cantidadReactivacion
	 */
	public long getCantidadReactivacion() {
		return cantidadReactivacion;
	}

	/**
	 * Metodo set para cantidadReactivacion
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @param setea cantidadReactivacion
	 */
	public void setCantidadReactivacion(long cantidadReactivacion) {
		this.cantidadReactivacion = cantidadReactivacion;
	}

	/**
	 * Metodo get para cantidadNomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @return Retorna cantidadNomenclatura
	 */
	public long getCantidadNomenclatura() {
		return cantidadNomenclatura;
	}

	/**
	 * Metodo set para cantidadNomenclatura
	 * 
	 * @author Sergio Arias
	 * @date 1/02/2020
	 * @param setea cantidadNomenclatura
	 */
	public void setCantidadNomenclatura(long cantidadNomenclatura) {
		this.cantidadNomenclatura = cantidadNomenclatura;
	}

}
