/**
 * 
 */
package co.dpworld.pmsapp.model;

/**
 * @author Sergio Arias
 * @date 30/11/2019
 *
 */
public class CrearDocumentoRp {

	private boolean estado;
	private String dID;

	/**
	 * Metodo get para estado
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @return Retorna estado
	 */
	public boolean isEstado() {
		return estado;
	}

	/**
	 * Metodo set para estado
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param setea estado
	 */
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	/**
	 * Metodo get para dID
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @return Retorna dID
	 */
	public String getdID() {
		return dID;
	}

	/**
	 * Metodo set para dID
	 * 
	 * @author Sergio Arias
	 * @date 30/11/2019
	 * @param setea dID
	 */
	public void setdID(String dID) {
		this.dID = dID;
	}

}
