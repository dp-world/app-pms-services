/**
 * 
 */
package co.dpworld.pmsapp.model;

import java.util.List;

/**
 * Clase que contiene los datos del usuario
 * 
 * @author Sergio Arias
 * @date 21/12/2019
 *
 */
public class InformacionUsuarioRp {

	private String nombreCompleto;
	private String departamento;
	private String nombreDepartamento;
	private String correoElectronico;
	private List<String> listaRoles;

	
	
	/**
	 * Metodo get para nombreDepartamento
	 * @author Sergio Arias
	 * @date 7/04/2020
	 * @return Retorna nombreDepartamento
	 */
	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	/**
	 * Metodo set para nombreDepartamento
	 * @author Sergio Arias
	 * @date 7/04/2020
	 * @param setea nombreDepartamento
	 */
	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	/**
	 * Metodo get para listaRoles
	 * @author Sergio Arias
	 * @date 7/04/2020
	 * @return Retorna listaRoles
	 */
	public List<String> getListaRoles() {
		return listaRoles;
	}

	/**
	 * Metodo set para listaRoles
	 * @author Sergio Arias
	 * @date 7/04/2020
	 * @param setea listaRoles
	 */
	public void setListaRoles(List<String> listaRoles) {
		this.listaRoles = listaRoles;
	}

	/**
	 * Metodo get para correoElectronico
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna correoElectronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * Metodo set para correoElectronico
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea correoElectronico
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * Metodo get para nombreCompleto
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna nombreCompleto
	 */
	public String getNombreCompleto() {
		return nombreCompleto;
	}

	/**
	 * Metodo set para nombreCompleto
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea nombreCompleto
	 */
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	/**
	 * Metodo get para departamento
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @return Retorna departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Metodo set para departamento
	 * 
	 * @author Sergio Arias
	 * @date 21/12/2019
	 * @param setea departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

}
