/**
 * 
 */
package co.dpworld.pmsapp.model;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que contiene la información de la razon creacion de un documento
 * 
 * @author Sergio Arias
 * @date 9/11/2019
 *
 */
public class RazonCreacion {

	/* Codigo del departamento y/o area */
	@NotNull(message = "El campo codigo (razonFuenteDeCreacion) no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Codigo de la razon y/o fuente de creacion", allowEmptyValue = false, required = true)
	private int codigo;
	/* Nombre del departamento y/o area */
	@NotNull(message = "El campo nombreRazonFuenteCreacion no puede ser nulo y/o vacio")
	@ApiModelProperty(value = "Cadena de texto con la razon y/o fuente de creacion", allowEmptyValue = false, required = true)
	private String nombreRazonFuenteCreacion;

	/**
	 * Constructor vacio para razon de cracion
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 */
	public RazonCreacion() {

	}

	/**
	 * Constructor con campos para razon de creacion
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param codigo                    Codigo de la razon y/o creacion
	 * @param nombreRazonFuenteCreacion Nombre de la razon fuente de creacion
	 */
	public RazonCreacion(int codigo, String razonFuenteCreacion) {
		super();
		this.codigo = codigo;
		this.nombreRazonFuenteCreacion = razonFuenteCreacion;
	}

	/**
	 * Metodo get para codigo
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * Metodo set para codigo
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea codigo
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * Metodo get para nombreRazonFuenteCreacion
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @return Retorna nombreRazonFuenteCreacion
	 */
	public String getNombreRazonFuenteCreacion() {
		return nombreRazonFuenteCreacion;
	}

	/**
	 * Metodo set para nombreRazonFuenteCreacion
	 * 
	 * @author Sergio Arias
	 * @date 9/11/2019
	 * @param setea nombreRazonFuenteCreacion
	 */
	public void setNombreRazonFuenteCreacion(String nombreRazonFuenteCreacion) {
		this.nombreRazonFuenteCreacion = nombreRazonFuenteCreacion;
	}

}
